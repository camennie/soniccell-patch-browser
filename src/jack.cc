/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <jack/jack.h>
#include <jack/midiport.h>
#include <alsa/asoundlib.h>
#include "main.h"
#include "widgets.h"
#include <iostream>
#include <vector>
#include <unistd.h>
#include <sys/time.h>
#include <fstream>
#include <FLAC++/metadata.h>
#include <FLAC++/encoder.h>

namespace
{

class JackEncoder: public FLAC::Encoder::File {
public:
	JackEncoder(): FLAC::Encoder::File() { }
protected:
	//virtual void progress_callback(FLAC__uint64 bytes_written, FLAC__uint64 samples_written, unsigned frames_written, unsigned total_frames_estimate);
};//JackEncoder

boost::shared_ptr<JackEncoder> jackEncoder;
jack_port_t *input_port;
jack_port_t *output_port;
jack_port_t *input_audio_port_l;
jack_port_t *input_audio_port_r;
jack_port_t *output_audio_port_l;
jack_port_t *output_audio_port_r;
snd_seq_t *seq_handle = NULL;
int outPortId;
std::vector<FLAC__int32> tmpBufferL;
std::vector<FLAC__int32> tmpBufferR;

time_t progStartSeconds;
unsigned int lastSendTime;

bool isRecordingSample;
bool xrunDuringRecord;

void jack_shutdown(void *arg)
{
//	exit(1);
}//jack_shutdown

int processXRun(void *arg)
{
    if (true == isRecordingSample) {
        xrunDuringRecord = true;
    }//if

    return 0;
}//processXRun

int processJack(jack_nframes_t nframes, void *arg)
{
    JackSingleton &editor = JackSingleton::instance();

    //Audio
    if (true == isRecordingSample) {
        jack_default_audio_sample_t *audio_in_l = (jack_default_audio_sample_t *)jack_port_get_buffer(input_audio_port_l, nframes);
        jack_default_audio_sample_t *audio_in_r = (jack_default_audio_sample_t *)jack_port_get_buffer(input_audio_port_r, nframes);

        if (tmpBufferL.size() < nframes) {
            tmpBufferL.resize(nframes);
            tmpBufferR.resize(nframes);
        }//if

        for (unsigned int cnt = 0; cnt < nframes; ++cnt) {
            tmpBufferL[cnt] = audio_in_l[cnt] * 0xffff;
            tmpBufferR[cnt] = audio_in_r[cnt] * 0xffff;
        }//for

        FLAC__int32 * buffer[2];
        buffer[0] = &tmpBufferL[0];
        buffer[1] = &tmpBufferR[0];
        bool ok = jackEncoder->process(buffer, nframes);
    }//if

    //Audio out
    {
        if (editor.playBuffer->empty() == false) {
            jack_default_audio_sample_t *audio_out_l = (jack_default_audio_sample_t *)jack_port_get_buffer(output_audio_port_l, nframes);
            jack_default_audio_sample_t *audio_out_r = (jack_default_audio_sample_t *)jack_port_get_buffer(output_audio_port_r, nframes);

            unsigned int numItems = 0;
            editor.playBuffer->pop_back_lots(nframes, audio_out_l, audio_out_r, &numItems);

            //std::cout << "audio out: " << editor.playBuffer->size() << " - " << numItems << std::endl;

            if (numItems < nframes) {
//                std::cout << "Underflow: " << nframes - numItems << std::endl;
                memset(&audio_out_l[numItems], audio_out_l[numItems-1], (nframes - numItems-1) * sizeof(float));
                memset(&audio_out_r[numItems], audio_out_r[numItems-1], (nframes - numItems-1) * sizeof(float));
            }//if
        } else {
            jack_default_audio_sample_t *audio_out_l = (jack_default_audio_sample_t *)jack_port_get_buffer(output_audio_port_l, nframes);
            jack_default_audio_sample_t *audio_out_r = (jack_default_audio_sample_t *)jack_port_get_buffer(output_audio_port_r, nframes);

            memset(audio_out_l, 0, nframes * sizeof(float));
            memset(audio_out_r, 0, nframes * sizeof(float));
        }//if
    }

    //MIDI
	void* port_buf = jack_port_get_buffer(input_port, nframes);
	void* port_buf_out = jack_port_get_buffer(output_port, nframes);
	jack_midi_event_t in_event;
	jack_nframes_t event_count = jack_midi_get_event_count(port_buf);

    jack_midi_clear_buffer(port_buf_out);

    {
    boost::recursive_mutex::scoped_lock lock(editor.incomingMessagesMutex);
	if (0 < event_count) {
		for(unsigned int i=0; i<event_count; i++) {
			jack_midi_event_get(&in_event, port_buf, i);
//std::cout << "In message" << std::endl;

            //Copy all but footer and checksum
            if (in_event.size > 2) {
                std::vector<jack_midi_data_t> newMessage;
                std::copy(in_event.buffer, in_event.buffer + in_event.size - 2, std::back_inserter(newMessage));
                editor.incomingMessages.push_back(newMessage);
            }//if
        }//for
    } else {
        if (false == editor.incomingMessages.empty()) {
            //FIXME: Do this handling in a different thread
            editor.handleMIDIMessage(editor.incomingMessages[0]);
            editor.incomingMessages.erase(editor.incomingMessages.begin());
        }//if
    }//if
    }//block

    boost::recursive_mutex::scoped_lock lock(editor.pendingMessagesMutex);
    if (editor.pendingMessages.empty() == false) {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        unsigned int curTime = (tv.tv_sec - progStartSeconds) * 1000 + tv.tv_usec / 1000;

//std::cout << "curtime: " << curTime << std::endl;        

        //We'll add a short delay to ensure we don't flood the SonicCell
        if (curTime - lastSendTime > 40) {
//std::cout << "time: " << curTime - lastSendTime << std::endl;
            lastSendTime = curTime;

//std::cout << "send queue: " << editor.pendingMessages.size() << std::endl;

//            std::cout << "message len: " << editor.pendingMessages[0].size() << std::endl;
            unsigned int pos = 0;
            editor.writeMessage(&editor.pendingMessages[pos][0], editor.pendingMessages[pos].size(), pos, 
                                port_buf_out);

            editor.pendingMessages.erase(editor.pendingMessages.begin());
        }//if
    }//if

    return 0;
}//processJack

}//anonymous namespace

void JackSingleton::initJack()
{
    playBuffer.reset(new bounded_buffer<float>(4000000));

    //jackClient = jack_client_new("SonicCell Patch Browser");
    jackClient = jack_client_open("SonicCell Patch Browser", JackNullOption, NULL);

	if (NULL == jackClient) {
        std::cerr << "jack server not running?" << std::endl;
		return;
	}//if

    std::string jackClientName(jack_get_client_name(jackClient));

	jack_set_process_callback (jackClient, &processJack, 0);
	jack_on_shutdown(jackClient, &jack_shutdown, 0);
    jack_set_xrun_callback(jackClient, &processXRun, 0);

	input_port = jack_port_register (jackClient, "midi_in", JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
	output_port = jack_port_register (jackClient, "midi_out", JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);
    input_audio_port_l = jack_port_register (jackClient, "input left", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    input_audio_port_r = jack_port_register (jackClient, "input right", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    output_audio_port_l = jack_port_register (jackClient, "output left", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    output_audio_port_r = jack_port_register (jackClient, "output right", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);

	if (jack_activate (jackClient)) {
        std::cerr << "cannot activate client" << std::endl;
		return;
	}//if
	
	//ALSA stuff
	snd_seq_open( &seq_handle, "hw", SND_SEQ_OPEN_DUPLEX, 0 );
	snd_seq_set_client_name(seq_handle, "SonicCell Patch Browser");
	outPortId = snd_seq_create_simple_port(seq_handle, "midi_out", SND_SEQ_PORT_CAP_READ | SND_SEQ_PORT_CAP_SUBS_READ,  SND_SEQ_PORT_TYPE_APPLICATION);
		
/*    
    const char **ports = jack_get_ports(jackClient, NULL, NULL, 0);

    unsigned int pos = 0;
    while (NULL != ports[pos]) {
        std::string port(ports[pos]);

        std::cout << "Port: " << port << std::endl;

        ++pos;
    }//while
*/
    /*int retVal =*/ jack_connect(jackClient, "SonicCellLibrarian_SYSEX:midi_out", "a2j:SonicCell (playback): SonicCell MIDI 1");
    /*int retVal2 =*/ jack_connect(jackClient, "a2j:SonicCell (capture): SonicCell MIDI 1", "SonicCellLibrarian_SYSEX:midi_in");

    //jack_connect(jackClient, "SonicCell Patch Browser:midi_out", "SonicCell:SonicCell MIDI 1");
    //jack_connect(jackClient, "SonicCell Patch Browser:midi_out", "Midi Through:Midi Through Port-0");
    jack_connect(jackClient, "system:capture_1", (jackClientName + std::string(":input left")).c_str());
    jack_connect(jackClient, "system:capture_2", (jackClientName + std::string(":input right")).c_str());

    jack_connect(jackClient, (jackClientName + std::string(":output left")).c_str(), "system:playback_1");
    jack_connect(jackClient, (jackClientName + std::string(":output right")).c_str(), "system:playback_2");

//    std::cout << retVal << " - " << retVal2 << std::endl;

    struct timeval tv;
    gettimeofday(&tv, NULL);
    progStartSeconds = tv.tv_sec - 1;
    lastSendTime = 0;

    isRecordingSample = false;

    tmpBufferL.resize(10000);
    tmpBufferR.resize(10000);

    xrunDuringRecord = false;
}//initJack

jack_client_t *JackSingleton::getJackClient()
{
    return jackClient;
}//getJackClient

boost::shared_ptr<bounded_buffer<float> > JackSingleton::getPlayBuffer()
{
    return playBuffer;
}//getPlayBuffer

bool JackSingleton::setSampleFile(const std::string &filename)
{
    bool retVal = false;

    if ((true == isRecordingSample) && (filename.empty() == true) && (jackEncoder != NULL)) {
        retVal = xrunDuringRecord;
        isRecordingSample = false;
        jackEncoder->finish();
        jackEncoder.reset();
        std::cout << "Closing flac." << std::endl;
    }//if

    if (filename.empty() == false) {
        jackEncoder.reset(new JackEncoder);

        bool ok = true;

        int channels = 2;
        int bps = 16;
        int sample_rate = 44100;
        int total_samples = 1200 * 1024;
        ok &= jackEncoder->set_verify(true);
    	ok &= jackEncoder->set_compression_level(5);
	    ok &= jackEncoder->set_channels(channels);
    	ok &= jackEncoder->set_bits_per_sample(bps);
    	ok &= jackEncoder->set_sample_rate(sample_rate);
	    ok &= jackEncoder->set_total_samples_estimate(total_samples);

    	/* initialize encoder */
	    if(true == ok) {
            std::cout << "Opening flac: " << filename << std::endl;
		    FLAC__StreamEncoderInitStatus init_status = jackEncoder->init(filename.c_str());
    		if(init_status != FLAC__STREAM_ENCODER_INIT_STATUS_OK) {
                std::cerr << "ERROR: initializing encoder: " << FLAC__StreamEncoderInitStatusString[init_status] << std::endl;
		    	ok = false;
    		}//if
	    }//if

        xrunDuringRecord = false;
        isRecordingSample = true;
    }//if

    return retVal;
}//setSampleFile

void JackSingleton::queueMessage(const jack_midi_data_t *data, size_t size)
{
    for (unsigned int pos = 1; pos < size - 1; ++pos) {
        if (data[pos] >= 0x80) {
            for (unsigned int index = 0; index < size; ++index) {
                std::cout << std::hex << (unsigned int)data[index] << " ";
            }//for
            std::cout << std::dec << " -- " << pos << std::endl;
        }//if
        assert(data[pos] < 0x80);
    }//for

    assert(size < 256);

    std::vector<jack_midi_data_t> newMessage;

    std::copy(data, data+size, std::back_inserter(newMessage));

    boost::recursive_mutex::scoped_lock lock(pendingMessagesMutex);
    pendingMessages.push_back(newMessage);

//usleep(40000);
}//queueMessage

void JackSingleton::addChecksum(std::vector<jack_midi_data_t> &requestBuffer)
{
    unsigned char checksum = 0;

    for (unsigned int checkPos = 7; checkPos < requestBuffer.size()-1; ++checkPos) {
        checksum += requestBuffer[checkPos];
    }//for

    checksum = (128 - (checksum & 0x7F)) & 0x7F;

    requestBuffer.pop_back();
    requestBuffer.push_back(checksum);
    requestBuffer.push_back(0xF7);
}//addChecksum

void JackSingleton::writeMessage(const jack_midi_data_t *data, size_t size, unsigned int offset, void *port_buf_out)
{
    jack_midi_event_t out_event;

    out_event.time = offset;
    out_event.size = size;
    out_event.buffer = const_cast<jack_midi_data_t*>(data);

    /*int ret =*/ jack_midi_event_write(port_buf_out, out_event.time, out_event.buffer, out_event.size);

    snd_seq_event_t ev;

    //Note off
    snd_seq_ev_clear(&ev);
    snd_seq_ev_set_source(&ev, outPortId);
    snd_seq_ev_set_subs(&ev);
    snd_seq_ev_set_direct(&ev);
    
    snd_seq_ev_set_sysex(&ev, size, (void*)data);
    
    snd_seq_event_output(seq_handle, &ev);
    snd_seq_drain_output(seq_handle);
    
/*
std::cout << "Writing message: ";
for (unsigned int pos = 0; pos < out_event.size; ++pos) {
    std::cout << std::hex << (unsigned int)out_event.buffer[pos] << " ";
}//for
std::cout << std::dec << std::endl;

std::cout << "port: " << port_buf_out << " -- time: " << out_event.time << " -- size: " << jack_midi_max_event_size(port_buf_out) << std::endl;
std::cout << "size2: " << size << std::endl << std::endl;
*/
}//writeMessage

