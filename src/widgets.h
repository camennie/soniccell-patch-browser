/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __WIDGETS_H__
#define __WIDGETS_H__

#include <libglademm/xml.h>
#include <gtkmm.h>
#include "data/SonicCell.h"

class SonicCellLibrarian;
class Preferences;

struct LibrarianTableModelColumns : public Gtk::TreeModel::ColumnRecord
{
    LibrarianTableModelColumns()
    {
        add(BankName);
        add(ID);
        add(Name);
        add(Category);
        add(Memo1);
        add(Memo2);
        add(Memo3);
        add(Memo4);
        add(HasSample);
        add(PatchInfoCopy);
    }//constructor

    Gtk::TreeModelColumn<Glib::ustring> BankName;
    Gtk::TreeModelColumn<unsigned int> ID;
    Gtk::TreeModelColumn<Glib::ustring> Name;
    Gtk::TreeModelColumn<Glib::ustring> Category;
    Gtk::TreeModelColumn<Glib::ustring> Memo1;
    Gtk::TreeModelColumn<Glib::ustring> Memo2;
    Gtk::TreeModelColumn<Glib::ustring> Memo3;
    Gtk::TreeModelColumn<Glib::ustring> Memo4;
    Gtk::TreeModelColumn<bool > HasSample;
    Gtk::TreeModelColumn<boost::shared_ptr<SonicCellData::PatchInfo> > PatchInfoCopy;
};//LibrarianTableModelColumns

struct LibrarianModeModelColumns : public Gtk::TreeModel::ColumnRecord
{
    LibrarianModeModelColumns()
    {
        add(ID);
        add(Mode);
    }//constructor

    Gtk::TreeModelColumn<unsigned int> ID;
    Gtk::TreeModelColumn<Glib::ustring> Mode;
};//LibrarianModeModelColumns

class ModelColumns2 : public Gtk::TreeModel::ColumnRecord
{
        public:
                ModelColumns2()
                {
                        add(filename_col);
                }//constructor

                Gtk::TreeModelColumn<Glib::ustring> filename_col;
};//ModelColumns2

struct SonicCellLibrarianWidgets
{
    SonicCellLibrarianWidgets(SonicCellLibrarian *owner_);

    static void createWindow();

    void refreshUI();

    void on_mode_changed();
    void on_readAll_clicked();
    void on_writeAll_clicked();
    void on_readSelected_clicked();
    void on_writeSelected_clicked();
    void on_menuOpen();
    void on_menuSave();
    void on_menuSaveAs();
    void on_menuNew();
    void on_menuQuit();
    void on_menuCut();
    void on_menuCopy();
    void on_menuPaste();
    void on_menuDelete();
    void on_menuPrefs();
    bool on_windowClose(GdkEventAny*);
    void on_selectionChanged();
    bool on_key_pressed(GdkEventKey *event);
    void memo1_on_edited(const Glib::ustring &path_string, const Glib::ustring &new_text);
    void memo2_on_edited(const Glib::ustring &path_string, const Glib::ustring &new_text);
    void memo3_on_edited(const Glib::ustring &path_string, const Glib::ustring &new_text);
    void memo4_on_edited(const Glib::ustring &path_string, const Glib::ustring &new_text);

    void setUpWidgets(Glib::RefPtr<Gnome::Glade::Xml> refXml, Gtk::Window *window);
    void sendIdentityRequest();

    void doPlaySample(bool forceRecord);

    void handleMidiChannelChange();

private:    
    void formatColumn(Gtk::CellRenderer *r, const Gtk::TreeModel::iterator& it);
    
    SonicCellLibrarian *owner;
    Preferences *preferences;
//    unsigned int currentMode;
 
    std::string curFilename;

    LibrarianTableModelColumns infoColumns;
//    LibrarianModeModelColumns modeColumns;

    Gtk::CellRendererText *memo1CellRenderer;
    Gtk::TreeView::Column *memo1TreeViewcolumn;
    Gtk::CellRendererText *memo2CellRenderer;
    Gtk::TreeView::Column *memo2TreeViewcolumn;
    Gtk::CellRendererText *memo3CellRenderer;
    Gtk::TreeView::Column *memo3TreeViewcolumn;
    Gtk::CellRendererText *memo4CellRenderer;
    Gtk::TreeView::Column *memo4TreeViewcolumn;

    Gtk::TreeView *infoTreeView;
    Gtk::Button *readAllButton;
    Gtk::ImageMenuItem *menuOpen;
    Gtk::ImageMenuItem *menuSave;
    Gtk::ImageMenuItem *menuSaveAs;
    Gtk::ImageMenuItem *menuNew;
    Gtk::ImageMenuItem *menuQuit;

    ModelColumns2 categoryBoxColumns;
    ModelColumns2 bankBoxColumns;
    Gtk::ComboBox *categoryComboBox;
    Gtk::ComboBox *bankComboBox;
    Gtk::Entry *filterEntry;

    Gtk::SpinButton *midiChannelSpinButton;
    int curMidiChannel;

    Glib::RefPtr<Gdk::Pixbuf> hasNoSamplePng;
    Glib::RefPtr<Gdk::Pixbuf> hasSamplePng;
};//SonicCellLibrarianWidgets

#endif
