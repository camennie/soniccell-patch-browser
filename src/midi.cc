/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/



#include "main.h"
#include "widgets.h"
#include <iostream>
#include <string>
#include <libglademm/xml.h>
#include <gtkmm.h>
#include <boost/lexical_cast.hpp>
#include "data/Patch.h"
#include "data/PatchCommon.h"
#include "main.h"

namespace
{

bool handleSysExMessage(jack_midi_data_t *data, unsigned int length)
{
     boost::shared_ptr<SonicCellData::SonicCell> baseData = JackSingleton::instance().currentWindow->getSonicCellData();
//    SonicCellLibrarian &editor = SonicCellLibrarian::instance();
//    boost::shared_ptr<SonicCellData::SonicCell> baseData = editor.getSonicCellData();
 
    bool ret = baseData->handleSysExMessage(data, length);

    if (false == ret) {
        std::cout << "Unknown SysEx Message" << std::endl;
    }//if

    return ret;
}//handleSysExMessage

}//anonymous namespace

void JackSingleton::handleMIDIMessage(std::vector<jack_midi_data_t> &inputMessage)
{
    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x12};
    jack_midi_data_t identityResponse[15] = {0xf0, 0x7e, 0x10, 0x06, 0x02, 0x41, 0x25, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf7};

    if (true == std::equal(dataHeader, dataHeader + 7, inputMessage.begin())) {
        bool ret = handleSysExMessage(&inputMessage[7], inputMessage.size() - 7);
        if (ret == true) {
            return;
        }//if        
    }//if

    if (true == std::equal(identityResponse, identityResponse+13, inputMessage.begin())) {
//        std::cout << "We're talking with a Roland SonicCell" << std::endl;
        return;
    }//if

    std::cout << "Unknown MIDI message" << std::endl;
    for (unsigned int pos = 0; pos < inputMessage.size(); ++pos) {
        std::cout << std::hex << (unsigned int)inputMessage[pos] << " ";
    }//for

    std::cout << std::endl;
}//handleMessage




