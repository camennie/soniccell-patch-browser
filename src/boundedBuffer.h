/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <boost/circular_buffer.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/thread.hpp>
#include <boost/progress.hpp>
#include <boost/bind.hpp>

template <class T>
class bounded_buffer 
{

public:

  typedef boost::circular_buffer<T> container_type;
  typedef typename container_type::size_type size_type;
  typedef typename container_type::value_type value_type;

  explicit bounded_buffer(size_type capacity) : m_unread(0), m_container_l(capacity), m_container_r(capacity) {}

  void push_front(const value_type& item, const value_type& item2) 
  {
     boost::mutex::scoped_lock lock(m_mutex);
     m_not_full.wait(lock, boost::bind(&bounded_buffer<value_type>::is_not_full, this));
     m_container_l.push_front(item);
     m_container_r.push_front(item2);
     ++m_unread;
     lock.unlock();
     m_not_empty.notify_one();
  }

  void push_front_no_count_update(const value_type& item, const value_type& item2) 
  {
     m_container_l.push_front(item);
     m_container_r.push_front(item2);
  }
   
  void updateCount()
  {
    boost::mutex::scoped_lock lock(m_mutex);
    m_unread = m_container_l.size();
    lock.unlock();
    m_not_empty.notify_one();
  }//updateCount

  /*
  void pop_back(value_type* pItem) 
  {
     boost::mutex::scoped_lock lock(m_mutex);
     m_not_empty.wait(lock, boost::bind(&bounded_buffer<value_type>::is_not_empty, this));
     *pItem = m_container[--m_unread];
     lock.unlock();
     m_not_full.notify_one();
  }
  */

  void pop_back_lots(int numFrames, value_type *pItems_l, value_type *pItems_r, unsigned int *numItems)
  {
     boost::mutex::scoped_lock lock(m_mutex);

    if (m_unread == 0) {
        *numItems = 0;
        lock.unlock();
        return;
    }//if

     m_not_empty.wait(lock, boost::bind(&bounded_buffer<value_type>::is_not_empty, this));

     numFrames = std::min(numFrames, (int)m_unread);

     for (unsigned int pos = 0; pos < numFrames; ++pos) {
         --m_unread;
         pItems_l[pos] = m_container_l[m_unread];
         pItems_r[pos] = m_container_r[m_unread];
     }//for

     *numItems = numFrames;
     assert(m_unread >= 0);
     lock.unlock();
     m_not_full.notify_one();
  }

    void clear()
    {
        boost::mutex::scoped_lock lock(m_mutex);

        if (is_not_empty() == true) {
            float lastL = m_container_l[0];
            float lastR = m_container_r[0];

            m_container_l.clear();
            m_container_r.clear();

            m_unread = 0;

            m_container_l.push_front(lastL);
            m_container_r.push_front(lastR);
            ++m_unread;

            for (unsigned int pos = 0; pos < 100; ++pos) {
                lastL *= 0.95;
                lastR *= 0.95;

                m_container_l.push_front(lastL);
                m_container_r.push_front(lastR);
                ++m_unread;
            }//for

            m_container_l.push_front(lastL);
            m_container_r.push_front(lastR);
            ++m_unread;
        }//if

        lock.unlock();
        m_not_empty.notify_one();
    }//clear

  bool empty() { return !is_not_empty(); }
  unsigned int size() { return m_unread; }

private:
  bounded_buffer(const bounded_buffer&);              // Disabled copy constructor
  bounded_buffer& operator = (const bounded_buffer&); // Disabled assign operator

  bool is_not_empty() const { return m_unread > 0; }
  bool is_not_full() const { return m_unread < m_container_l.capacity(); }

  size_type m_unread;
  container_type m_container_l;
  container_type m_container_r;
  boost::mutex m_mutex;
  boost::condition m_not_empty;
  boost::condition m_not_full;
};


