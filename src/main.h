/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef __MAIN_H__
#define __MAIN_H__

#include <jack/jack.h>
#include <jack/midiport.h>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <data/SonicCell.h>
#include <data/Patch.h>
#include <gtkmm.h>
#include <libglademm/xml.h>
#include <deque>
#include "boundedBuffer.h"

class SonicCellLibrarianWidgets;
class SonicCellLibrarian;

class JackSingleton
{
    static JackSingleton *JSInstance;

public:    
    jack_client_t *jackClient;

    SonicCellLibrarian *currentWindow;

    boost::recursive_mutex incomingMessagesMutex;
    std::vector<std::vector<jack_midi_data_t> > incomingMessages;

    boost::shared_ptr<bounded_buffer<float> > playBuffer;

    boost::recursive_mutex pendingMessagesMutex;
    std::vector<std::vector<jack_midi_data_t> > pendingMessages; //Not really public, just badly designed

    JackSingleton();
public:
    static JackSingleton &instance();
    ~JackSingleton();

    void writeMessage(const jack_midi_data_t *data, size_t size, unsigned int offset, void *port_buf_out); //ditto

    void initJack();
    jack_client_t *getJackClient();

    bool setSampleFile(const std::string &filename);
    boost::shared_ptr<bounded_buffer<float> > getPlayBuffer();

    static void addChecksum(std::vector<jack_midi_data_t> &requestBuffer);
    void handleMIDIMessage(std::vector<jack_midi_data_t> &in_event);
    void queueMessage(const jack_midi_data_t *data, size_t size);
};//JackSingleton

class SonicCellLibrarian : public Gtk::Window
{
    boost::shared_ptr<SonicCellData::SonicCell> sonicCellData;
    
public:
    SonicCellLibrarian(BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& xml);
    ~SonicCellLibrarian();

    boost::recursive_mutex widgetsMutex;
    boost::shared_ptr<SonicCellLibrarianWidgets> widgets; //ditto

    boost::shared_ptr<SonicCellData::SonicCell> getSonicCellData() const;
};//SonicCellEditor

extern std::vector<SonicCellLibrarianWidgets *> activeWindows;
extern std::vector<boost::shared_ptr<SonicCellData::Patch> > patchCopyBuffer;


#endif
