/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "widgets.h"
#include "main.h"
#include <boost/lexical_cast.hpp>
#include "data/Patch.h"
#include "data/PatchCommon.h"
#include "data/PatchCommonMFX.h"
#include "data/PatchCommonChorus.h"
#include "data/PatchCommonReverb.h"
#include "data/PatchToneMixTable.h"
#include "data/PatchTone.h"
#include "data/SonicCell.h"
#include "Preferences.h"
#include <iostream>
#include <deque>
#include <stdlib.h>
#include <boost/function.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/thread.hpp>
#include <boost/foreach.hpp>
#include <cstdio>
#include <FLAC++/metadata.h>
#include <FLAC++/decoder.h>

namespace
{
 
FLAC__uint64 total_samples = 0;
unsigned sample_rate = 0;
unsigned channels = 0;
unsigned bps = 0;

class JackDecoder: public FLAC::Decoder::File 
{
    boost::shared_ptr<bounded_buffer<float> > playBuffer;
    bool onFirstDecode;
public:
	JackDecoder(): FLAC::Decoder::File() 
    { 
        JackSingleton &jack = JackSingleton::instance();
        playBuffer = jack.getPlayBuffer();
        onFirstDecode = true;
    }//constructor
protected:
	virtual ::FLAC__StreamDecoderWriteStatus write_callback(const ::FLAC__Frame *frame, const FLAC__int32 * const buffer[]);

    virtual void metadata_callback(const ::FLAC__StreamMetadata *metadata) 
    {
        /* print some stats */
        if(metadata->type == FLAC__METADATA_TYPE_STREAMINFO) {
            /* save for later */
            total_samples = metadata->data.stream_info.total_samples;
            sample_rate = metadata->data.stream_info.sample_rate;
            channels = metadata->data.stream_info.channels;
            bps = metadata->data.stream_info.bits_per_sample;

//            fprintf(stderr, "sample rate    : %u Hz\n", sample_rate);
//            fprintf(stderr, "channels       : %u\n", channels);
//            fprintf(stderr, "bits per sample: %u\n", bps);
//            fprintf(stderr, "total samples  : %llu\n", total_samples);
        }
//        std::cout << "metadata_callback" << std::endl; 
    }

	virtual void error_callback(::FLAC__StreamDecoderErrorStatus status) { std::cout << "error_callback" << std::endl; }
};//JackDecoder


bool stopRecordingAllPatches = true;
boost::shared_ptr<boost::function<void (void)> > recordAllThreadStartFunc;
boost::shared_ptr<boost::thread> recordAllThread;

bool stopPlayingSample = false;
boost::shared_ptr<boost::function<void (void)> > playSampleThreadStartFunc;
boost::shared_ptr<boost::thread> playSampleThread;
std::string g_sampleFilename;

boost::array<const char *, 11> banks = {
	"---",
	"GM",
	"PR-A",
	"PR-B",
	"PR-C",
	"PR-D",
	"PR-E",
	"PR-F",
	"PR-G",
	"SRX-08",
	"USER"
    };//banks

    boost::array<const char *, 39> categories = {
                                            "--- / No Assign",
                                            "PNO / AC.Piano",
                                            "EP / EL.Piano",
                                            "KEY / Keyboards",
                                            "BEL / Bell",
                                            "MLT / Mallet",
                                            "ORG / Organ",
                                            "ACD / Accordion",
                                            "HRM / Harmonica",
                                            "AGT / AC.Guitar",
                                            "EGT / EL.Guitar",
                                            "DGT / DIST.Guitar",
                                            "BS / Bass",
                                            "SBS / Synth Bass",
                                            "STR / Strings",
                                            "ORC / Orchestra",
                                            "HIT / Hit&Stab",
                                            "WND / Wind",
                                            "FLT / Flute",
                                            "BRS / AC.Brass",
                                            "SBR / Synth Brass",
                                            "SAX / Sax",
                                            "HLD / Hard Lead",
                                            "SLD / Soft Lead",
                                            "TEK / Techno Synth",
                                            "PLS / Pulsating",
                                            "FX / Synth FX",
                                            "SYN / Other Synth",
                                            "BPD / Bright Pad",
                                            "SPD / Soft Pad",
                                            "VOX / Vox",
                                            "PLK / Plucked",
                                            "ETH / Ethnic",
                                            "FRT / Fretted",
                                            "PRC / Percussion",
                                            "SFX / Sound FX",
                                            "BTS / Beat&Groove",
                                            "DRM / Drums",
                                            "CMB / Combination"
                                            };
    
std::string getCategoryStr(unsigned int category)
{
    

    if (category < categories.size()) {
        return categories[category];
    } else {
        return "UNKOWN";
    }//if
}//getCategoryStr

/*
void requestPatch(SonicCellLibrarian *owner, unsigned int index)
{
    boost::shared_ptr<SonicCellData::SonicCell> baseData = owner->getSonicCellData();
    boost::shared_ptr<SonicCellData::Patch> patch = baseData->getUserPatch(index);

    std::vector<std::vector<jack_midi_data_t> > requests = patch->createUpdateRequest(index);
    for (unsigned int pos = 0; pos < requests.size(); ++pos) {
        std::vector<jack_midi_data_t> request = requests[pos];
        JackSingleton &jack = JackSingleton::instance();
        jack.queueMessage(&request[0], request.size());
    }//for

    baseData->setPatchDirtyFlag(index, true);
    
    owner->widgets->refreshUI();
}//requestPatch

void writePatch(SonicCellLibrarian *owner, unsigned int index)
{
    boost::shared_ptr<SonicCellData::SonicCell> baseData = owner->getSonicCellData();
    boost::shared_ptr<SonicCellData::Patch> patch = baseData->getUserPatch(index);

    std::vector<std::vector<jack_midi_data_t> > requests = patch->createWriteRequest(index);
    for (unsigned int pos = 0; pos < requests.size(); ++pos) {
        std::vector<jack_midi_data_t> request = requests[pos];
        JackSingleton &jack = JackSingleton::instance();
        jack.queueMessage(&request[0], request.size());
    }//for
}//writePatch
*/

void changeSelection(boost::shared_ptr<SonicCellData::PatchInfo> curPatch, int curMidiChannel)
{
    std::vector<jack_midi_data_t> setMSB;
    std::vector<jack_midi_data_t> setLSB;
    std::vector<jack_midi_data_t> programChange;

    setMSB.push_back(0xb0 + curMidiChannel);
    setMSB.push_back(0x00);
    setMSB.push_back(curPatch->patchBankMSB);

    setLSB.push_back(0xb0 + curMidiChannel);
    setLSB.push_back(0x20);
    setLSB.push_back(curPatch->patchBankLSB);

    programChange.push_back(0xc0 + curMidiChannel);
    programChange.push_back(curPatch->patchNum);

    JackSingleton &jack = JackSingleton::instance();
    jack.queueMessage(&setMSB[0], setMSB.size());
    jack.queueMessage(&setLSB[0], setLSB.size());
    jack.queueMessage(&programChange[0], programChange.size());
}//changeSelection

::FLAC__StreamDecoderWriteStatus JackDecoder::write_callback(const ::FLAC__Frame *frame, const FLAC__int32 * const buffer[])
{
    //static int totalcnt = 0;

    float inv16Bit = 1.0f / ((float)0xffff);    

//    totalcnt += frame->header.blocksize;
//    std::cout << "write_callback: " << frame->header.blocksize << " - " << totalcnt << " - " << total_samples - totalcnt << std::endl;

    if (true == onFirstDecode) {
        onFirstDecode = false;
        playBuffer->clear();
        while (playBuffer->empty() == false);

        for (size_t i = 0; i < frame->header.blocksize; i++) {            
            float left = buffer[0][i] * inv16Bit;
            float right = buffer[1][i] * inv16Bit;

            playBuffer->push_front_no_count_update(left, right);
        }//for

        playBuffer->updateCount();
    } else {
        for (size_t i = 0; i < frame->header.blocksize; i++) {
            if (true == stopPlayingSample) {
                playBuffer->clear();
                return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
            }//if

            float left = buffer[0][i] * inv16Bit;
            float right = buffer[1][i] * inv16Bit;

            playBuffer->push_front(left, right);
        }//for
    }//if

	return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
}//write_callback

void playSampleThreadFunc()
{
    stopPlayingSample = false;

//    std::cout << "in playSample" << std::endl;

    bool ok = true;
    JackDecoder decoder;
    
	FLAC__StreamDecoderInitStatus init_status = decoder.init(g_sampleFilename.c_str());
	if (init_status != FLAC__STREAM_DECODER_INIT_STATUS_OK) {
        std::cerr << "ERROR: initializing decoder: " << FLAC__StreamDecoderInitStatusString[init_status] << std::endl;
		ok = false;
	}//if

	if (ok) {
		ok = decoder.process_until_end_of_stream();
//        std::cerr << "decoding: " << (ok? "succeeded" : "FAILED") << std::endl;
//        std::cerr << "   state: " << decoder.get_state().resolved_as_cstring(decoder) << std::endl;
	}//if

//    std::cout << "Finished playing sample: " << g_sampleFilename << std::endl;
}//playSampleThreadFunc

void recordSample(std::string filename, Preferences *preferences)
{
    std::cout << "in recordSample" << std::endl;

    JackSingleton &jack = JackSingleton::instance();
    jack.setSampleFile(filename);

    std::cout << "Recording: " << filename << " with command: '" << preferences->getPlayMidiCommand() << "'" << std::endl;
    
    system(preferences->getPlayMidiCommand().c_str());

//boost::this_thread::sleep(boost::posix_time::milliseconds(1000 * 15));

    std::cout << "Finished recording: " << filename << std::endl;
}//recordSample

std::string getSampleFilename(boost::shared_ptr<SonicCellData::PatchInfo> curPatch, Preferences *preferences)
{
    std::string patchName = curPatch->patchName;
    for (unsigned int pos = 0; pos < patchName.size(); ++pos) {
        if (patchName[pos] == '/') {
            patchName[pos] = '_';
        }//if
    }//for

    std::string patchNum = boost::lexical_cast<std::string>(curPatch->actualPatchNum);

    if ((curPatch->bankName != "PR-A") && (curPatch->bankName != "PR-B")) {
        if (curPatch->actualPatchNum < 100) {
            patchNum = "0" + patchNum;
        }//if

        if (curPatch->actualPatchNum < 10) {
            patchNum = "0" + patchNum;
        }//if
    }//if

    std::string filename =  curPatch->bankName + std::string("-") + patchNum + std::string("-") + patchName + std::string(".flac");
    filename = preferences->getSamplePath() + std::string("/") + filename;
    return filename;
}//getSampleFilename

void deleteFile(std::string filename)
{
    std::remove(filename.c_str());
}//deleteFile

void recordAllThreads(SonicCellLibrarian *librarian, Preferences *preferences, int curMidiChannel)
{
    JackSingleton &jack = JackSingleton::instance();
    boost::shared_ptr<SonicCellData::SonicCell> sonicCell = librarian->getSonicCellData();
    std::vector<boost::shared_ptr<SonicCellData::PatchInfo> > currentPatchData = sonicCell->getAllPatchInfos();
    BOOST_FOREACH (boost::shared_ptr<SonicCellData::PatchInfo> curPatch, currentPatchData) {
redo:        
        if (true == stopRecordingAllPatches) {
            break;
        }//if

        if (curPatch->category == 37) { //drums
            continue;
        }//if

        std::string patchFilename = getSampleFilename(curPatch, preferences);
        struct stat buffer;
        if (stat(patchFilename.c_str(), &buffer) != 0) {
            changeSelection(curPatch, curMidiChannel);
            boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

            recordSample(patchFilename, preferences);

            bool hasXRun = jack.setSampleFile("");
            if (true == hasXRun) {
                std::cout << "Had XRUN" << std::endl;
                deleteFile(patchFilename);
                goto redo;
            }//if

            std::cout << "Auto sample record finished recording: " << patchFilename << std::endl;
        }//if
    }//foreach

    std::cout << "Exiting recordAllThreads" << std::endl;
}//recordAllThreads

void checkForSamples(SonicCellLibrarian *librarian, Preferences *preferences)
{
    boost::shared_ptr<SonicCellData::SonicCell> sonicCell = librarian->getSonicCellData();
    std::vector<boost::shared_ptr<SonicCellData::PatchInfo> > currentPatchData = sonicCell->getAllPatchInfos();
    BOOST_FOREACH (boost::shared_ptr<SonicCellData::PatchInfo> curPatch, currentPatchData) {
        std::string patchFilename = getSampleFilename(curPatch, preferences);
        struct stat buffer;
        if (stat(patchFilename.c_str(), &buffer) != 0) {
            curPatch->hasSample = false;
        } else {
            curPatch->hasSample = true;
        }//if
    }//if

}//checkForSamples

}//anonymous namespace

SonicCellLibrarianWidgets::SonicCellLibrarianWidgets(SonicCellLibrarian *owner_)
{
//    currentMode = 0;
    owner = owner_;
    curMidiChannel = 0;
}//constructor

void SonicCellLibrarianWidgets::createWindow()
{
    SonicCellLibrarian *mainWindow = NULL;
    Glib::RefPtr<Gnome::Glade::Xml> refXml = Gnome::Glade::Xml::create("SonicCellLibrarian.glade");
    refXml->get_widget_derived("PatchWindow", mainWindow);
    assert(mainWindow != NULL);

    mainWindow->present();

    JackSingleton::instance().currentWindow = mainWindow;
}//createWindow

void SonicCellLibrarianWidgets::setUpWidgets(Glib::RefPtr<Gnome::Glade::Xml> refXml, Gtk::Window *window)
{
    preferences = new Preferences(refXml);

    activeWindows.push_back(this);

    refXml->get_widget("midiChannelSpinButton", midiChannelSpinButton);
    midiChannelSpinButton->signal_value_changed().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::handleMidiChannelChange));

    refXml->get_widget("readAllButton", readAllButton);
    refXml->get_widget("MainTreeView", infoTreeView);

    refXml->get_widget("menu_open", menuOpen);
    refXml->get_widget("menu_save", menuSave);
    refXml->get_widget("menu_saveas", menuSaveAs);
    refXml->get_widget("menu_new", menuNew);
    refXml->get_widget("menu_quit", menuQuit);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Gtk::ListStore::create(infoColumns);
    this->infoTreeView->set_model(infoTreeViewListStore);
    
    infoTreeView->modify_base(Gtk::STATE_NORMAL, Gdk::Color("#999999")); 

    infoTreeView->append_column("Bank", infoColumns.BankName);
    infoTreeView->append_column("ID", infoColumns.ID);
    infoTreeView->append_column("Name", infoColumns.Name);
    infoTreeView->append_column("Cat", infoColumns.Category);

    Gtk::TreeViewColumn *bankColumn = infoTreeView->get_column(0);
    bankColumn->set_resizable();
    bankColumn->set_sort_indicator(true);
    bankColumn->set_sort_column(infoColumns.BankName);
    Gtk::TreeViewColumn *idColumn = infoTreeView->get_column(1);
    idColumn->set_resizable();
    idColumn->set_sort_indicator(true);
    idColumn->set_sort_column(infoColumns.ID);
    Gtk::TreeViewColumn *nameColumn = infoTreeView->get_column(2);
    nameColumn->set_resizable();
    Gtk::TreeViewColumn *catColumn = infoTreeView->get_column(3);
    catColumn->set_resizable();
    catColumn->set_sort_indicator(true);
    catColumn->set_sort_column(infoColumns.Category);

    Gtk::CellRenderer *renderer;
    
    renderer = bankColumn->get_first_cell_renderer();
    bankColumn->set_cell_data_func(*renderer, sigc::mem_fun(*this, &SonicCellLibrarianWidgets::formatColumn));
    renderer = idColumn->get_first_cell_renderer();
    idColumn->set_cell_data_func(*renderer, sigc::mem_fun(*this, &SonicCellLibrarianWidgets::formatColumn));
    renderer = nameColumn->get_first_cell_renderer();
    nameColumn->set_cell_data_func(*renderer, sigc::mem_fun(*this, &SonicCellLibrarianWidgets::formatColumn));
    renderer = catColumn->get_first_cell_renderer();
    catColumn->set_cell_data_func(*renderer, sigc::mem_fun(*this, &SonicCellLibrarianWidgets::formatColumn));

///////////////////

    infoTreeView->append_column_editable("Memo1", infoColumns.Memo1);
    infoTreeView->append_column_editable("Memo2", infoColumns.Memo2);
    infoTreeView->append_column_editable("Memo3", infoColumns.Memo3);
    infoTreeView->append_column_editable("Memo4", infoColumns.Memo4);
    infoTreeView->append_column("Sample", infoColumns.HasSample);

    Gtk::TreeViewColumn *memo1Column = infoTreeView->get_column(4);
    memo1Column->set_resizable();
    Gtk::CellRendererText *memo1Renderer = (Gtk::CellRendererText*)memo1Column->get_first_cell_renderer();
    memo1Renderer->signal_edited().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::memo1_on_edited));

    Gtk::TreeViewColumn *memo2Column = infoTreeView->get_column(5);
    memo2Column->set_resizable();
    Gtk::CellRendererText *memo2Renderer = (Gtk::CellRendererText*)memo2Column->get_first_cell_renderer();
    memo2Renderer->signal_edited().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::memo2_on_edited));

    Gtk::TreeViewColumn *memo3Column = infoTreeView->get_column(6);
    memo3Column->set_resizable();
    Gtk::CellRendererText *memo3Renderer = (Gtk::CellRendererText*)memo3Column->get_first_cell_renderer();
    memo3Renderer->signal_edited().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::memo3_on_edited));

    Gtk::TreeViewColumn *memo4Column = infoTreeView->get_column(7);
    memo4Column->set_resizable();
    Gtk::CellRendererText *memo4Renderer = (Gtk::CellRendererText*)memo4Column->get_first_cell_renderer();
    memo4Renderer->signal_edited().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::memo4_on_edited));

    Gtk::TreeViewColumn *hasSampleColumn = infoTreeView->get_column(8);
    //hasSampleColumn->set_resizable();
    //Gtk::CellRendererText *hasSampleColumnRenderer = (Gtk::CellRendererText*)hasSampleColumn->get_first_cell_renderer();
    
    renderer = memo1Column->get_first_cell_renderer();
    memo1Column->set_cell_data_func(*renderer, sigc::mem_fun(*this, &SonicCellLibrarianWidgets::formatColumn));
    renderer = memo2Column->get_first_cell_renderer();
    memo2Column->set_cell_data_func(*renderer, sigc::mem_fun(*this, &SonicCellLibrarianWidgets::formatColumn));
    renderer = memo3Column->get_first_cell_renderer();
    memo3Column->set_cell_data_func(*renderer, sigc::mem_fun(*this, &SonicCellLibrarianWidgets::formatColumn));
    renderer = memo4Column->get_first_cell_renderer();
    memo4Column->set_cell_data_func(*renderer, sigc::mem_fun(*this, &SonicCellLibrarianWidgets::formatColumn));

    renderer = hasSampleColumn->get_first_cell_renderer();
    hasSampleColumn->set_cell_data_func(*renderer, sigc::mem_fun(*this, &SonicCellLibrarianWidgets::formatColumn));


    readAllButton->signal_clicked().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::on_readAll_clicked) );

    menuOpen->signal_activate().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::on_menuOpen));
    menuSave->signal_activate().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::on_menuSave));
    menuSaveAs->signal_activate().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::on_menuSaveAs));
    menuNew->signal_activate().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::on_menuNew));
    menuQuit->signal_activate().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::on_menuQuit));

    Gtk::ImageMenuItem *menuItem;
    refXml->get_widget("menu_prefs", menuItem);
    menuItem->signal_activate().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::on_menuPrefs));

    window->signal_delete_event().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::on_windowClose));

    window->signal_key_press_event().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::on_key_pressed));
    infoTreeView->signal_key_press_event().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::on_key_pressed));
    infoTreeView->signal_cursor_changed().connect(sigc::mem_fun(*this, &SonicCellLibrarianWidgets::on_selectionChanged), false);
    
    refXml->get_widget("filterEntry", filterEntry);
    refXml->get_widget("bankComboBox", bankComboBox);
    refXml->get_widget("categoryComboBox", categoryComboBox);
    
    Glib::RefPtr<Gtk::ListStore> comboListStore = Gtk::ListStore::create(categoryBoxColumns);
    categoryComboBox->set_model(comboListStore);
    categoryComboBox->pack_start(categoryBoxColumns.filename_col);
    
    std::set<std::string> categoriesSet;
    
    for (unsigned int pos = 0; pos < categories.size(); ++pos) {
	    categoriesSet.insert(categories[pos]);
    }//for
    
    for (std::set<std::string>::const_iterator setIter = categoriesSet.begin(); setIter != categoriesSet.end(); ++setIter) {
    	Gtk::TreeModel::Row row = *(comboListStore->append());
	    row[categoryBoxColumns.filename_col] = *setIter;
    }//for
    
    categoryComboBox->set_active(0);
    
    Glib::RefPtr<Gtk::ListStore> banksListStore = Gtk::ListStore::create(bankBoxColumns);
    bankComboBox->set_model(banksListStore);
    bankComboBox->pack_start(bankBoxColumns.filename_col);
    
    for (unsigned int pos = 0; pos < banks.size(); ++pos) {
    	Gtk::TreeModel::Row row = *(banksListStore->append());
    	row[bankBoxColumns.filename_col] = banks[pos];
    }//for
    
    bankComboBox->set_active(0);
        
    categoryComboBox->signal_changed().connect( sigc::mem_fun(*this, &SonicCellLibrarianWidgets::refreshUI) );
    bankComboBox->signal_changed().connect( sigc::mem_fun(*this, &SonicCellLibrarianWidgets::refreshUI) );
    filterEntry->signal_changed().connect( sigc::mem_fun(*this, &SonicCellLibrarianWidgets::refreshUI) );

    filterEntry->modify_base(Gtk::STATE_NORMAL, Gdk::Color("#dddddd"));
 
    hasNoSamplePng = Gdk::Pixbuf::create_from_file("./reload.png");
    hasSamplePng = Gdk::Pixbuf::create_from_file("./check.png");

    ///////////////////////////
    std::string filename = "CurrentSet-1.xml";
    boost::shared_ptr<SonicCellData::SonicCell> baseData = owner->getSonicCellData();
    baseData->loadFromFile(filename);
    curFilename = filename;
    checkForSamples(owner, preferences);

    refreshUI();
}//setUpWidgets

void SonicCellLibrarianWidgets::handleMidiChannelChange()
{
    curMidiChannel = midiChannelSpinButton->get_value_as_int();
}//handleMidiChannelChange

void SonicCellLibrarianWidgets::doPlaySample(bool forceRecord)
{
    Glib::RefPtr<Gtk::TreeView::Selection> selection = infoTreeView->get_selection();
    Glib::ListHandle<Gtk::TreePath, Gtk::TreePath_Traits> rows = selection->get_selected_rows();

    Glib::Container_Helpers::ListHandleIterator<Gtk::TreePath_Traits> iter = rows.begin();

    Gtk::TreePath path = *iter;
    Glib::ustring pathStr = path.to_string();
//    unsigned int row = boost::lexical_cast<unsigned int>(pathStr);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());

    Gtk::TreeModel::Row curRow = *(infoTreeViewListStore->get_iter(path));

    boost::shared_ptr<SonicCellData::PatchInfo> curPatch = curRow[infoColumns.PatchInfoCopy];

    std::string filename = getSampleFilename(curPatch, preferences);

    struct stat buffer;
    if ((false == forceRecord) && (stat(filename.c_str(), &buffer) == 0)) {
        curRow[infoColumns.HasSample] = true;

        boost::shared_ptr<boost::function<void (void)> > playSampleThreadStartFunc;
        boost::shared_ptr<boost::thread> playSampleThread;

        g_sampleFilename = filename;
        playSampleThreadStartFunc.reset(new boost::function<void (void)>(boost::lambda::bind(&playSampleThreadFunc)));
        playSampleThread.reset(new boost::thread(*playSampleThreadStartFunc));
    } else {
        std::cout << "Didn't find sample '" << filename << "': " << forceRecord << std::endl;

        /*
        JackSingleton &jack = JackSingleton::instance();

        recordSample(filename, preferences);

        bool hasXRun = jack.setSampleFile("");
        if (true == hasXRun) {
            std::cout << "Had XRUN" << std::endl;
            deleteFile(filename);
        }//if
        curRow[infoColumns.HasSample] = true;
        */
    }//if

    std::cout << "'" << filename << "'" << std::endl;
}//doPlaySample

bool SonicCellLibrarianWidgets::on_key_pressed(GdkEventKey *event)
{
    char key = *(event->string);

    if (key == 's') {
        JackSingleton &jack = JackSingleton::instance();
        jack.getPlayBuffer()->clear();
        stopPlayingSample = true;
        if (playSampleThread != NULL) {
            playSampleThread->join();
            playSampleThread.reset();
        }//if
        doPlaySample(false);
    }//if

    if (key == '^') {
        if (recordAllThread == NULL) {
            stopRecordingAllPatches = false;
            recordAllThreadStartFunc.reset(new boost::function<void (void)>(boost::lambda::bind(&recordAllThreads, boost::lambda::var(owner), boost::lambda::var(preferences), boost::lambda::var(curMidiChannel))));
            recordAllThread.reset(new boost::thread(*recordAllThreadStartFunc));
        }//if
    }//if

    if (key == '`') {
        if (stopRecordingAllPatches == false) {
            std::cout << "Stopping after next patch" << std::endl;
        }//if
        stopRecordingAllPatches = true;

        if (recordAllThread != NULL) {
            recordAllThread->join();
            checkForSamples(owner, preferences);
            refreshUI();
        }//if
    }//if

    return true;
}//on_key_pressed

bool SonicCellLibrarianWidgets::on_windowClose(GdkEventAny *)
{
    std::vector<SonicCellLibrarianWidgets *>::iterator windowIter = std::find(activeWindows.begin(), activeWindows.end(), this);

    if (windowIter != activeWindows.end()) {
        activeWindows.erase(windowIter);
    }//if

    if (true == activeWindows.empty()) {
        Gtk::Main::quit();
    }//if

    return false;
}//on_windowClose

void SonicCellLibrarianWidgets::on_selectionChanged()
{
    Glib::RefPtr<Gtk::TreeView::Selection> selection = infoTreeView->get_selection();
    Glib::ListHandle<Gtk::TreePath, Gtk::TreePath_Traits> rows = selection->get_selected_rows();

    Glib::Container_Helpers::ListHandleIterator<Gtk::TreePath_Traits> iter = rows.begin();

    Gtk::TreePath path = *iter;
    Glib::ustring pathStr = path.to_string();
//    unsigned int row = boost::lexical_cast<unsigned int>(pathStr);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());

    Gtk::TreeModel::Row curRow = *(infoTreeViewListStore->get_iter(path));

    boost::shared_ptr<SonicCellData::PatchInfo> curPatch = curRow[infoColumns.PatchInfoCopy];
    changeSelection(curPatch, curMidiChannel);

//    std::cout << "Selected: " << curPatch->patchName << std::endl;
}//on_selectionChanged

void SonicCellLibrarianWidgets::on_menuQuit()
{
    Gtk::Main::quit();
}//on_menuQuit

void SonicCellLibrarianWidgets::on_menuPrefs()
{
    preferences->show();
}//on_menuPrefs

void SonicCellLibrarianWidgets::on_menuNew()
{
    SonicCellLibrarianWidgets::createWindow();
}//on_menuNew

void SonicCellLibrarianWidgets::on_menuSave()
{
    if (false == curFilename.empty()) {
        boost::shared_ptr<SonicCellData::SonicCell> baseData = owner->getSonicCellData();
        baseData->saveToFile(curFilename);

        for (unsigned int index = 0; index < 256; ++index) {
//            baseData->setPatchDirtyFlag(index, false);
        }//for
    } else {
        on_menuSaveAs();
    }//if

    refreshUI();
}//on_menuSave

void SonicCellLibrarianWidgets::on_menuSaveAs()
{
    Gtk::FileChooserDialog dialog("Save...", Gtk::FILE_CHOOSER_ACTION_SAVE);
//    dialog.set_transient_for(*this);
    dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);

    Gtk::FileFilter filter_any;
    filter_any.set_name("Any files");
    filter_any.add_pattern("*");
    dialog.add_filter(filter_any);

    int result = dialog.run();
    switch(result) {
        case(Gtk::RESPONSE_OK):
        {
            std::string filename = dialog.get_filename();
            boost::shared_ptr<SonicCellData::SonicCell> baseData = owner->getSonicCellData();
            baseData->saveToFile(filename);
            curFilename = filename;

//            for (unsigned int index = 0; index < 256; ++index) {
//                baseData->setPatchDirtyFlag(index, false);
//            }//for
            break;
        }
        case(Gtk::RESPONSE_CANCEL):
            break;
        default:
            break;
    }//switch

    refreshUI();
}//on_menuSave

void SonicCellLibrarianWidgets::on_menuOpen()
{
     Gtk::FileChooserDialog dialog("Load", Gtk::FILE_CHOOSER_ACTION_OPEN);
//    dialog.set_transient_for(*this);
    dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);

    Gtk::FileFilter filter_any;
    filter_any.set_name("Any files");
    filter_any.add_pattern("*");
    dialog.add_filter(filter_any);

    int result = dialog.run();
    switch(result) {
        case(Gtk::RESPONSE_OK):
        {
            std::string filename = dialog.get_filename();
            boost::shared_ptr<SonicCellData::SonicCell> baseData = owner->getSonicCellData();
            baseData->loadFromFile(filename);
            curFilename = filename;
            checkForSamples(owner, preferences);

//            for (unsigned int index = 0; index < 256; ++index) {
//                baseData->setPatchDirtyFlag(index, false);
//            }//for
            break;
        }
        case(Gtk::RESPONSE_CANCEL):
            break;
        default:
            break;
    }//switch 

    refreshUI();    
}//on_menuOpen

void SonicCellLibrarianWidgets::on_readAll_clicked()
{
    JackSingleton &jack = JackSingleton::instance();
    jack.currentWindow = owner;

    sendIdentityRequest();

    std::vector<std::vector<jack_midi_data_t> > requests = owner->getSonicCellData()->createUpdateRequest();

    for (unsigned int pos = 0; pos < requests.size(); ++pos) {
        std::vector<jack_midi_data_t> request = requests[pos];
        JackSingleton &jack = JackSingleton::instance();
        jack.queueMessage(&request[0], request.size());
    }//for

    while (jack.pendingMessages.empty() == false) {
        sleep(1);
    }//while

    sleep(5);
    refreshUI();
}//on_readAll_clicked

void SonicCellLibrarianWidgets::memo1_on_edited(const Glib::ustring &path_string, const Glib::ustring &new_text)
{
    Glib::RefPtr<Gtk::TreeView::Selection> selection = infoTreeView->get_selection();
    Glib::ListHandle<Gtk::TreePath, Gtk::TreePath_Traits> rows = selection->get_selected_rows();

    Glib::Container_Helpers::ListHandleIterator<Gtk::TreePath_Traits> iter = rows.begin();

    Gtk::TreePath path = *iter;
    Glib::ustring pathStr = path.to_string();
 //   unsigned int row = boost::lexical_cast<unsigned int>(pathStr);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());

    Gtk::TreeModel::Row curRow = *(infoTreeViewListStore->get_iter(path));

    boost::shared_ptr<SonicCellData::PatchInfo> curPatch = curRow[infoColumns.PatchInfoCopy];

    curPatch->memo1 = new_text;
}//memo1_on_edited

void SonicCellLibrarianWidgets::memo2_on_edited(const Glib::ustring &path_string, const Glib::ustring &new_text)
{
    Glib::RefPtr<Gtk::TreeView::Selection> selection = infoTreeView->get_selection();
    Glib::ListHandle<Gtk::TreePath, Gtk::TreePath_Traits> rows = selection->get_selected_rows();

    Glib::Container_Helpers::ListHandleIterator<Gtk::TreePath_Traits> iter = rows.begin();

    Gtk::TreePath path = *iter;
    Glib::ustring pathStr = path.to_string();
//    unsigned int row = boost::lexical_cast<unsigned int>(pathStr);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());

    Gtk::TreeModel::Row curRow = *(infoTreeViewListStore->get_iter(path));

    boost::shared_ptr<SonicCellData::PatchInfo> curPatch = curRow[infoColumns.PatchInfoCopy];

    curPatch->memo2 = new_text;
}//memo2_on_edited

void SonicCellLibrarianWidgets::memo3_on_edited(const Glib::ustring &path_string, const Glib::ustring &new_text)
{
    Glib::RefPtr<Gtk::TreeView::Selection> selection = infoTreeView->get_selection();
    Glib::ListHandle<Gtk::TreePath, Gtk::TreePath_Traits> rows = selection->get_selected_rows();

    Glib::Container_Helpers::ListHandleIterator<Gtk::TreePath_Traits> iter = rows.begin();

    Gtk::TreePath path = *iter;
    Glib::ustring pathStr = path.to_string();
  //  unsigned int row = boost::lexical_cast<unsigned int>(pathStr);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());

    Gtk::TreeModel::Row curRow = *(infoTreeViewListStore->get_iter(path));

    boost::shared_ptr<SonicCellData::PatchInfo> curPatch = curRow[infoColumns.PatchInfoCopy];

    curPatch->memo3 = new_text;
}//memo3_on_edited

void SonicCellLibrarianWidgets::memo4_on_edited(const Glib::ustring &path_string, const Glib::ustring &new_text)
{
    Glib::RefPtr<Gtk::TreeView::Selection> selection = infoTreeView->get_selection();
    Glib::ListHandle<Gtk::TreePath, Gtk::TreePath_Traits> rows = selection->get_selected_rows();

    Glib::Container_Helpers::ListHandleIterator<Gtk::TreePath_Traits> iter = rows.begin();

    Gtk::TreePath path = *iter;
    Glib::ustring pathStr = path.to_string();
    //unsigned int row = boost::lexical_cast<unsigned int>(pathStr);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());

    Gtk::TreeModel::Row curRow = *(infoTreeViewListStore->get_iter(path));

    boost::shared_ptr<SonicCellData::PatchInfo> curPatch = curRow[infoColumns.PatchInfoCopy];

    curPatch->memo4 = new_text;
}//memo4_on_edited

void SonicCellLibrarianWidgets::refreshUI()
{
    Glib::ustring curCategory;
    Glib::ustring curBank;
    std::string filterText;
    
    filterText = filterEntry->get_text();
    transform(filterText.begin(), filterText.end(), filterText.begin(), tolower);
    
    Gtk::TreeModel::const_iterator categoryIter = categoryComboBox->get_active();
    Gtk::TreeModel::Row row = *categoryIter;
    curCategory = row[categoryBoxColumns.filename_col];
    
    Gtk::TreeModel::const_iterator bankIter = bankComboBox->get_active();
    row = *bankIter;
    curBank = row[bankBoxColumns.filename_col];
    
    boost::shared_ptr<SonicCellData::SonicCell> baseData = owner->getSonicCellData();
    std::vector<boost::shared_ptr<SonicCellData::PatchInfo> > patches = baseData->getAllPatchInfos();

    std::string title = std::string("SonicCell Patch Browser - ") + curFilename;

    bool isDirty = false;
//    for (unsigned int index = 0; index < 256; ++index) {
//        if (true == baseData->getPatchDirtyFlag(index)) {
//            isDirty = true;
//            break;
//        }//if
//    }//for

    if (true == isDirty) {
        title = title + std::string(" *");
    }//if

    owner->set_title(title);

    boost::recursive_mutex::scoped_lock lock(owner->widgetsMutex);

//    modeBox->set_active(currentMode);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());
    infoTreeViewListStore->clear();

    for (unsigned int pos = 0; pos < patches.size(); ++pos) {
        std::string categoryStr = getCategoryStr(patches[pos]->category);
        
        if ((curCategory != categories[0]) && (curCategory != categoryStr)) {
            continue;
        }//if
        
        if ((curBank != banks[0]) && (curBank != patches[pos]->bankName)) {
            continue;
        }//if
        
        std::string patchName = patches[pos]->patchName;
        transform(patchName.begin(), patchName.end(), patchName.begin(), tolower);
        if ((filterText.length() > 0) && (patchName.find(filterText) == std::string::npos)) {
            continue;
        }//if
	
        Gtk::TreeModel::Row newRow = *(infoTreeViewListStore->append());
        newRow[infoColumns.ID] = pos + 1;
        
        newRow[infoColumns.Category] = categoryStr;

        newRow[infoColumns.Name] = patches[pos]->patchName;

        newRow[infoColumns.BankName] = patches[pos]->bankName;
        newRow[infoColumns.ID] = patches[pos]->actualPatchNum;

        newRow[infoColumns.PatchInfoCopy] = patches[pos];

//        if (std::numeric_limits<unsigned int>::max() == category) {
//            newRow[infoColumns.Category] = "CAT";
//        }//if

//        boost::shared_ptr<SonicCellData::Patch> patch = baseData->getUserPatch(pos);

//        newRow.set_value(3, "BLAH"); //patch->getMemo(0));
//        newRow[*memo2TreeViewcolumn] = patch->getMemo(1);
//        newRow[*memo3TreeViewcolumn] = patch->getMemo(2);
//        newRow[*memo4TreeViewcolumn] = patch->getMemo(3);

        newRow[infoColumns.Memo1] = patches[pos]->memo1;
        newRow[infoColumns.Memo2] = patches[pos]->memo2;
        newRow[infoColumns.Memo3] = patches[pos]->memo3;
        newRow[infoColumns.Memo4] = patches[pos]->memo4;

        newRow[infoColumns.HasSample] = patches[pos]->hasSample;
    }//for
}//refreshUI

void SonicCellLibrarianWidgets::sendIdentityRequest()
{
    jack_midi_data_t data[6] = {0xf0, 0x7e, 0x10, 0x06, 0x01, 0xf7};
    JackSingleton &jack = JackSingleton::instance();
    jack.queueMessage(data, 6);

    refreshUI();
}//sendIdentityRequest

void SonicCellLibrarianWidgets::formatColumn(Gtk::CellRenderer *r, const Gtk::TreeModel::iterator& it)
{
    Glib::RefPtr<const Gtk::TreeModel> treeModel = infoTreeView->get_model();
    Gtk::TreeModel::Path path = treeModel->get_path(it);
    int row = path[0];
    
    if (row % 2) {
	r->property_cell_background() = "#aaaaaa";
    } else {
	r->property_cell_background() = "#dddddd";
    }//if
}//formatColumn

