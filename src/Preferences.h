/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __PREFERENCES_H
#define __PREFERENCES_H

#include <libglademm/xml.h>
#include <gtkmm.h>
#include <boost/array.hpp>
#include <vector>
#include <string>
#include <jack/midiport.h>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/access.hpp>


class Preferences
{

    std::string samplePath;
    std::string playMidiCommand;
    Gtk::Entry *samplePathEntry;
    Gtk::Entry *playMidiCommandEntry;

    Gtk::Dialog *preferencesDialog;

public:
    Preferences(Glib::RefPtr<Gnome::Glade::Xml> refXml);
    ~Preferences();

    void save();

    void show();

    std::string getSamplePath() const;
    std::string getPlayMidiCommand() const;

    template<class Archive> void serialize(Archive &ar, const unsigned int version);
	friend class boost::serialization::access;
};//Preferences

#endif

