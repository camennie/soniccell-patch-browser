/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <boost/shared_ptr.hpp>
#include <jack/jack.h>
#include <iostream>
#include <libglademm/xml.h>
#include <gtkmm.h>
#include "main.h"
#include "widgets.h"
#include <sigc++/sigc++.h>

std::vector<SonicCellLibrarianWidgets *> activeWindows;
std::vector<boost::shared_ptr<SonicCellData::Patch> > patchCopyBuffer;

JackSingleton *JackSingleton::JSInstance = NULL;

namespace 
{


}//anonymous namespace

int main(int argc, char **argv)
{
    Gtk::Main gtkMain(argc, argv);

    JackSingleton::instance().initJack();

    if (NULL == JackSingleton::instance().getJackClient()) {
        std::cerr << "Couldn't create JACK ports. Exiting." << std::endl;
 ////       return -1;
    }//if


    SonicCellLibrarianWidgets::createWindow();

    boost::shared_ptr<SonicCellData::SonicCell> baseData = JackSingleton::instance().currentWindow->getSonicCellData();
    JackSingleton::instance().currentWindow->widgets->sendIdentityRequest();

    boost::array<jack_midi_data_t, 14> patchMode = {0xF0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x12, 0x01, 0x00, 0x00, 0x00, 0x00, 0x7F, 0xF7};
    JackSingleton::instance().queueMessage(&patchMode[0], patchMode.size());

//    baseData->checkForSRXs();

/*
    for (unsigned char bank = 0; bank < 7; ++bank) {
        std::vector<jack_midi_data_t> setMSB;
        std::vector<jack_midi_data_t> setLSB;
        std::vector<jack_midi_data_t> programChange;

        setMSB.push_back(0xb0);
        setMSB.push_back(0x00);
        setMSB.push_back(87);

        setLSB.push_back(0xb0);
        setLSB.push_back(0x20);
        setLSB.push_back(64 + bank);

        programChange.push_back(0xc0);
        programChange.push_back(0x00);

        JackSingleton::instance().queueMessage(&setMSB[0], 3);
        JackSingleton::instance().queueMessage(&setLSB[0], 3);
        JackSingleton::instance().queueMessage(&programChange[0], 2);

        sleep(2);
    }//for
*/
    
    /*
    //GM -- can't get high values..
    for (unsigned char bank = 0; bank < 5; ++bank) {
        std::vector<jack_midi_data_t> setMSB;
        std::vector<jack_midi_data_t> setLSB;
        std::vector<jack_midi_data_t> programChange;

        setMSB.push_back(0xb0);
        setMSB.push_back(0x00);
        setMSB.push_back(121);

        setLSB.push_back(0xb0);
        setLSB.push_back(0x20);
        setLSB.push_back(bank);

        programChange.push_back(0xc0);
        programChange.push_back(0x00);

        JackSingleton::instance().queueMessage(&setMSB[0], 3);
        JackSingleton::instance().queueMessage(&setLSB[0], 3);
        JackSingleton::instance().queueMessage(&programChange[0], 2);

        sleep(2);
    }//for
    */

/*    
    //SRX 8
    for (unsigned char bank = 0; bank < 4; ++bank) {
        std::vector<jack_midi_data_t> setMSB;
        std::vector<jack_midi_data_t> setLSB;
        std::vector<jack_midi_data_t> programChange;

        setMSB.push_back(0xb0);
        setMSB.push_back(0x00);
        setMSB.push_back(93);

        setLSB.push_back(0xb0);
        setLSB.push_back(0x20);
        setLSB.push_back(15 + bank);

        programChange.push_back(0xc0);
        programChange.push_back(0x00);

        JackSingleton::instance().queueMessage(&setMSB[0], 3);
        JackSingleton::instance().queueMessage(&setLSB[0], 3);
        JackSingleton::instance().queueMessage(&programChange[0], 2);

        sleep(2);
    }//for
*/    

    /*
    //User
    for (unsigned char bank = 0; bank < 2; ++bank) {
        std::vector<jack_midi_data_t> setMSB;
        std::vector<jack_midi_data_t> setLSB;
        std::vector<jack_midi_data_t> programChange;

        setMSB.push_back(0xb0);
        setMSB.push_back(0x00);
        setMSB.push_back(87);

        setLSB.push_back(0xb0);
        setLSB.push_back(0x20);
        setLSB.push_back(bank);

        programChange.push_back(0xc0);
        programChange.push_back(0x00);

        JackSingleton::instance().queueMessage(&setMSB[0], 3);
        JackSingleton::instance().queueMessage(&setLSB[0], 3);
        JackSingleton::instance().queueMessage(&programChange[0], 2);

        sleep(2);
    }//for
    */

/*
    for (unsigned char patch = 0; patch < 127; ++patch) {
        boost::array<jack_midi_data_t, 14> selectPatchBase = {0xF0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x12, 0x01, 0x00, 0x00, 0x06, patch};
        std::vector<jack_midi_data_t> selectPatch;
        std::copy(&selectPatchBase[0], &selectPatchBase[selectPatchBase.size()-1], std::back_inserter(selectPatch));
        JackSingleton::instance().addChecksum(selectPatch);
        JackSingleton::instance().queueMessage(&selectPatch[0], selectPatch.size());
        sleep(2);
    }//for
*/
    Gtk::Main::run();

    return 0;
}//main

JackSingleton::JackSingleton()
{
    jackClient = NULL;
}//constructor

JackSingleton::~JackSingleton()
{
    jack_client_close(jackClient);
}//destrcutor

SonicCellLibrarian::SonicCellLibrarian(BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& refXml) : Gtk::Window(cobject)
{
    sonicCellData.reset(new SonicCellData::SonicCell);
    widgets.reset(new SonicCellLibrarianWidgets(this));

    widgets->setUpWidgets(refXml, this);
}//constructor

SonicCellLibrarian::~SonicCellLibrarian()
{
    //Nothing
}//destructor

JackSingleton &JackSingleton::instance()
{
    if (NULL == JackSingleton::JSInstance) {
        JackSingleton::JSInstance = new JackSingleton;
    }//if

    return *JackSingleton::JSInstance;
}//instance

boost::shared_ptr<SonicCellData::SonicCell> SonicCellLibrarian::getSonicCellData() const
{
    return sonicCellData;
}//getSonicCellData


