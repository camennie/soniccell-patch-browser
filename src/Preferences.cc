/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Preferences.h"
#include <fstream>



Preferences::Preferences(Glib::RefPtr<Gnome::Glade::Xml> refXml)
{
    refXml->get_widget("preferencesDialog", preferencesDialog);

    refXml->get_widget("samplePathEntry", samplePathEntry);
    refXml->get_widget("playMidiEntry", playMidiCommandEntry);

    std::ifstream inputStream("SonicCellPatchBrowserPreferences.xml");
	if (false == inputStream.good()) {
		return;
	}//if

	boost::archive::xml_iarchive inputArchive(inputStream);
    inputArchive & BOOST_SERIALIZATION_NVP(samplePath);
    inputArchive & BOOST_SERIALIZATION_NVP(playMidiCommand);

    samplePathEntry->set_text(samplePath);
    playMidiCommandEntry->set_text(playMidiCommand);

    inputStream.close();
}//constructor

Preferences::~Preferences()
{
    save();
}//destructor

void Preferences::save()
{
    std::ofstream outputStream("SonicCellPatchBrowserPreferences.xml");
	assert(outputStream.good());
	if (false == outputStream.good()) {
		return;
	}//if

	boost::archive::xml_oarchive outputArchive(outputStream);
    outputArchive & BOOST_SERIALIZATION_NVP(samplePath);
    outputArchive & BOOST_SERIALIZATION_NVP(playMidiCommand);

    outputStream.close();
}//show

void Preferences::show()
{
    int result = preferencesDialog->run();
    if (result == 1) {
        samplePath = samplePathEntry->get_text();
        playMidiCommand = playMidiCommandEntry->get_text();
        save();
    }//if

    preferencesDialog->hide();
}//show

std::string Preferences::getSamplePath() const
{
    return samplePath;
}//getSamplePath

std::string Preferences::getPlayMidiCommand() const
{
    return playMidiCommand;
}//getPlayMidiCommand

template<class Archive>
void Preferences::serialize(Archive &ar, const unsigned int version)
{
    ar & BOOST_SERIALIZATION_NVP(samplePath);
    ar & BOOST_SERIALIZATION_NVP(playMidiCommand);
}//serialize


template void Preferences::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive &ar, const unsigned int version);

template void Preferences::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive &ar, const unsigned int version);

