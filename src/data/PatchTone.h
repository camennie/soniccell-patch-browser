/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __PATCH_TONE_H_
#define __PATCH_TONE_H_

#include <boost/shared_ptr.hpp>
#include <boost/array.hpp>
#include <vector>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/access.hpp>
#include <jack/midiport.h>

namespace SonicCellData
{

class PatchTone
{
    unsigned char ToneLevel;
    unsigned char ToneCoarseTune;
    unsigned char ToneFineTune;
    unsigned char ToneRandomPitchDepth;
    unsigned char TonePan;
    unsigned char TonePanKeyfollow;
    unsigned char ToneRandomPanDepth;
    unsigned char ToneAlternatePanDepth;
    unsigned char ToneEnvMode;
    unsigned char ToneDelayMode;
    unsigned short ToneDelayTime;

    unsigned char ToneDrySendLevel;
    unsigned char ToneChorusSendLevelMFX;
    unsigned char ToneReverbSendLevelMFX;
    unsigned char ToneChorusSendLevelNonMFX;
    unsigned char ToneReverbSendLevelNonMFX;
    unsigned char ToneOutputAssign;

    unsigned char ToneReceiveBender;
    unsigned char ToneReceiveExpression;
    unsigned char ToneReceiveHold_1;
    unsigned char ToneReceivePanMode;
    unsigned char ToneRedamperSwitch;

    boost::array<boost::array<unsigned char, 4>, 4> ToneControlSwitch;

    unsigned char WaveGroupType;
    unsigned int WaveGroupID;
    unsigned int WaveNumberLMono;
    unsigned int WaveNumberR;
    unsigned char WaveGain;
    unsigned char WaveFXMSwitch;
    unsigned char WaveFXMColor;
    unsigned char WaveFXMDepth;
    unsigned char WaveTempoSync;
    unsigned char WavePitchKeyfollow;
    unsigned char PitchEnvDepth;
    unsigned char PitchEnvVelocitySens;
    unsigned char PitchEnvTime1VelocitySens;
    unsigned char PitchEnvTime4VelocitySens;
    unsigned char PitchEnvTimeKeyfollow;

    boost::array<unsigned char, 4> PitchEnvTime;
    boost::array<unsigned char, 5> PitchEnvLevel;

    unsigned char TVFFilterType;
    unsigned char TVFCutoffFrequency;
    unsigned char TVFCutoffKeyfollow;
    unsigned char TVFCutoffVelocityCurve;
    unsigned char TVFCutoffVelocitySens;
    unsigned char TVFResonance;
    unsigned char TVFResonanceVelocitySens;
    unsigned char TVFEnvDepth;
    unsigned char TVFEnvVelocityCurve;
    unsigned char TVFEnvVelocitySens;
    unsigned char TVFEnvTime1VelocitySens;
    unsigned char TVFEnvTime4VelocitySens;
    unsigned char TVFEnvTimeKeyfollow;
    boost::array<unsigned char, 4> TVFEnvTime;
    boost::array<unsigned char, 5> TVFEnvLevel;

    unsigned char BiasLevel;
    unsigned char BiasPosition;
    unsigned char BiasDirection;
    unsigned char TVALevelVelocityCurve;
    unsigned char TVALevelVelocitySens;
    unsigned char TVAEnvTime1VelocitySens;
    unsigned char TVAEnvTime4VelocitySens;
    unsigned char TVAEnvTimeKeyfollow;
    boost::array<unsigned char, 4> TVAEnvTime;
    boost::array<unsigned char, 3> TVAEnvLevel;

    boost::array<unsigned char, 2> LFOWaveform;
    boost::array<unsigned short, 2> LFORate;
    boost::array<unsigned char, 2> LFOOffset;
    boost::array<unsigned char, 2> LFORateDetune;
    boost::array<unsigned char, 2> LFODelayTime;
    boost::array<unsigned char, 2> LFODelayTimeKeyfollow;
    boost::array<unsigned char, 2> LFOFadeMode;
    boost::array<unsigned char, 2> LFOFadeTime;
    boost::array<unsigned char, 2> LFOKeyTrigger;
    boost::array<unsigned char, 2> LFOPitchDepth;
    boost::array<unsigned char, 2> LFOTVFDepth;
    boost::array<unsigned char, 2> LFOTVADepth;
    boost::array<unsigned char, 2> LFOPanDepth;

    unsigned char LFOStepType;
    boost::array<unsigned char, 16> LFOStep;

    ////////
    unsigned int toneNum;

    PatchTone() {} //for serialization
public:
    static const unsigned int fullDataSize = 0x9a; //0x011a;
    
    PatchTone(unsigned int toneNum_);
    virtual ~PatchTone();

    //Data will have address stripped from it
    bool handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize);

    std::vector<jack_midi_data_t> createUpdateRequest(unsigned char myIndex) const;
    std::vector<jack_midi_data_t> createWriteRequest(unsigned char myIndex) const;

    //Gets
    unsigned char getToneLevel() const;
    unsigned char getToneCoarseTune() const;
    unsigned char getToneFineTune() const;
    unsigned char getToneRandomPitchDepth() const;
    unsigned char getTonePan() const;
    unsigned char getTonePanKeyfollow() const;
    unsigned char getToneRandomPanDepth() const;
    unsigned char getToneAlternatePanDepth() const;
    unsigned char getToneEnvMode() const;
    unsigned char getToneDelayMode() const;
    unsigned short getToneDelayTime() const;

    unsigned char getToneDrySendLevel() const;
    unsigned char getToneChorusSendLevelMFX() const;
    unsigned char getToneReverbSendLevelMFX() const;
    unsigned char getToneChorusSendLevelNonMFX() const;
    unsigned char getToneReverbSendLevelNonMFX() const;
    unsigned char getToneOutputAssign() const;

    unsigned char getToneReceiveBender() const;
    unsigned char getToneReceiveExpression() const;
    unsigned char getToneReceiveHold_1() const;
    unsigned char getToneReceivePanMode() const;
    unsigned char getToneRedamperSwitch() const;

    unsigned char getToneControlSwitch(unsigned int control, unsigned int index) const;

    unsigned char getWaveGroupType() const;
    unsigned int getWaveGroupID() const;
    unsigned int getWaveNumberLMono() const;
    unsigned int getWaveNumberR() const;
    unsigned char getWaveGain() const;
    unsigned char getWaveFXMSwitch() const;
    unsigned char getWaveFXMColor() const;
    unsigned char getWaveFXMDepth() const;
    unsigned char getWaveTempoSync() const;
    unsigned char getWavePitchKeyfollow() const;
    unsigned char getPitchEnvDepth() const;
    unsigned char getPitchEnvVelocitySens() const;
    unsigned char getPitchEnvTime1VelocitySens() const;
    unsigned char getPitchEnvTime4VelocitySens() const;
    unsigned char getPitchEnvTimeKeyfollow() const;

    unsigned char getPitchEnvTime(unsigned int index) const;
    unsigned char getPitchEnvLevel(unsigned int index) const;

    unsigned char getTVFFilterType() const;
    unsigned char getTVFCutoffFrequency() const;
    unsigned char getTVFCutoffKeyfollow() const;
    unsigned char getTVFCutoffVelocityCurve() const;
    unsigned char getTVFCutoffVelocitySens() const;
    unsigned char getTVFResonance() const;
    unsigned char getTVFResonanceVelocitySens() const;
    unsigned char getTVFEnvDepth() const;
    unsigned char getTVFEnvVelocityCurve() const;
    unsigned char getTVFEnvVelocitySens() const;
    unsigned char getTVFEnvTime1VelocitySens() const;
    unsigned char getTVFEnvTime4VelocitySens() const;
    unsigned char getTVFEnvTimeKeyfollow() const;
    unsigned char getTVFEnvTime(unsigned int index) const;
    unsigned char getTVFEnvLevel(unsigned int index) const;

    unsigned char getBiasLevel() const;
    unsigned char getBiasPosition() const;
    unsigned char getBiasDirection() const;
    unsigned char getTVALevelVelocityCurve() const;
    unsigned char getTVALevelVelocitySens() const;
    unsigned char getTVAEnvTime1VelocitySens() const;
    unsigned char getTVAEnvTime4VelocitySens() const;
    unsigned char getTVAEnvTimeKeyfollow() const;
    unsigned char getTVAEnvTime(unsigned int index) const;
    unsigned char getTVAEnvLevel(unsigned int index) const;

    unsigned char getLFOWaveform(unsigned int index) const;
    unsigned short getLFORate(unsigned int index) const;
    unsigned char getLFOOffset(unsigned int index) const;
    unsigned char getLFORateDetune(unsigned int index) const;
    unsigned char getLFODelayTime(unsigned int index) const;
    unsigned char getLFODelayTimeKeyfollow(unsigned int index) const;
    unsigned char getLFOFadeMode(unsigned int index) const;
    unsigned char getLFOFadeTime(unsigned int index) const;
    unsigned char getLFOKeyTrigger(unsigned int index) const;
    unsigned char getLFOPitchDepth(unsigned int index) const;
    unsigned char getLFOTVFDepth(unsigned int index) const;
    unsigned char getLFOTVADepth(unsigned int index) const;
    unsigned char getLFOPanDepth(unsigned int index) const;

    unsigned char getLFOStepType() const;
    unsigned char getLFOStep(unsigned int index) const;

    //Sets
    void setToneLevel(unsigned char value);
    void setToneCoarseTune(unsigned char value);
    void setToneFineTune(unsigned char value);
    void setToneRandomPitchDepth(unsigned char value);
    void setTonePan(unsigned char value);
    void setTonePanKeyfollow(unsigned char value);
    void setToneRandomPanDepth(unsigned char value);
    void setToneAlternatePanDepth(unsigned char value);
    void setToneEnvMode(unsigned char value);
    void setToneDelayMode(unsigned char value);
    void setToneDelayTime(unsigned short value);

    void setToneDrySendLevel(unsigned char value);
    void setToneChorusSendLevelMFX(unsigned char value);
    void setToneReverbSendLevelMFX(unsigned char value);
    void setToneChorusSendLevelNonMFX(unsigned char value);
    void setToneReverbSendLevelNonMFX(unsigned char value);
    void setToneOutputAssign(unsigned char value);

    void setToneReceiveBender(unsigned char value);
    void setToneReceiveExpression(unsigned char value);
    void setToneReceiveHold_1(unsigned char value);
    void setToneReceivePanMode(unsigned char value);
    void setToneRedamperSwitch(unsigned char value);

    void setToneControlSwitch(unsigned int control, unsigned int index, unsigned char value);

    void setWaveGroupType(unsigned char value);
    void setWaveGroupID(unsigned int value);
    void setWaveNumberLMono(unsigned int value);
    void setWaveNumberR(unsigned int value);
    void setWaveGain(unsigned char value);
    void setWaveFXMSwitch(unsigned char value);
    void setWaveFXMColor(unsigned char value);
    void setWaveFXMDepth(unsigned char value);
    void setWaveTempoSync(unsigned char value);
    void setWavePitchKeyfollow(unsigned char value);
    void setPitchEnvDepth(unsigned char value);
    void setPitchEnvVelocitySens(unsigned char value);
    void setPitchEnvTime1VelocitySens(unsigned char value);
    void setPitchEnvTime4VelocitySens(unsigned char value);
    void setPitchEnvTimeKeyfollow(unsigned char value);

    void setPitchEnvTime(unsigned int index, unsigned char value);
    void setPitchEnvLevel(unsigned int index, unsigned char value);

    void setTVFFilterType(unsigned char value);
    void setTVFCutoffFrequency(unsigned char value);
    void setTVFCutoffKeyfollow(unsigned char value);
    void setTVFCutoffVelocityCurve(unsigned char value);
    void setTVFCutoffVelocitySens(unsigned char value);
    void setTVFResonance(unsigned char value);
    void setTVFResonanceVelocitySens(unsigned char value);
    void setTVFEnvDepth(unsigned char value);
    void setTVFEnvVelocityCurve(unsigned char value);
    void setTVFEnvVelocitySens(unsigned char value);
    void setTVFEnvTime1VelocitySens(unsigned char value);
    void setTVFEnvTime4VelocitySens(unsigned char value);
    void setTVFEnvTimeKeyfollow(unsigned char value);
    void setTVFEnvTime(unsigned int index, unsigned char value);
    void setTVFEnvLevel(unsigned int index, unsigned char value);

    void setBiasLevel(unsigned char value);
    void setBiasPosition(unsigned char value);
    void setBiasDirection(unsigned char value);
    void setTVALevelVelocityCurve(unsigned char value);
    void setTVALevelVelocitySens(unsigned char value);
    void setTVAEnvTime1VelocitySens(unsigned char value);
    void setTVAEnvTime4VelocitySens(unsigned char value);
    void setTVAEnvTimeKeyfollow(unsigned char value);
    void setTVAEnvTime(unsigned int index, unsigned char value);
    void setTVAEnvLevel(unsigned int index, unsigned char value);

    void setLFOWaveform(unsigned int index, unsigned char value);
    void setLFORate(unsigned int index, unsigned short value);
    void setLFOOffset(unsigned int index, unsigned char value);
    void setLFORateDetune(unsigned int index, unsigned char value);
    void setLFODelayTime(unsigned int index, unsigned char value);
    void setLFODelayTimeKeyfollow(unsigned int index, unsigned char value);
    void setLFOFadeMode(unsigned int index, unsigned char value);
    void setLFOFadeTime(unsigned int index, unsigned char value);
    void setLFOKeyTrigger(unsigned int index, unsigned char value);
    void setLFOPitchDepth(unsigned int index, unsigned char value);
    void setLFOTVFDepth(unsigned int index, unsigned char value);
    void setLFOTVADepth(unsigned int index, unsigned char value);
    void setLFOPanDepth(unsigned int index, unsigned char value);

    void setLFOStepType(unsigned char value);
    void setLFOStep(unsigned int index, unsigned char value);

    boost::shared_ptr<PatchTone> deepClone() const;

    template<class Archive> void serialize(Archive &ar, const unsigned int version);
	friend class boost::serialization::access;
};//PatchTone

}//namespace SonicCellData

#endif
