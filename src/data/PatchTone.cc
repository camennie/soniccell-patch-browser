/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "PatchTone.h"
#include "../main.h"
#include <iostream>
#include <string.h>

SonicCellData::PatchTone::PatchTone(unsigned int toneNum_) : toneNum(toneNum_) 
{
    ToneLevel = 0;
    ToneCoarseTune = 0;
    ToneFineTune = 0;
    ToneRandomPitchDepth = 0;
    TonePan = 0;
    TonePanKeyfollow = 0;
    ToneRandomPanDepth = 0;
    ToneAlternatePanDepth = 0;
    ToneEnvMode = 0;
    ToneDelayMode = 0;
    ToneDelayTime = 0;

    ToneDrySendLevel = 0;
    ToneChorusSendLevelMFX = 0;
    ToneReverbSendLevelMFX = 0;
    ToneChorusSendLevelNonMFX = 0;
    ToneReverbSendLevelNonMFX = 0;
    ToneOutputAssign = 0;

    ToneReceiveBender = 0;
    ToneReceiveExpression = 0;
    ToneReceiveHold_1 = 0;
    ToneReceivePanMode = 0;
    ToneRedamperSwitch = 0;

    memset(&ToneControlSwitch[0], 0, sizeof(ToneControlSwitch));

    WaveGroupType = 0;
    WaveGroupID = 0;
    WaveNumberLMono = 0;
    WaveNumberR = 0;
    WaveGain = 0;
    WaveFXMSwitch = 0;
    WaveFXMColor = 0;
    WaveFXMDepth = 0;
    WaveTempoSync = 0;
    WavePitchKeyfollow = 0;
    PitchEnvDepth = 0;
    PitchEnvVelocitySens = 0;
    PitchEnvTime1VelocitySens = 0;
    PitchEnvTime4VelocitySens = 0;
    PitchEnvTimeKeyfollow = 0;

    memset(&PitchEnvTime[0], 0, sizeof(PitchEnvTime));
    memset(&PitchEnvLevel[0], 0, sizeof(PitchEnvLevel));

    TVFFilterType = 0;
    TVFCutoffFrequency = 0;
    TVFCutoffKeyfollow = 0;
    TVFCutoffVelocityCurve = 0;
    TVFCutoffVelocitySens = 0;
    TVFResonance = 0;
    TVFResonanceVelocitySens = 0;
    TVFEnvDepth = 0;
    TVFEnvVelocityCurve = 0;
    TVFEnvVelocitySens = 0;
    TVFEnvTime1VelocitySens = 0;
    TVFEnvTime4VelocitySens = 0;
    TVFEnvTimeKeyfollow = 0;
    memset(&TVFEnvTime[0], 0, sizeof(TVFEnvTime));
    memset(&TVFEnvLevel[0], 0, sizeof(TVFEnvLevel));

    BiasLevel = 0;
    BiasPosition = 0;
    BiasDirection = 0;
    TVALevelVelocityCurve = 0;
    TVALevelVelocitySens = 0;
    TVAEnvTime1VelocitySens = 0;
    TVAEnvTime4VelocitySens = 0;
    TVAEnvTimeKeyfollow = 0;
    memset(&TVAEnvTime[0], 0, sizeof(TVAEnvTime));
    memset(&TVAEnvLevel[0], 0, sizeof(TVAEnvLevel));

    memset(&LFOWaveform[0], 0, sizeof(LFOWaveform));
    memset(&LFORate[0], 0, sizeof(LFORate));
    memset(&LFOOffset[0], 0, sizeof(LFOOffset));
    memset(&LFORateDetune[0], 0, sizeof(LFORateDetune));
    memset(&LFODelayTime[0], 0, sizeof(LFODelayTime));
    memset(&LFODelayTimeKeyfollow[0], 0, sizeof(LFODelayTimeKeyfollow));
    memset(&LFOFadeMode[0], 0, sizeof(LFOFadeMode));
    memset(&LFOFadeTime[0], 0, sizeof(LFOFadeTime));
    memset(&LFOKeyTrigger[0], 0, sizeof(LFOKeyTrigger));
    memset(&LFOPitchDepth[0], 0, sizeof(LFOPitchDepth));
    memset(&LFOTVFDepth[0], 0, sizeof(LFOTVFDepth));
    memset(&LFOTVADepth[0], 0, sizeof(LFOTVADepth));
    memset(&LFOPanDepth[0], 0, sizeof(LFOPanDepth));

    LFOStepType = 0;
    memset(&LFOStep[0], 0, sizeof(LFOStep));
}//constructor

SonicCellData::PatchTone::~PatchTone()
{
    //Nothing
}//destructor

bool SonicCellData::PatchTone::handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize)
{
    if (dataSize != SonicCellData::PatchTone::fullDataSize) {
        std::cout << "Initial tone dataSize: " << dataSize << std::endl;
    }//if

    switch (offset) {
        case 0x00:
            if (0 == dataSize) break;
            ToneLevel = *data;
            data++; dataSize--;
        case 0x01:
            if (0 == dataSize) break;
            ToneCoarseTune = *data;
            data++; dataSize--;
        case 0x02:
            if (0 == dataSize) break;
            ToneFineTune = *data;
            data++; dataSize--;
        case 0x03:
            if (0 == dataSize) break;
            ToneRandomPitchDepth = *data;
            data++; dataSize--;
        case 0x04:
            if (0 == dataSize) break;
            TonePan = *data;
            data++; dataSize--;
        case 0x05:
            if (0 == dataSize) break;
            TonePanKeyfollow = *data;
            data++; dataSize--;
        case 0x06:
            if (0 == dataSize) break;
            ToneRandomPanDepth = *data;
            data++; dataSize--;
        case 0x07:
            if (0 == dataSize) break;
            ToneAlternatePanDepth = *data;
            data++; dataSize--;
        case 0x08:
            if (0 == dataSize) break;
            ToneEnvMode = *data;
            data++; dataSize--;
        case 0x09:
            if (0 == dataSize) break;
            ToneDelayMode = *data;
            data++; dataSize--;
        case 0x0a:
            if (0 == dataSize) break;
            ToneDelayTime = (data[0] << 8) + data[1];
            data += 2; dataSize -= 2;
        case 0x0c:
            if (0 == dataSize) break;
            ToneDrySendLevel = *data;
            data++; dataSize--;
        case 0x0d:
            if (0 == dataSize) break;
            ToneChorusSendLevelMFX = *data;
            data++; dataSize--;
        case 0x0e:
            if (0 == dataSize) break;
            ToneReverbSendLevelMFX = *data;
            data++; dataSize--;
        case 0x0f:
            if (0 == dataSize) break;
            ToneChorusSendLevelNonMFX = *data;
            data++; dataSize--;
        case 0x10:
            if (0 == dataSize) break;
            ToneReverbSendLevelNonMFX = *data;
            data++; dataSize--;
        case 0x11:
            if (0 == dataSize) break;
            ToneOutputAssign = *data;
            data++; dataSize--;
        case 0x12:
            if (0 == dataSize) break;
            ToneReceiveBender = *data;
            data++; dataSize--;
        case 0x13:
            if (0 == dataSize) break;
            ToneReceiveExpression = *data;
            data++; dataSize--;
        case 0x14:
            if (0 == dataSize) break;
            ToneReceiveHold_1 = *data;
            data++; dataSize--;
        case 0x15:
            if (0 == dataSize) break;
            ToneReceivePanMode = *data;
            data++; dataSize--;
        case 0x16:
            if (0 == dataSize) break;
            ToneRedamperSwitch = *data;
            data++; dataSize--;
        case 0x17:
            if (0 == dataSize) break;
            ToneControlSwitch[0][0] = *data;
            data++; dataSize--;
        case 0x18:
            if (0 == dataSize) break;
            ToneControlSwitch[0][1] = *data;
            data++; dataSize--;
        case 0x19:
            if (0 == dataSize) break;
            ToneControlSwitch[0][2] = *data;
            data++; dataSize--;
        case 0x1a:
            if (0 == dataSize) break;
            ToneControlSwitch[0][3] = *data;
            data++; dataSize--;
        case 0x1b:
            if (0 == dataSize) break;
            ToneControlSwitch[1][0] = *data;
            data++; dataSize--;
        case 0x1c:
            if (0 == dataSize) break;
            ToneControlSwitch[1][1] = *data;
            data++; dataSize--;
        case 0x1d:
            if (0 == dataSize) break;
            ToneControlSwitch[1][2] = *data;
            data++; dataSize--;
        case 0x1e:
            if (0 == dataSize) break;
            ToneControlSwitch[1][3] = *data;
            data++; dataSize--;
        case 0x1f:
            if (0 == dataSize) break;
            ToneControlSwitch[2][0] = *data;
            data++; dataSize--;
        case 0x20:
            if (0 == dataSize) break;
            ToneControlSwitch[2][1] = *data;
            data++; dataSize--;
        case 0x21:
            if (0 == dataSize) break;
            ToneControlSwitch[2][2] = *data;
            data++; dataSize--;
        case 0x22:
            if (0 == dataSize) break;
            ToneControlSwitch[2][3] = *data;
            data++; dataSize--;
        case 0x23:
            if (0 == dataSize) break;
            ToneControlSwitch[3][0] = *data;
            data++; dataSize--;
        case 0x24:
            if (0 == dataSize) break;
            ToneControlSwitch[3][1] = *data;
            data++; dataSize--;
        case 0x25:
            if (0 == dataSize) break;
            ToneControlSwitch[3][2] = *data;
            data++; dataSize--;
        case 0x26:
            if (0 == dataSize) break;
            ToneControlSwitch[3][3] = *data;
            data++; dataSize--;
        case 0x27:
            if (0 == dataSize) break;
            WaveGroupType = *data;
            data++; dataSize--;
        case 0x28:
            if (0 == dataSize) break;
            WaveGroupID = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x2c:
            if (0 == dataSize) break;
            WaveNumberLMono = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x30:
            if (0 == dataSize) break;
            WaveNumberR = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x34:
            if (0 == dataSize) break;
            WaveGain = *data;
            data++; dataSize--;
        case 0x35:
            if (0 == dataSize) break;
            WaveFXMSwitch = *data;
            data++; dataSize--;
        case 0x36:
            if (0 == dataSize) break;
            WaveFXMColor = *data;
            data++; dataSize--;
        case 0x37:
            if (0 == dataSize) break;
            WaveFXMDepth = *data;
            data++; dataSize--;
        case 0x38:
            if (0 == dataSize) break;
            WaveTempoSync = *data;
            data++; dataSize--;
        case 0x39:
            if (0 == dataSize) break;
            WavePitchKeyfollow = *data;
            data++; dataSize--;
        case 0x3a:
            if (0 == dataSize) break;
            PitchEnvDepth = *data;
            data++; dataSize--;
        case 0x3b:
            if (0 == dataSize) break;
            PitchEnvVelocitySens = *data;
            data++; dataSize--;
        case 0x3c:
            if (0 == dataSize) break;
            PitchEnvTime1VelocitySens = *data;
            data++; dataSize--;
        case 0x3d:
            if (0 == dataSize) break;
            PitchEnvTime4VelocitySens = *data;
            data++; dataSize--;
        case 0x3e:
            if (0 == dataSize) break;
            PitchEnvTimeKeyfollow = *data;
            data++; dataSize--;
        case 0x3f:
            if (0 == dataSize) break;
            PitchEnvTime[0] = *data;
            data++; dataSize--;
        case 0x40:
            if (0 == dataSize) break;
            PitchEnvTime[1] = *data;
            data++; dataSize--;
        case 0x41:
            if (0 == dataSize) break;
            PitchEnvTime[2] = *data;
            data++; dataSize--;
        case 0x42:
            if (0 == dataSize) break;
            PitchEnvTime[3] = *data;
            data++; dataSize--;
        case 0x43:
            if (0 == dataSize) break;
            PitchEnvLevel[0] = *data;
            data++; dataSize--;
        case 0x44:
            if (0 == dataSize) break;
            PitchEnvLevel[1] = *data;
            data++; dataSize--;
        case 0x45:
            if (0 == dataSize) break;
            PitchEnvLevel[2] = *data;
            data++; dataSize--;
        case 0x46:
            if (0 == dataSize) break;
            PitchEnvLevel[3] = *data;
            data++; dataSize--;
        case 0x47:
            if (0 == dataSize) break;
            PitchEnvLevel[4] = *data;
            data++; dataSize--;
        case 0x48:
            if (0 == dataSize) break;
            TVFFilterType = *data;
            data++; dataSize--;
        case 0x49:
            if (0 == dataSize) break;
            TVFCutoffFrequency = *data;
            data++; dataSize--;
        case 0x4a:
            if (0 == dataSize) break;
            TVFCutoffKeyfollow = *data;
            data++; dataSize--;
        case 0x4b:
            if (0 == dataSize) break;
            TVFCutoffVelocityCurve = *data;
            data++; dataSize--;
        case 0x4c:
            if (0 == dataSize) break;
            TVFCutoffVelocitySens = *data;
            data++; dataSize--;
        case 0x4d:
            if (0 == dataSize) break;
            TVFResonance = *data;
            data++; dataSize--;
        case 0x4e:
            if (0 == dataSize) break;
            TVFResonanceVelocitySens = *data;
            data++; dataSize--;
        case 0x4f:
            if (0 == dataSize) break;
            TVFEnvDepth = *data;
            data++; dataSize--;
        case 0x50:
            if (0 == dataSize) break;
            TVFEnvVelocityCurve = *data;
            data++; dataSize--;
        case 0x51:
            if (0 == dataSize) break;
            TVFEnvVelocitySens = *data;
            data++; dataSize--;
        case 0x52:
            if (0 == dataSize) break;
            TVFEnvTime1VelocitySens = *data;
            data++; dataSize--;
        case 0x53:
            if (0 == dataSize) break;
            TVFEnvTime4VelocitySens = *data;
            data++; dataSize--;
        case 0x54:
            if (0 == dataSize) break;
            TVFEnvTimeKeyfollow = *data;
            data++; dataSize--;
        case 0x55:
            if (0 == dataSize) break;
            TVFEnvTime[0] = *data;
            data++; dataSize--;
        case 0x56:
            if (0 == dataSize) break;
            TVFEnvTime[1] = *data;
            data++; dataSize--;
        case 0x57:
            if (0 == dataSize) break;
            TVFEnvTime[2] = *data;
            data++; dataSize--;
        case 0x58:
            if (0 == dataSize) break;
            TVFEnvTime[3] = *data;
            data++; dataSize--;
        case 0x59:
            if (0 == dataSize) break;
            TVFEnvLevel[0] = *data;
            data++; dataSize--;
        case 0x5a:
            if (0 == dataSize) break;
            TVFEnvLevel[1] = *data;
            data++; dataSize--;
        case 0x5b:
            if (0 == dataSize) break;
            TVFEnvLevel[2] = *data;
            data++; dataSize--;
        case 0x5c:
            if (0 == dataSize) break;
            TVFEnvLevel[3] = *data;
            data++; dataSize--;
        case 0x5d:
            if (0 == dataSize) break;
            TVFEnvLevel[4] = *data;
            data++; dataSize--;
        case 0x5e:
            if (0 == dataSize) break;
            BiasLevel = *data;
            data++; dataSize--;
        case 0x5f:
            if (0 == dataSize) break;
            BiasPosition = *data;
            data++; dataSize--;
        case 0x60:
            if (0 == dataSize) break;
            BiasDirection = *data;
            data++; dataSize--;
        case 0x61:
            if (0 == dataSize) break;
            TVALevelVelocityCurve = *data;
            data++; dataSize--;
        case 0x62:
            if (0 == dataSize) break;
            TVALevelVelocitySens = *data;
            data++; dataSize--;
        case 0x63:
            if (0 == dataSize) break;
            TVAEnvTime1VelocitySens = *data;
            data++; dataSize--;
        case 0x64:
            if (0 == dataSize) break;
            TVAEnvTime4VelocitySens = *data;
            data++; dataSize--;
        case 0x65:
            if (0 == dataSize) break;
            TVAEnvTimeKeyfollow = *data;
            data++; dataSize--;
        case 0x66:
            if (0 == dataSize) break;
            TVAEnvTime[0] = *data;
            data++; dataSize--;
        case 0x67:
            if (0 == dataSize) break;
            TVAEnvTime[1] = *data;
            data++; dataSize--;
        case 0x68:
            if (0 == dataSize) break;
            TVAEnvTime[2] = *data;
            data++; dataSize--;
        case 0x69:
            if (0 == dataSize) break;
            TVAEnvTime[3] = *data;
            data++; dataSize--;
        case 0x6a:
            if (0 == dataSize) break;
            TVAEnvLevel[0] = *data;
            data++; dataSize--;
        case 0x6b:
            if (0 == dataSize) break;
            TVAEnvLevel[1] = *data;
            data++; dataSize--;
        case 0x6c:
            if (0 == dataSize) break;
            TVAEnvLevel[2] = *data;
            data++; dataSize--;
        case 0x6d:
            if (0 == dataSize) break;
            LFOWaveform[0] = *data;
            data++; dataSize--;
        case 0x6e:
            if (0 == dataSize) break;
            LFORate[0] = (data[0] << 8) + data[1];
            data += 2; dataSize -= 2;
        case 0x70:
            if (0 == dataSize) break;
            LFOOffset[0] = *data;
            data++; dataSize--;
        case 0x71:
            if (0 == dataSize) break;
            LFORateDetune[0] = *data;
            data++; dataSize--;
        case 0x72:
            if (0 == dataSize) break;
            LFODelayTime[0] = *data;
            data++; dataSize--;
        case 0x73:
            if (0 == dataSize) break;
            LFODelayTimeKeyfollow[0] = *data;
            data++; dataSize--;
        case 0x74:
            if (0 == dataSize) break;
            LFOFadeMode[0] = *data;
            data++; dataSize--;
        case 0x75:
            if (0 == dataSize) break;
            LFOFadeTime[0] = *data;
            data++; dataSize--;
        case 0x76:
            if (0 == dataSize) break;
            LFOKeyTrigger[0] = *data;
            data++; dataSize--;
        case 0x77:
            if (0 == dataSize) break;
            LFOPitchDepth[0] = *data;
            data++; dataSize--;
        case 0x78:
            if (0 == dataSize) break;
            LFOTVFDepth[0] = *data;
            data++; dataSize--;
        case 0x79:
            if (0 == dataSize) break;
            LFOTVADepth[0] = *data;
            data++; dataSize--;
        case 0x7a:
            if (0 == dataSize) break;
            LFOPanDepth[0] = *data;
            data++; dataSize--;
        case 0x7b:
            if (0 == dataSize) break;
            LFOWaveform[1] = *data;
            data++; dataSize--;
        case 0x7c:
            if (0 == dataSize) break;
            LFORate[1] = (data[0] << 8) + data[1];
            data += 2; dataSize -= 2;
        case 0x7e:
            if (0 == dataSize) break;
            LFOOffset[1] = *data;
            data++; dataSize--;
        case 0x7f:
            if (0 == dataSize) break;
            LFORateDetune[1] = *data;
            data++; dataSize--;
        case 0x80:
            if (0 == dataSize) break;
            LFODelayTime[1] = *data;
            data++; dataSize--;
        case 0x81:
            if (0 == dataSize) break;
            LFODelayTimeKeyfollow[1] = *data;
            data++; dataSize--;
        case 0x82:
            if (0 == dataSize) break;
            LFOFadeMode[1] = *data;
            data++; dataSize--;
        case 0x83:
            if (0 == dataSize) break;
            LFOFadeTime[1] = *data;
            data++; dataSize--;
        case 0x84:
            if (0 == dataSize) break;
            LFOKeyTrigger[1] = *data;
            data++; dataSize--;
        case 0x85:
            if (0 == dataSize) break;
            LFOPitchDepth[1] = *data;
            data++; dataSize--;
        case 0x86:
            if (0 == dataSize) break;
            LFOTVFDepth[1] = *data;
            data++; dataSize--;
        case 0x87:
            if (0 == dataSize) break;
            LFOTVADepth[1] = *data;
            data++; dataSize--;
        case 0x88:
            if (0 == dataSize) break;
            LFOPanDepth[1] = *data;
            data++; dataSize--;
        case 0x89:
            if (0 == dataSize) break;
            LFOStepType = *data;
            data++; dataSize--;
        case 0x8a:
            if (0 == dataSize) break;
            LFOStep[0] = *data;
            data++; dataSize--;
        case 0x8b:
            if (0 == dataSize) break;
            LFOStep[1] = *data;
            data++; dataSize--;
        case 0x8c:
            if (0 == dataSize) break;
            LFOStep[2] = *data;
            data++; dataSize--;
        case 0x8d:
            if (0 == dataSize) break;
            LFOStep[3] = *data;
            data++; dataSize--;
        case 0x8e:
            if (0 == dataSize) break;
            LFOStep[4] = *data;
            data++; dataSize--;
        case 0x8f:
            if (0 == dataSize) break;
            LFOStep[5] = *data;
            data++; dataSize--;
        case 0x90:
            if (0 == dataSize) break;
            LFOStep[6] = *data;
            data++; dataSize--;
        case 0x91:
            if (0 == dataSize) break;
            LFOStep[7] = *data;
            data++; dataSize--;
        case 0x92:
            if (0 == dataSize) break;
            LFOStep[8] = *data;
            data++; dataSize--;
        case 0x93:
            if (0 == dataSize) break;
            LFOStep[9] = *data;
            data++; dataSize--;
        case 0x94:
            if (0 == dataSize) break;
            LFOStep[10] = *data;
            data++; dataSize--;
        case 0x95:
            if (0 == dataSize) break;
            LFOStep[11] = *data;
            data++; dataSize--;
        case 0x96:
            if (0 == dataSize) break;
            LFOStep[12] = *data;
            data++; dataSize--;
        case 0x97:
            if (0 == dataSize) break;
            LFOStep[13] = *data;
            data++; dataSize--;
        case 0x98:
            if (0 == dataSize) break;
            LFOStep[14] = *data;
            data++; dataSize--;
        case 0x99:
            if (0 == dataSize) break;
            LFOStep[15] = *data;
            data++; dataSize--;
            break;
        default:
            std::cerr << "Invalid patch tone offset: " << offset << std::endl;
            return false;
    }//switch

    if (0 != dataSize) {
        std::cout << "tone dataSize: " << dataSize << std::endl;
        return false;
    }//if

    assert(0 == dataSize);
    return true;
}//handleDataUpdate

std::vector<jack_midi_data_t> SonicCellData::PatchTone::createUpdateRequest(unsigned char myIndex) const
{
    std::vector<jack_midi_data_t> requestBuffer;

    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x11};
    std::copy(dataHeader, dataHeader+7, std::back_inserter(requestBuffer));
    
    unsigned char addressHighHigh = 0x30;
    unsigned char addressHighLow = myIndex;
    if (myIndex > 127) {
        addressHighHigh++;
        addressHighLow -= 128;
    }//if

    jack_midi_data_t addressHigh[2] = {addressHighHigh, addressHighLow};
    std::copy(addressHigh, addressHigh+2, std::back_inserter(requestBuffer));

    jack_midi_data_t commonOffset[2] = {0x20 + toneNum * 2, 0x00};
    std::copy(commonOffset, commonOffset+2, std::back_inserter(requestBuffer));

    unsigned int size = 0x0000011a; //SonicCellData::PatchTone::fullDataSize;
    requestBuffer.push_back((size & 0xff000000) >> 24);
    requestBuffer.push_back((size & 0x00ff0000) >> 16);
    requestBuffer.push_back((size & 0x0000ff00) >> 8);
    requestBuffer.push_back(size & 0x000000ff);

    requestBuffer.push_back(0xf7);

    JackSingleton::addChecksum(requestBuffer);

    return requestBuffer;
}//createUpdateRequest

std::vector<jack_midi_data_t> SonicCellData::PatchTone::createWriteRequest(unsigned char myIndex) const
{
    std::vector<jack_midi_data_t> requestBuffer;

    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x12};
    std::copy(dataHeader, dataHeader+7, std::back_inserter(requestBuffer));
    
    unsigned char addressHighHigh = 0x30;
    unsigned char addressHighLow = myIndex;
    if (myIndex > 127) {
        addressHighHigh++;
        addressHighLow -= 128;
    }//if

    jack_midi_data_t addressHigh[2] = {addressHighHigh, addressHighLow};
    std::copy(addressHigh, addressHigh+2, std::back_inserter(requestBuffer));

    jack_midi_data_t commonOffset[2] = {0x20 + toneNum * 2, 0x00};
    std::copy(commonOffset, commonOffset+2, std::back_inserter(requestBuffer));

    //Add data
    requestBuffer.push_back(ToneLevel);    
    requestBuffer.push_back(ToneCoarseTune);
    requestBuffer.push_back(ToneFineTune);
    requestBuffer.push_back(ToneRandomPitchDepth);
    requestBuffer.push_back(TonePan);
    requestBuffer.push_back(TonePanKeyfollow);
    requestBuffer.push_back(ToneRandomPanDepth);
    requestBuffer.push_back(ToneAlternatePanDepth);
    requestBuffer.push_back(ToneEnvMode);
    requestBuffer.push_back(ToneDelayMode);
    requestBuffer.push_back((ToneDelayTime & 0xff00) >> 8);
    requestBuffer.push_back(ToneDelayTime & 0x00ff);
    requestBuffer.push_back(ToneDrySendLevel);
    requestBuffer.push_back(ToneChorusSendLevelMFX);
    requestBuffer.push_back(ToneReverbSendLevelMFX);
    requestBuffer.push_back(ToneChorusSendLevelNonMFX);
    requestBuffer.push_back(ToneReverbSendLevelNonMFX);
    requestBuffer.push_back(ToneOutputAssign);

    requestBuffer.push_back(ToneReceiveBender);
    requestBuffer.push_back(ToneReceiveExpression);
    requestBuffer.push_back(ToneReceiveHold_1);
    requestBuffer.push_back(ToneReceivePanMode);
    requestBuffer.push_back(ToneRedamperSwitch);

    for (unsigned int index1 = 0; index1 < ToneControlSwitch.size(); ++index1) {
        for (unsigned int index2 = 0; index2 < ToneControlSwitch[0].size(); ++index2) {
            requestBuffer.push_back(ToneControlSwitch[index1][index2]);
        }//for
    }//for

    requestBuffer.push_back(WaveGroupType);

    requestBuffer.push_back((WaveGroupID & 0xff000000) >> 24);
    requestBuffer.push_back((WaveGroupID & 0x00ff0000) >> 16);
    requestBuffer.push_back((WaveGroupID & 0x0000ff00) >> 8);
    requestBuffer.push_back(WaveGroupID & 0x000000ff);

    requestBuffer.push_back((WaveNumberLMono & 0xff000000) >> 24);
    requestBuffer.push_back((WaveNumberLMono & 0x00ff0000) >> 16);
    requestBuffer.push_back((WaveNumberLMono & 0x0000ff00) >> 8);
    requestBuffer.push_back(WaveNumberLMono & 0x000000ff);

    requestBuffer.push_back((WaveNumberR & 0xff000000) >> 24);
    requestBuffer.push_back((WaveNumberR & 0x00ff0000) >> 16);
    requestBuffer.push_back((WaveNumberR & 0x0000ff00) >> 8);
    requestBuffer.push_back(WaveNumberR & 0x000000ff);

    requestBuffer.push_back(WaveGain);
    requestBuffer.push_back(WaveFXMSwitch);
    requestBuffer.push_back(WaveFXMColor);
    requestBuffer.push_back(WaveFXMDepth);
    requestBuffer.push_back(WaveTempoSync);
    requestBuffer.push_back(WavePitchKeyfollow);
    requestBuffer.push_back(PitchEnvDepth);
    requestBuffer.push_back(PitchEnvVelocitySens);
    requestBuffer.push_back(PitchEnvTime1VelocitySens);
    requestBuffer.push_back(PitchEnvTime4VelocitySens);
    requestBuffer.push_back(PitchEnvTimeKeyfollow);

    for (unsigned int index = 0; index < PitchEnvTime.size(); ++index) {
        requestBuffer.push_back(PitchEnvTime[index]);
    }//for

    for (unsigned int index = 0; index < PitchEnvLevel.size(); ++index) {
        requestBuffer.push_back(PitchEnvLevel[index]);
    }//for

    requestBuffer.push_back(TVFFilterType);
    requestBuffer.push_back(TVFCutoffFrequency);
    requestBuffer.push_back(TVFCutoffKeyfollow);
    requestBuffer.push_back(TVFCutoffVelocityCurve);
    requestBuffer.push_back(TVFCutoffVelocitySens);
    requestBuffer.push_back(TVFResonance);
    requestBuffer.push_back(TVFResonanceVelocitySens);
    requestBuffer.push_back(TVFEnvDepth);
    requestBuffer.push_back(TVFEnvVelocityCurve);
    requestBuffer.push_back(TVFEnvVelocitySens);
    requestBuffer.push_back(TVFEnvTime1VelocitySens);
    requestBuffer.push_back(TVFEnvTime4VelocitySens);
    requestBuffer.push_back(TVFEnvTimeKeyfollow);

    for (unsigned int index = 0; index < TVFEnvTime.size(); ++index) {
        requestBuffer.push_back(TVFEnvTime[index]);
    }//for

    for (unsigned int index = 0; index < TVFEnvLevel.size(); ++index) {
        requestBuffer.push_back(TVFEnvLevel[index]);
    }//for

    requestBuffer.push_back(BiasLevel);
    requestBuffer.push_back(BiasPosition);
    requestBuffer.push_back(BiasDirection);
    requestBuffer.push_back(TVALevelVelocityCurve);
    requestBuffer.push_back(TVALevelVelocitySens);
    requestBuffer.push_back(TVAEnvTime1VelocitySens);
    requestBuffer.push_back(TVAEnvTime4VelocitySens);
    requestBuffer.push_back(TVAEnvTimeKeyfollow);

    for (unsigned int index = 0; index < TVAEnvTime.size(); ++index) {
        requestBuffer.push_back(TVAEnvTime[index]);
    }//for

    for (unsigned int index = 0; index < TVAEnvLevel.size(); ++index) {
        requestBuffer.push_back(TVAEnvLevel[index]);
    }//for

    for (unsigned int index = 0; index < 2; ++index) {
        requestBuffer.push_back(LFOWaveform[index]);
        requestBuffer.push_back((LFORate[index] & 0xff00) >> 8);
        requestBuffer.push_back(LFORate[index] & 0x00ff);
        requestBuffer.push_back(LFOOffset[index]);
        requestBuffer.push_back(LFORateDetune[index]);
        requestBuffer.push_back(LFODelayTime[index]);
        requestBuffer.push_back(LFODelayTimeKeyfollow[index]);
        requestBuffer.push_back(LFOFadeMode[index]);
        requestBuffer.push_back(LFOFadeTime[index]);
        requestBuffer.push_back(LFOKeyTrigger[index]);
        requestBuffer.push_back(LFOPitchDepth[index]);
        requestBuffer.push_back(LFOTVFDepth[index]);
        requestBuffer.push_back(LFOTVADepth[index]);
        requestBuffer.push_back(LFOPanDepth[index]);
    }//for

    requestBuffer.push_back(LFOStepType);

    for (unsigned int index = 0; index < LFOStep.size(); ++index) {
        requestBuffer.push_back(LFOStep[index]);
    }//for

    //Footer
    requestBuffer.push_back(0xf7);

    JackSingleton::addChecksum(requestBuffer);

    return requestBuffer;
}//createWriteRequest

unsigned char SonicCellData::PatchTone::getToneLevel() const
{
    return ToneLevel;
}//getToneLevel

unsigned char SonicCellData::PatchTone::getToneCoarseTune() const
{
    return ToneCoarseTune;
}//getToneCoarseTune

unsigned char SonicCellData::PatchTone::getToneFineTune() const
{
    return ToneFineTune;
}//getToneFineTune

unsigned char SonicCellData::PatchTone::getToneRandomPitchDepth() const
{
    return ToneRandomPitchDepth;
}//getToneRandomPitchDepth

unsigned char SonicCellData::PatchTone::getTonePan() const
{
    return TonePan;
}//getTonePan

unsigned char SonicCellData::PatchTone::getTonePanKeyfollow() const
{
    return TonePanKeyfollow;
}//getTonePanKeyfollow

unsigned char SonicCellData::PatchTone::getToneRandomPanDepth() const
{
    return ToneRandomPanDepth;
}//getToneRandomPanDepth

unsigned char SonicCellData::PatchTone::getToneAlternatePanDepth() const
{
    return ToneAlternatePanDepth;
}//getToneAlternatePanDepth

unsigned char SonicCellData::PatchTone::getToneEnvMode() const
{
    return ToneEnvMode;
}//getToneEnvMode

unsigned char SonicCellData::PatchTone::getToneDelayMode() const
{
    return ToneDelayMode;
}//getToneDelayMode

unsigned short SonicCellData::PatchTone::getToneDelayTime() const
{
    return ToneDelayTime;
}//getToneDelayTime

unsigned char SonicCellData::PatchTone::getToneDrySendLevel() const
{
    return ToneDrySendLevel;
}//getToneDrySendLevel

unsigned char SonicCellData::PatchTone::getToneChorusSendLevelMFX() const
{
    return ToneChorusSendLevelMFX;
}//getToneChorusSendLevelMFX

unsigned char SonicCellData::PatchTone::getToneReverbSendLevelMFX() const
{
    return ToneReverbSendLevelMFX;
}//getToneReverbSendLevelMFX

unsigned char SonicCellData::PatchTone::getToneChorusSendLevelNonMFX() const
{
    return ToneChorusSendLevelNonMFX;
}//getToneChorusSendLevelNonMFX

unsigned char SonicCellData::PatchTone::getToneReverbSendLevelNonMFX() const
{
    return ToneReverbSendLevelNonMFX;
}//getToneReverbSendLevelNonMFX

unsigned char SonicCellData::PatchTone::getToneOutputAssign() const
{
    return ToneOutputAssign;
}//getToneOutputAssign

unsigned char SonicCellData::PatchTone::getToneReceiveBender() const
{
    return ToneReceiveBender;
}//getToneReceiveBender

unsigned char SonicCellData::PatchTone::getToneReceiveExpression() const
{
    return ToneReceiveExpression;
}//getToneReceiveExpression

unsigned char SonicCellData::PatchTone::getToneReceiveHold_1() const
{
    return ToneReceiveHold_1;
}//getToneReceiveHold_1

unsigned char SonicCellData::PatchTone::getToneReceivePanMode() const
{
    return ToneReceivePanMode;
}//getToneReceivePanMode

unsigned char SonicCellData::PatchTone::getToneRedamperSwitch() const
{
    return ToneRedamperSwitch;
}//getToneRedamperSwitch

unsigned char SonicCellData::PatchTone::getToneControlSwitch(unsigned int control, unsigned int index) const
{
    if ((control < ToneControlSwitch.size()) && (index < ToneControlSwitch[0].size())) {
        return ToneControlSwitch[control][index];
    } else {
        return 0;
    }//if
}//getToneControlSwitch

unsigned char SonicCellData::PatchTone::getWaveGroupType() const
{
    return WaveGroupType;
}//getWaveGroupType

unsigned int SonicCellData::PatchTone::getWaveGroupID() const
{
    return WaveGroupID;
}//getWaveGroupID

unsigned int SonicCellData::PatchTone::getWaveNumberLMono() const
{
    return WaveNumberLMono;
}//getWaveNumberLMono

unsigned int SonicCellData::PatchTone::getWaveNumberR() const
{
    return WaveNumberR;
}//getWaveNumberR

unsigned char SonicCellData::PatchTone::getWaveGain() const
{
    return WaveGain;
}//getWaveGain

unsigned char SonicCellData::PatchTone::getWaveFXMSwitch() const
{
    return WaveFXMSwitch;
}//getWaveFXMSwitch

unsigned char SonicCellData::PatchTone::getWaveFXMColor() const
{
    return WaveFXMColor;
}//getWaveFXMColor

unsigned char SonicCellData::PatchTone::getWaveFXMDepth() const
{
    return WaveFXMDepth;
}//getWaveFXMDepth

unsigned char SonicCellData::PatchTone::getWaveTempoSync() const
{
    return WaveTempoSync;
}//getWaveTempoSync

unsigned char SonicCellData::PatchTone::getWavePitchKeyfollow() const
{
    return WavePitchKeyfollow;
}//getWavePitchKeyfollow

unsigned char SonicCellData::PatchTone::getPitchEnvDepth() const
{
    return PitchEnvDepth;
}//getPitchEnvDepth

unsigned char SonicCellData::PatchTone::getPitchEnvVelocitySens() const
{
    return PitchEnvVelocitySens;
}//getPitchEnvVelocitySens

unsigned char SonicCellData::PatchTone::getPitchEnvTime1VelocitySens() const
{
    return PitchEnvTime1VelocitySens;
}//getPitchEnvTime1VelocitySens

unsigned char SonicCellData::PatchTone::getPitchEnvTime4VelocitySens() const
{
    return PitchEnvTime4VelocitySens;
}//getPitchEnvTime4VelocitySens

unsigned char SonicCellData::PatchTone::getPitchEnvTimeKeyfollow() const
{
    return PitchEnvTimeKeyfollow;
}//getPitchEnvTimeKeyfollow


unsigned char SonicCellData::PatchTone::getPitchEnvTime(unsigned int index) const
{
    if (index < PitchEnvTime.size()) {
        return PitchEnvTime[index];
    } else {
        return 0;
    }//if
}//getPitchEnvTime

unsigned char SonicCellData::PatchTone::getPitchEnvLevel(unsigned int index) const
{
    if (index < PitchEnvLevel.size()) {
       return PitchEnvLevel[index];
    } else {
        return 0;
    }//if
}//getPitchEnvLevel

unsigned char SonicCellData::PatchTone::getTVFFilterType() const
{
    return TVFFilterType;
}//getTVFFilterType

unsigned char SonicCellData::PatchTone::getTVFCutoffFrequency() const
{
    return TVFCutoffFrequency;
}//getTVFCutoffFrequency

unsigned char SonicCellData::PatchTone::getTVFCutoffKeyfollow() const
{
    return TVFCutoffKeyfollow;
}//getTVFCutoffKeyfollow

unsigned char SonicCellData::PatchTone::getTVFCutoffVelocityCurve() const
{
    return TVFCutoffVelocityCurve;
}//getTVFCutoffVelocityCurve

unsigned char SonicCellData::PatchTone::getTVFCutoffVelocitySens() const
{
    return TVFCutoffVelocitySens;
}//getTVFCutoffVelocitySens

unsigned char SonicCellData::PatchTone::getTVFResonance() const
{
    return TVFResonance;
}//getTVFResonance

unsigned char SonicCellData::PatchTone::getTVFResonanceVelocitySens() const
{
    return TVFResonanceVelocitySens;
}//getTVFResonanceVelocitySens

unsigned char SonicCellData::PatchTone::getTVFEnvDepth() const
{
    return TVFEnvDepth;
}//getTVFEnvDepth

unsigned char SonicCellData::PatchTone::getTVFEnvVelocityCurve() const
{
    return TVFEnvVelocityCurve;
}//getTVFEnvVelocityCurve

unsigned char SonicCellData::PatchTone::getTVFEnvVelocitySens() const
{
    return TVFEnvVelocitySens;
}//getTVFEnvVelocitySens

unsigned char SonicCellData::PatchTone::getTVFEnvTime1VelocitySens() const
{
    return TVFEnvTime1VelocitySens;
}//getTVFEnvTime1VelocitySens

unsigned char SonicCellData::PatchTone::getTVFEnvTime4VelocitySens() const
{
    return TVFEnvTime4VelocitySens;
}//getTVFEnvTime4VelocitySens

unsigned char SonicCellData::PatchTone::getTVFEnvTimeKeyfollow() const
{
    return TVFEnvTimeKeyfollow;
}//getTVFEnvTimeKeyfollow

unsigned char SonicCellData::PatchTone::getTVFEnvTime(unsigned int index) const
{
    if (index < TVFEnvTime.size()) {
        return TVFEnvTime[index];
    } else {
        return 0;
    }//if
}//getTVFEnvTime

unsigned char SonicCellData::PatchTone::getTVFEnvLevel(unsigned int index) const
{
    if (index < TVFEnvLevel.size()) {
        return TVFEnvLevel[index];
    } else {
        return 0;
    }//if
}//getTVFEnvLevel

unsigned char SonicCellData::PatchTone::getBiasLevel() const
{
    return BiasLevel;
}//getBiasLevel

unsigned char SonicCellData::PatchTone::getBiasPosition() const
{
    return BiasPosition;
}//getBiasPosition

unsigned char SonicCellData::PatchTone::getBiasDirection() const
{
    return BiasDirection;
}//getBiasDirection

unsigned char SonicCellData::PatchTone::getTVALevelVelocityCurve() const
{
    return TVALevelVelocityCurve;
}//getTVALevelVelocityCurve

unsigned char SonicCellData::PatchTone::getTVALevelVelocitySens() const
{
    return TVALevelVelocitySens;
}//getTVALevelVelocitySens

unsigned char SonicCellData::PatchTone::getTVAEnvTime1VelocitySens() const
{
    return TVAEnvTime1VelocitySens;
}//getTVAEnvTime1VelocitySens

unsigned char SonicCellData::PatchTone::getTVAEnvTime4VelocitySens() const
{
    return TVAEnvTime4VelocitySens;
}//getTVAEnvTime4VelocitySens

unsigned char SonicCellData::PatchTone::getTVAEnvTimeKeyfollow() const
{
    return TVAEnvTimeKeyfollow;
}//getTVAEnvTimeKeyfollow

unsigned char SonicCellData::PatchTone::getTVAEnvTime(unsigned int index) const
{
    if (index < TVAEnvTime.size()) {
        return TVAEnvTime[index];
    } else {
        return 0;
    }//if
}//getTVAEnvTime

unsigned char SonicCellData::PatchTone::getTVAEnvLevel(unsigned int index) const
{
    if (index < TVAEnvLevel.size()) {
        return TVAEnvLevel[index];
    } else {
        return 0;
    }//if
}//getTVAEnvLevel

unsigned char SonicCellData::PatchTone::getLFOWaveform(unsigned int index) const
{
    if (index < LFOWaveform.size()) {
        return LFOWaveform[index];
    } else {
        return 0;
    }//if
}//getLFOWaveform

unsigned short SonicCellData::PatchTone::getLFORate(unsigned int index) const
{
    if (index < LFORate.size()) {
        return LFORate[index];
    } else {
        return 0;
    }//if
}//getLFORate

unsigned char SonicCellData::PatchTone::getLFOOffset(unsigned int index) const
{
    if (index < LFOOffset.size()) {
        return LFOOffset[index];
    } else {
        return 0;
    }//if
}//getLFOOffset

unsigned char SonicCellData::PatchTone::getLFORateDetune(unsigned int index) const
{
    if (index < LFORateDetune.size()) {
        return LFORateDetune[index];
    } else {
        return 0;
    }//if
}//getLFORateDetune

unsigned char SonicCellData::PatchTone::getLFODelayTime(unsigned int index) const
{
    if (index < LFODelayTime.size()) {
        return LFODelayTime[index];
    } else {
        return 0;
    }//if
}//getLFODelayTime

unsigned char SonicCellData::PatchTone::getLFODelayTimeKeyfollow(unsigned int index) const
{
    if (index < LFODelayTimeKeyfollow.size()) {
        return LFODelayTimeKeyfollow[index];
    } else {
        return 0;
    }//if
}//getLFODelayTimeKeyfollow

unsigned char SonicCellData::PatchTone::getLFOFadeMode(unsigned int index) const
{
    if (index < LFOFadeMode.size()) {
        return LFOFadeMode[index];
    } else {
        return 0;
    }//if
}//getLFOFadeMode

unsigned char SonicCellData::PatchTone::getLFOFadeTime(unsigned int index) const
{
    if (index < LFOFadeTime.size()) {
        return LFOFadeTime[index];
    } else {
        return 0;
    }//if
}//getLFOFadeTime

unsigned char SonicCellData::PatchTone::getLFOKeyTrigger(unsigned int index) const
{
    if (index < LFOKeyTrigger.size()) {
        return LFOKeyTrigger[index];
    } else {
        return 0;
    }//if
}//getLFOKeyTrigger

unsigned char SonicCellData::PatchTone::getLFOPitchDepth(unsigned int index) const
{
    if (index < LFOPitchDepth.size()) {
        return LFOPitchDepth[index];
    } else {
        return 0;
    }//if
}//getLFOPitchDepth

unsigned char SonicCellData::PatchTone::getLFOTVFDepth(unsigned int index) const
{
    if (index < LFOTVFDepth.size()) {
        return LFOTVFDepth[index];
    } else {
        return 0;
    }//if
}//getLFOTVFDepth

unsigned char SonicCellData::PatchTone::getLFOTVADepth(unsigned int index) const
{
    if (index < LFOTVADepth.size()) {
        return LFOTVADepth[index];
    } else {
        return 0;
    }//if
}//getLFOTVADepth

unsigned char SonicCellData::PatchTone::getLFOPanDepth(unsigned int index) const
{
    if (index < LFOPanDepth.size()) {
        return LFOPanDepth[index];
    } else {
        return 0;
    }//if
}//getLFOPanDepth

unsigned char SonicCellData::PatchTone::getLFOStepType() const
{
    return LFOStepType;
}//getLFOStepType

unsigned char SonicCellData::PatchTone::getLFOStep(unsigned int index) const
{
    if (index < LFOStep.size()) {
        return LFOStep[index];
    } else {
        return 0;
    }//if
}//getLFOStep

//Sets
void SonicCellData::PatchTone::setToneLevel(unsigned char value)
{
    ToneLevel = value;
}//setToneLevel

void SonicCellData::PatchTone::setToneCoarseTune(unsigned char value)
{
    ToneCoarseTune = value;
}//setToneCoarseTune

void SonicCellData::PatchTone::setToneFineTune(unsigned char value)
{
    ToneFineTune = value;
}//setToneFineTune

void SonicCellData::PatchTone::setToneRandomPitchDepth(unsigned char value)
{
    ToneRandomPitchDepth = value;
}//setToneRandomPitchDepth

void SonicCellData::PatchTone::setTonePan(unsigned char value)
{
    TonePan = value;
}//setTonePan

void SonicCellData::PatchTone::setTonePanKeyfollow(unsigned char value)
{
    TonePanKeyfollow = value;
}//setTonePanKeyfollow

void SonicCellData::PatchTone::setToneRandomPanDepth(unsigned char value)
{
    ToneRandomPanDepth = value;
}//setToneRandomPanDepth

void SonicCellData::PatchTone::setToneAlternatePanDepth(unsigned char value)
{
    ToneAlternatePanDepth = value;
}//setToneAlternatePanDepth

void SonicCellData::PatchTone::setToneEnvMode(unsigned char value)
{
    ToneEnvMode = value;
}//setToneEnvMode

void SonicCellData::PatchTone::setToneDelayMode(unsigned char value)
{
    ToneDelayMode = value;
}//setToneDelayMode

void SonicCellData::PatchTone::setToneDelayTime(unsigned short value)
{
    ToneDelayTime = value;
}//setToneDelayTime


void SonicCellData::PatchTone::setToneDrySendLevel(unsigned char value)
{
    ToneDrySendLevel = value;
}//setToneDrySendLevel

void SonicCellData::PatchTone::setToneChorusSendLevelMFX(unsigned char value)
{
    ToneChorusSendLevelMFX = value;
}//setToneChorusSendLevelMFX

void SonicCellData::PatchTone::setToneReverbSendLevelMFX(unsigned char value)
{
    ToneReverbSendLevelMFX = value;
}//setToneReverbSendLevelMFX

void SonicCellData::PatchTone::setToneChorusSendLevelNonMFX(unsigned char value)
{
    ToneChorusSendLevelNonMFX = value;
}//setToneChorusSendLevelNonMFX

void SonicCellData::PatchTone::setToneReverbSendLevelNonMFX(unsigned char value)
{
    ToneReverbSendLevelNonMFX = value;
}//setToneReverbSendLevelNonMFX

void SonicCellData::PatchTone::setToneOutputAssign(unsigned char value)
{
    ToneOutputAssign = value;
}//setToneOutputAssign


void SonicCellData::PatchTone::setToneReceiveBender(unsigned char value)
{
    ToneReceiveBender = value;
}//setToneReceiveBender

void SonicCellData::PatchTone::setToneReceiveExpression(unsigned char value)
{
    ToneReceiveExpression = value;
}//setToneReceiveExpression

void SonicCellData::PatchTone::setToneReceiveHold_1(unsigned char value)
{
    ToneReceiveHold_1 = value;
}//setToneReceiveHold_1

void SonicCellData::PatchTone::setToneReceivePanMode(unsigned char value)
{
    ToneReceivePanMode = value;
}//setToneReceivePanMode

void SonicCellData::PatchTone::setToneRedamperSwitch(unsigned char value)
{
    ToneRedamperSwitch = value;
}//setToneRedamperSwitch

void SonicCellData::PatchTone::setToneControlSwitch(unsigned int control, unsigned int index, unsigned char value)
{
    if ((control < ToneControlSwitch.size()) && (index < ToneControlSwitch[0].size())) {
        ToneControlSwitch[control][index] = value;
    }//if
}//setToneControlSwitch

void SonicCellData::PatchTone::setWaveGroupType(unsigned char value)
{
    WaveGroupType = value;
}//setWaveGroupType

void SonicCellData::PatchTone::setWaveGroupID(unsigned int value)
{
    WaveGroupID = value;
}//setWaveGroupID

void SonicCellData::PatchTone::setWaveNumberLMono(unsigned int value)
{
    WaveNumberLMono = value;
}//setWaveNumberLMono

void SonicCellData::PatchTone::setWaveNumberR(unsigned int value)
{
    WaveNumberR = value;
}//setWaveNumberR

void SonicCellData::PatchTone::setWaveGain(unsigned char value)
{
    WaveGain = value;
}//setWaveGain

void SonicCellData::PatchTone::setWaveFXMSwitch(unsigned char value)
{
    WaveFXMSwitch = value;
}//setWaveFXMSwitch

void SonicCellData::PatchTone::setWaveFXMColor(unsigned char value)
{
    WaveFXMColor = value;
}//setWaveFXMColor

void SonicCellData::PatchTone::setWaveFXMDepth(unsigned char value)
{
    WaveFXMDepth = value;
}//setWaveFXMDepth

void SonicCellData::PatchTone::setWaveTempoSync(unsigned char value)
{
    WaveTempoSync = value;
}//setWaveTempoSync

void SonicCellData::PatchTone::setWavePitchKeyfollow(unsigned char value)
{
    WavePitchKeyfollow = value;
}//setWavePitchKeyfollow

void SonicCellData::PatchTone::setPitchEnvDepth(unsigned char value)
{
    PitchEnvDepth = value;
}//setPitchEnvDepth

void SonicCellData::PatchTone::setPitchEnvVelocitySens(unsigned char value)
{
    PitchEnvVelocitySens = value;
}//setPitchEnvVelocitySens

void SonicCellData::PatchTone::setPitchEnvTime1VelocitySens(unsigned char value)
{
    PitchEnvTime1VelocitySens = value;
}//setPitchEnvTime1VelocitySens

void SonicCellData::PatchTone::setPitchEnvTime4VelocitySens(unsigned char value)
{
    PitchEnvTime4VelocitySens = value;
}//setPitchEnvTime4VelocitySens

void SonicCellData::PatchTone::setPitchEnvTimeKeyfollow(unsigned char value)
{
    PitchEnvTimeKeyfollow = value;
}//setPitchEnvTimeKeyfollow


void SonicCellData::PatchTone::setPitchEnvTime(unsigned int index, unsigned char value)
{
    if (index < PitchEnvTime.size()) {
        PitchEnvTime[index] = value;
    }//if
}//setPitchEnvTime

void SonicCellData::PatchTone::setPitchEnvLevel(unsigned int index, unsigned char value)
{
    if (index < PitchEnvLevel.size()) {
        PitchEnvLevel[index] = value;
    }//if
}//setPitchEnvLevel

void SonicCellData::PatchTone::setTVFFilterType(unsigned char value)
{
    TVFFilterType = value;
}//setTVFFilterType

void SonicCellData::PatchTone::setTVFCutoffFrequency(unsigned char value)
{
    TVFCutoffFrequency = value;
}//setTVFCutoffFrequency

void SonicCellData::PatchTone::setTVFCutoffKeyfollow(unsigned char value)
{
    TVFCutoffKeyfollow = value;
}//setTVFCutoffKeyfollow

void SonicCellData::PatchTone::setTVFCutoffVelocityCurve(unsigned char value)
{
    TVFCutoffVelocityCurve = value;
}//setTVFCutoffVelocityCurve

void SonicCellData::PatchTone::setTVFCutoffVelocitySens(unsigned char value)
{
    TVFCutoffVelocitySens = value;
}//setTVFCutoffVelocitySens

void SonicCellData::PatchTone::setTVFResonance(unsigned char value)
{
    TVFResonance = value;
}//setTVFResonance

void SonicCellData::PatchTone::setTVFResonanceVelocitySens(unsigned char value)
{
    TVFResonanceVelocitySens = value;
}//setTVFResonanceVelocitySens

void SonicCellData::PatchTone::setTVFEnvDepth(unsigned char value)
{
    TVFEnvDepth = value;
}//setTVFEnvDepth

void SonicCellData::PatchTone::setTVFEnvVelocityCurve(unsigned char value)
{
    TVFEnvVelocityCurve = value;
}//setTVFEnvVelocityCurve

void SonicCellData::PatchTone::setTVFEnvVelocitySens(unsigned char value)
{
    TVFEnvVelocitySens = value;
}//setTVFEnvVelocitySens

void SonicCellData::PatchTone::setTVFEnvTime1VelocitySens(unsigned char value)
{
    TVFEnvTime1VelocitySens = value;
}//setTVFEnvTime1VelocitySens

void SonicCellData::PatchTone::setTVFEnvTime4VelocitySens(unsigned char value)
{
    TVFEnvTime4VelocitySens = value;
}//setTVFEnvTime4VelocitySens

void SonicCellData::PatchTone::setTVFEnvTimeKeyfollow(unsigned char value)
{
    TVFEnvTimeKeyfollow = value;
}//setTVFEnvTimeKeyfollow

void SonicCellData::PatchTone::setTVFEnvTime(unsigned int index, unsigned char value)
{
    if (index < TVFEnvTime.size()) {
        TVFEnvTime[index] = value;
    }//if
}//setTVFEnvTime

void SonicCellData::PatchTone::setTVFEnvLevel(unsigned int index, unsigned char value)
{
    if (index < TVFEnvLevel.size()) {
        TVFEnvLevel[index] = value;
    }//if
}//setTVFEnvLevel

void SonicCellData::PatchTone::setBiasLevel(unsigned char value)
{
    BiasLevel = value;
}//setBiasLevel

void SonicCellData::PatchTone::setBiasPosition(unsigned char value)
{
    BiasPosition = value;
}//setBiasPosition

void SonicCellData::PatchTone::setBiasDirection(unsigned char value)
{
    BiasDirection = value;
}//setBiasDirection

void SonicCellData::PatchTone::setTVALevelVelocityCurve(unsigned char value)
{
    TVALevelVelocityCurve = value;
}//setTVALevelVelocityCurve

void SonicCellData::PatchTone::setTVALevelVelocitySens(unsigned char value)
{
    TVALevelVelocitySens = value;
}//setTVALevelVelocitySens

void SonicCellData::PatchTone::setTVAEnvTime1VelocitySens(unsigned char value)
{
    TVAEnvTime1VelocitySens = value;
}//setTVAEnvTime1VelocitySens

void SonicCellData::PatchTone::setTVAEnvTime4VelocitySens(unsigned char value)
{
    TVAEnvTime4VelocitySens = value;
}//setTVAEnvTime4VelocitySens

void SonicCellData::PatchTone::setTVAEnvTimeKeyfollow(unsigned char value)
{
    TVAEnvTimeKeyfollow = value;
}//setTVAEnvTimeKeyfollow

void SonicCellData::PatchTone::setTVAEnvTime(unsigned int index, unsigned char value)
{
    if (index < TVAEnvTime.size()) {
        TVAEnvTime[index] = value;
    }//if
}//setTVAEnvTime

void SonicCellData::PatchTone::setTVAEnvLevel(unsigned int index, unsigned char value)
{
    if (index < TVAEnvLevel.size()) {
        TVAEnvLevel[index] = value;
    }//if
}//setTVAEnvLevel

void SonicCellData::PatchTone::setLFOWaveform(unsigned int index, unsigned char value)
{
    if (index < LFOWaveform.size()) {
        LFOWaveform[index] = value;
    }//if
}//setLFOWaveform

void SonicCellData::PatchTone::setLFORate(unsigned int index, unsigned short value)
{
    if (index < LFORate.size()) {
        LFORate[index] = value;
    }//if
}//setLFORate

void SonicCellData::PatchTone::setLFOOffset(unsigned int index, unsigned char value)
{
    if (index < LFOOffset.size()) {
        LFOOffset[index] = value;
    }//if
}//setLFOOffset

void SonicCellData::PatchTone::setLFORateDetune(unsigned int index, unsigned char value)
{
    if (index < LFORateDetune.size()) {
        LFORateDetune[index] = value;
    }//if
}//setLFORateDetune

void SonicCellData::PatchTone::setLFODelayTime(unsigned int index, unsigned char value)
{
    if (index < LFODelayTime.size()) {
        LFODelayTime[index] = value;
    }//if
}//setLFODelayTime

void SonicCellData::PatchTone::setLFODelayTimeKeyfollow(unsigned int index, unsigned char value)
{
    if (index < LFODelayTimeKeyfollow.size()) {
        LFODelayTimeKeyfollow[index] = value;
    }//if
}//setLFODelayTimeKeyfollow

void SonicCellData::PatchTone::setLFOFadeMode(unsigned int index, unsigned char value)
{
    if (index < LFOFadeMode.size()) {
        LFOFadeMode[index] = value;
    }//if
}//setLFOFadeMode

void SonicCellData::PatchTone::setLFOFadeTime(unsigned int index, unsigned char value)
{
    if (index < LFOFadeTime.size()) {
        LFOFadeTime[index] = value;
    }//if
}//setLFOFadeTime

void SonicCellData::PatchTone::setLFOKeyTrigger(unsigned int index, unsigned char value)
{
    if (index < LFOKeyTrigger.size()) {
        LFOKeyTrigger[index] = value;
    }//if
}//setLFOKeyTrigger

void SonicCellData::PatchTone::setLFOPitchDepth(unsigned int index, unsigned char value)
{
    if (index < LFOPitchDepth.size()) {
        LFOPitchDepth[index] = value;
    }//if
}//setLFOPitchDepth

void SonicCellData::PatchTone::setLFOTVFDepth(unsigned int index, unsigned char value)
{
    if (index < LFOTVFDepth.size()) {
        LFOTVFDepth[index] = value;
    }//if
}//setLFOTVFDepth

void SonicCellData::PatchTone::setLFOTVADepth(unsigned int index, unsigned char value)
{
    if (index < LFOTVADepth.size()) {
        LFOTVADepth[index] = value;
    }//if
}//setLFOTVADepth

void SonicCellData::PatchTone::setLFOPanDepth(unsigned int index, unsigned char value)
{
    if (index < LFOPanDepth.size()) {
        LFOPanDepth[index] = value;
    }//if
}//setLFOPanDepth

void SonicCellData::PatchTone::setLFOStepType(unsigned char value)
{
    LFOStepType = value;
}//setLFOStepType

void SonicCellData::PatchTone::setLFOStep(unsigned int index, unsigned char value)
{
    if (index < LFOStep.size()) {
        LFOStep[index] = value;
    }//if
}//setLFOStep

boost::shared_ptr<SonicCellData::PatchTone> SonicCellData::PatchTone::deepClone() const
{
    boost::shared_ptr<SonicCellData::PatchTone> clone(new SonicCellData::PatchTone);

    *clone = *this;

    return clone;
}//deepClone

template<class Archive>
void SonicCellData::PatchTone::serialize(Archive &ar, const unsigned int version)
{
    ar & BOOST_SERIALIZATION_NVP(toneNum);

    ar & BOOST_SERIALIZATION_NVP(ToneLevel);
    ar & BOOST_SERIALIZATION_NVP(ToneCoarseTune);
    ar & BOOST_SERIALIZATION_NVP(ToneFineTune);
    ar & BOOST_SERIALIZATION_NVP(ToneRandomPitchDepth);
    ar & BOOST_SERIALIZATION_NVP(TonePan);
    ar & BOOST_SERIALIZATION_NVP(TonePanKeyfollow);
    ar & BOOST_SERIALIZATION_NVP(ToneRandomPanDepth);
    ar & BOOST_SERIALIZATION_NVP(ToneAlternatePanDepth);
    ar & BOOST_SERIALIZATION_NVP(ToneEnvMode);
    ar & BOOST_SERIALIZATION_NVP(ToneDelayMode);
    ar & BOOST_SERIALIZATION_NVP(ToneDelayTime);
    ar & BOOST_SERIALIZATION_NVP(ToneDrySendLevel);
    ar & BOOST_SERIALIZATION_NVP(ToneChorusSendLevelMFX);
    ar & BOOST_SERIALIZATION_NVP(ToneReverbSendLevelMFX);
    ar & BOOST_SERIALIZATION_NVP(ToneChorusSendLevelNonMFX);
    ar & BOOST_SERIALIZATION_NVP(ToneReverbSendLevelNonMFX);
    ar & BOOST_SERIALIZATION_NVP(ToneOutputAssign);

    ar & BOOST_SERIALIZATION_NVP(ToneReceiveBender);
    ar & BOOST_SERIALIZATION_NVP(ToneReceiveExpression);
    ar & BOOST_SERIALIZATION_NVP(ToneReceiveHold_1);
    ar & BOOST_SERIALIZATION_NVP(ToneReceivePanMode);
    ar & BOOST_SERIALIZATION_NVP(ToneRedamperSwitch);

    for (unsigned int index1 = 0; index1 < ToneControlSwitch.size(); ++index1) {
        for (unsigned int index2 = 0; index2 < ToneControlSwitch[0].size(); ++index2) {
            unsigned char ToneControlSwitchVal = ToneControlSwitch[index1][index2];
            ar & BOOST_SERIALIZATION_NVP(ToneControlSwitchVal);
            ToneControlSwitch[index1][index2] = ToneControlSwitchVal;
        }//for
    }//for

    ar & BOOST_SERIALIZATION_NVP(WaveGroupType);
    ar & BOOST_SERIALIZATION_NVP(WaveGroupID);
    ar & BOOST_SERIALIZATION_NVP(WaveNumberLMono);
    ar & BOOST_SERIALIZATION_NVP(WaveNumberR);
    ar & BOOST_SERIALIZATION_NVP(WaveGain);
    ar & BOOST_SERIALIZATION_NVP(WaveFXMSwitch);
    ar & BOOST_SERIALIZATION_NVP(WaveFXMColor);
    ar & BOOST_SERIALIZATION_NVP(WaveFXMDepth);
    ar & BOOST_SERIALIZATION_NVP(WaveTempoSync);
    ar & BOOST_SERIALIZATION_NVP(WavePitchKeyfollow);
    ar & BOOST_SERIALIZATION_NVP(PitchEnvDepth);
    ar & BOOST_SERIALIZATION_NVP(PitchEnvVelocitySens);
    ar & BOOST_SERIALIZATION_NVP(PitchEnvTime1VelocitySens);
    ar & BOOST_SERIALIZATION_NVP(PitchEnvTime4VelocitySens);
    ar & BOOST_SERIALIZATION_NVP(PitchEnvTimeKeyfollow);

    for (unsigned int index = 0; index < PitchEnvTime.size(); ++index) {
        unsigned char PitchEnvTimeVal = PitchEnvTime[index];
        ar & BOOST_SERIALIZATION_NVP(PitchEnvTimeVal);
        PitchEnvTime[index] = PitchEnvTimeVal;
    }//for

    for (unsigned int index = 0; index < PitchEnvLevel.size(); ++index) {
        unsigned char PitchEnvLevelVal = PitchEnvLevel[index];
        ar & BOOST_SERIALIZATION_NVP(PitchEnvLevelVal);
        PitchEnvLevel[index] = PitchEnvLevelVal;
    }//for

    ar & BOOST_SERIALIZATION_NVP(TVFFilterType);
    ar & BOOST_SERIALIZATION_NVP(TVFCutoffFrequency);
    ar & BOOST_SERIALIZATION_NVP(TVFCutoffKeyfollow);
    ar & BOOST_SERIALIZATION_NVP(TVFCutoffVelocityCurve);
    ar & BOOST_SERIALIZATION_NVP(TVFCutoffVelocitySens);
    ar & BOOST_SERIALIZATION_NVP(TVFResonance);
    ar & BOOST_SERIALIZATION_NVP(TVFResonanceVelocitySens);
    ar & BOOST_SERIALIZATION_NVP(TVFEnvDepth);
    ar & BOOST_SERIALIZATION_NVP(TVFEnvVelocityCurve);
    ar & BOOST_SERIALIZATION_NVP(TVFEnvVelocitySens);
    ar & BOOST_SERIALIZATION_NVP(TVFEnvTime1VelocitySens);
    ar & BOOST_SERIALIZATION_NVP(TVFEnvTime4VelocitySens);
    ar & BOOST_SERIALIZATION_NVP(TVFEnvTimeKeyfollow);

    for (unsigned int index = 0; index < TVFEnvTime.size(); ++index) {
        unsigned char TVFEnvTimeVal = TVFEnvTime[index];
        ar & BOOST_SERIALIZATION_NVP(TVFEnvTimeVal);
        TVFEnvTime[index] = TVFEnvTimeVal;
    }//for

    for (unsigned int index = 0; index < TVFEnvLevel.size(); ++index) {
        unsigned char TVFEnvLevelVal = TVFEnvLevel[index];
        ar & BOOST_SERIALIZATION_NVP(TVFEnvLevelVal);
        TVFEnvLevel[index] = TVFEnvLevelVal;
    }//for

    ar & BOOST_SERIALIZATION_NVP(BiasLevel);
    ar & BOOST_SERIALIZATION_NVP(BiasPosition);
    ar & BOOST_SERIALIZATION_NVP(BiasDirection);
    ar & BOOST_SERIALIZATION_NVP(TVALevelVelocityCurve);
    ar & BOOST_SERIALIZATION_NVP(TVALevelVelocitySens);
    ar & BOOST_SERIALIZATION_NVP(TVAEnvTime1VelocitySens);
    ar & BOOST_SERIALIZATION_NVP(TVAEnvTime4VelocitySens);
    ar & BOOST_SERIALIZATION_NVP(TVAEnvTimeKeyfollow);

    for (unsigned int index = 0; index < TVAEnvTime.size(); ++index) {
        unsigned char TVAEnvTimeVal = TVAEnvTime[index];
        ar & BOOST_SERIALIZATION_NVP(TVAEnvTimeVal);
        TVAEnvTime[index] = TVAEnvTimeVal;
    }//for

    for (unsigned int index = 0; index < TVAEnvLevel.size(); ++index) {
        unsigned char TVAEnvLevelVal = TVAEnvLevel[index];
        ar & BOOST_SERIALIZATION_NVP(TVAEnvLevelVal);
        TVAEnvLevel[index] = TVAEnvLevelVal;
    }//for

    for (unsigned int index = 0; index < 2; ++index) {
        unsigned char LFOWaveformVal = LFOWaveform[index];
        unsigned short LFORateVal = LFORate[index];
        unsigned char LFOOffsetVal = LFOOffset[index];
        unsigned char LFORateDetuneVal = LFORateDetune[index];
        unsigned char LFODelayTimeVal = LFODelayTime[index];
        unsigned char LFODelayTimeKeyfollowVal = LFODelayTimeKeyfollow[index];
        unsigned char LFOFadeModeVal = LFOFadeMode[index];
        unsigned char LFOFadeTimeVal = LFOFadeTime[index];
        unsigned char LFOKeyTriggerVal = LFOKeyTrigger[index];
        unsigned char LFOPitchDepthVal = LFOPitchDepth[index];
        unsigned char LFOTVFDepthVal = LFOTVFDepth[index];
        unsigned char LFOTVADepthVal = LFOTVADepth[index];
        unsigned char LFOPanDepthVal = LFOPanDepth[index];

        ar & BOOST_SERIALIZATION_NVP(LFOWaveformVal);
        ar & BOOST_SERIALIZATION_NVP(LFORateVal);
        ar & BOOST_SERIALIZATION_NVP(LFOOffsetVal);
        ar & BOOST_SERIALIZATION_NVP(LFORateDetuneVal);
        ar & BOOST_SERIALIZATION_NVP(LFODelayTimeVal);
        ar & BOOST_SERIALIZATION_NVP(LFODelayTimeKeyfollowVal);
        ar & BOOST_SERIALIZATION_NVP(LFOFadeModeVal);
        ar & BOOST_SERIALIZATION_NVP(LFOFadeTimeVal);
        ar & BOOST_SERIALIZATION_NVP(LFOKeyTriggerVal);
        ar & BOOST_SERIALIZATION_NVP(LFOPitchDepthVal);
        ar & BOOST_SERIALIZATION_NVP(LFOTVFDepthVal);
        ar & BOOST_SERIALIZATION_NVP(LFOTVADepthVal);
        ar & BOOST_SERIALIZATION_NVP(LFOPanDepthVal);

        LFOWaveform[index] = LFOWaveformVal;
        LFORate[index] = LFORateVal;
        LFOOffset[index] = LFOOffsetVal;
        LFORateDetune[index] = LFORateDetuneVal;
        LFODelayTime[index] = LFODelayTimeVal;
        LFODelayTimeKeyfollow[index] = LFODelayTimeKeyfollowVal;
        LFOFadeMode[index] = LFOFadeModeVal;
        LFOFadeTime[index] = LFOFadeTimeVal;
        LFOKeyTrigger[index] = LFOKeyTriggerVal;
        LFOPitchDepth[index] = LFOPitchDepthVal;
        LFOTVFDepth[index] = LFOTVFDepthVal;
        LFOTVADepth[index] = LFOTVADepthVal;
        LFOPanDepth[index] = LFOPanDepthVal;
    }//for

    ar & BOOST_SERIALIZATION_NVP(LFOStepType);

    for (unsigned int index = 0; index < LFOStep.size(); ++index) {
        unsigned char LFOStepVal = LFOStep[index];
        ar & BOOST_SERIALIZATION_NVP(LFOStepVal);
        LFOStep[index] = LFOStepVal;
    }//for
}//serialize


template void SonicCellData::PatchTone::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive &ar, const unsigned int version);

template void SonicCellData::PatchTone::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive &ar, const unsigned int version);


