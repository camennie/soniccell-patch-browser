/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Patch.h"
#include "PatchCommon.h"
#include "PatchCommonMFX.h"
#include "PatchCommonChorus.h"
#include "PatchCommonReverb.h"
#include "PatchToneMixTable.h"
#include "PatchTone.h"
#include "../main.h"
#include "../widgets.h"
#include <iostream>

SonicCellData::Patch::Patch()
{
    patchCommon.reset(new SonicCellData::PatchCommon);
    patchCommonMFX.reset(new SonicCellData::PatchCommonMFX);
    patchCommonChorus.reset(new SonicCellData::PatchCommonChorus);
    patchCommonReverb.reset(new SonicCellData::PatchCommonReverb);
    patchToneMixTable.reset(new SonicCellData::PatchToneMixTable);

    for (unsigned int pos = 0; pos < patchTones.size(); ++pos) {
        patchTones[pos].reset(new SonicCellData::PatchTone(pos));
    }//for
}//constructor

SonicCellData::Patch::~Patch()
{
    //Nothing
}//destructor

//Data will have address stripped from it
bool SonicCellData::Patch::handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize)
{
    char patchOffset = (offset >> 8) & 0xfe;

    offset -= (patchOffset << 8);
    offset = ((offset & 0xff00) >> 8) * 128 + (offset & 0x00ff);

    switch (patchOffset) {
        case 0x00:
            patchCommon->handleSysExMessage(offset, data, dataSize);
            return true;

        case 0x02:
            patchCommonMFX->handleSysExMessage(offset, data, dataSize);
            return true;

        case 0x04:
            patchCommonChorus->handleSysExMessage(offset, data, dataSize);
            return true;

        case 0x06:
            patchCommonReverb->handleSysExMessage(offset, data, dataSize);
            return true;

        case 0x10:
            patchToneMixTable->handleSysExMessage(offset, data, dataSize);
            return true;

        case 0x20:
            patchTones[0]->handleSysExMessage(offset, data, dataSize);
            return true;

        case 0x22:
            patchTones[1]->handleSysExMessage(offset, data, dataSize);
            return true;

        case 0x24:
            patchTones[2]->handleSysExMessage(offset, data, dataSize);
            return true;

        case 0x26:
            patchTones[3]->handleSysExMessage(offset, data, dataSize);
            return true;

        default:
            break;
    }//switch

    std::cout << "Unknown user patch message: " << patchOffset << std::endl;

    return false;
}//handleSysExMessage

std::vector<std::vector<jack_midi_data_t> > SonicCellData::Patch::createUpdateRequest(unsigned char myIndex) const
{
    std::vector<std::vector<jack_midi_data_t> > retData;

    std::vector<jack_midi_data_t> tmpData;

    tmpData = patchCommon->createUpdateRequest(myIndex);
    if (false == tmpData.empty()) {
        retData.push_back(tmpData);
    }//if

    tmpData = patchCommonMFX->createUpdateRequest(myIndex);
    if (false == tmpData.empty()) {
        retData.push_back(tmpData);
    }//if

    tmpData = patchCommonChorus->createUpdateRequest(myIndex);
    if (false == tmpData.empty()) {
        retData.push_back(tmpData);
    }//if

    tmpData = patchCommonReverb->createUpdateRequest(myIndex);
    if (false == tmpData.empty()) {
        retData.push_back(tmpData);
    }//if

    tmpData = patchToneMixTable->createUpdateRequest(myIndex);
    if (false == tmpData.empty()) {
        retData.push_back(tmpData);
    }//if

    for (unsigned int index = 0; index < patchTones.size(); ++index) {
        tmpData = patchTones[index]->createUpdateRequest(myIndex);
        if (false == tmpData.empty()) {
            retData.push_back(tmpData);
        }//if
    }//for

    return retData;
}//createUpdateRequest

std::vector<std::vector<jack_midi_data_t> > SonicCellData::Patch::createWriteRequest(unsigned char myIndex) const
{
    std::vector<std::vector<jack_midi_data_t> > retData;

    std::vector<jack_midi_data_t> tmpData;

    tmpData = patchCommon->createWriteRequest(myIndex);
    retData.push_back(tmpData);

    tmpData = patchCommonMFX->createWriteRequest(myIndex);
    retData.push_back(tmpData);

    tmpData = patchCommonChorus->createWriteRequest(myIndex);
    retData.push_back(tmpData);

    tmpData = patchCommonReverb->createWriteRequest(myIndex);
    retData.push_back(tmpData);

    tmpData = patchToneMixTable->createWriteRequest(myIndex);
    retData.push_back(tmpData);

    for (unsigned int index = 0; index < patchTones.size(); ++index) {
        tmpData = patchTones[index]->createWriteRequest(myIndex);
        retData.push_back(tmpData);
    }//for

    return retData;
}//createWriteRequest

boost::shared_ptr<SonicCellData::PatchCommon> SonicCellData::Patch::getPatchCommon() const
{
    return patchCommon;
}//getPatchCommon

boost::shared_ptr<SonicCellData::PatchCommonMFX> SonicCellData::Patch::getPatchCommonMFX() const
{
    return patchCommonMFX;
}//getPatchCommonMFX

boost::shared_ptr<SonicCellData::PatchCommonChorus> SonicCellData::Patch::getPatchCommonChorus() const
{
    return patchCommonChorus;
}//getPatchCommonChorus

boost::shared_ptr<SonicCellData::PatchCommonReverb> SonicCellData::Patch::getPatchCommonReverb() const
{
    return patchCommonReverb;
}//getPatchCommonReverb

boost::shared_ptr<SonicCellData::PatchToneMixTable> SonicCellData::Patch::getPatchToneMixTable() const
{
    return patchToneMixTable;
}//getPatchToneMixTable

boost::shared_ptr<SonicCellData::PatchTone> SonicCellData::Patch::getPatchTone(unsigned int index) const
{
    if (index < patchTones.size()) {
        return patchTones[index];
    } else {
        return boost::shared_ptr<SonicCellData::PatchTone>();
    }//if
}//getPatchTone

void SonicCellData::Patch::setMemo(unsigned int index, const std::string &memo)
{
    if (index < memos.size()) {
        memos[index] = memo;
    }//if
}//setMemo

std::string SonicCellData::Patch::getMemo(unsigned int index) const
{
    if (index < memos.size()) {
        return memos[index];
    } else {
        return "";
    }//if
}//getMemo

boost::shared_ptr<SonicCellData::Patch> SonicCellData::Patch::deepClone() const
{
    boost::shared_ptr<SonicCellData::Patch> clone(new SonicCellData::Patch);

    clone->patchCommon = this->patchCommon->deepClone();
    clone->patchCommonMFX = this->patchCommonMFX->deepClone();
    clone->patchCommonChorus = this->patchCommonChorus->deepClone();
    clone->patchCommonReverb = this->patchCommonReverb->deepClone();
    clone->patchToneMixTable = this->patchToneMixTable->deepClone();

    for (unsigned int index = 0; index < patchTones.size(); ++index) {
        clone->patchTones[index] = this->patchTones[index]->deepClone();
    }//for

    clone->memos = this->memos;
    return clone;
}//deepClone

template<class Archive>
void SonicCellData::Patch::serialize(Archive &ar, const unsigned int version)
{
	ar & BOOST_SERIALIZATION_NVP(patchCommon);
    
    ar & BOOST_SERIALIZATION_NVP(patchCommonMFX);
    ar & BOOST_SERIALIZATION_NVP(patchCommonChorus);
    ar & BOOST_SERIALIZATION_NVP(patchCommonReverb);
    ar & BOOST_SERIALIZATION_NVP(patchToneMixTable);

    for (unsigned int pos = 0; pos < patchTones.size(); ++pos) {
        PatchTone patchTone(pos);
        patchTone = *patchTones[pos];
        ar & BOOST_SERIALIZATION_NVP(patchTone);
        *patchTones[pos] = patchTone;
    }//for

    if (version > 0) {
        for (unsigned int pos = 0; pos < memos.size(); ++pos) {
            std::string memo = memos[pos];
            ar & BOOST_SERIALIZATION_NVP(memo);
            memos[pos] = memo;
        }//for
    }//if
}//serialize


template void SonicCellData::Patch::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive &ar, const unsigned int version);

template void SonicCellData::Patch::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive &ar, const unsigned int version);

