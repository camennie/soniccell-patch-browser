/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __PATCH_COMMON_REVERB_H
#define __PATCH_COMMON_REVERB_H

#include <boost/array.hpp>
#include <vector>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/access.hpp>
#include <jack/midiport.h>

namespace SonicCellData
{

class PatchCommonReverb
{
    unsigned char ReverbType;
    unsigned char ReverbLevel;
    unsigned char ReverbOutputAssign;
    boost::array<unsigned int, 20> ReverbParameters;

public:
    static const unsigned int fullDataSize = 0x53;

    PatchCommonReverb();
    virtual ~PatchCommonReverb();

    //Data will have address stripped from it
    bool handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize);

    std::vector<jack_midi_data_t> createUpdateRequest(unsigned char myIndex) const;
    std::vector<jack_midi_data_t> createWriteRequest(unsigned char myIndex) const;

    unsigned char getReverbType() const;
    unsigned char getReverbLevel() const;
    unsigned char getReverbOutputAssign() const;
    unsigned int getReverbParameter(unsigned int index) const;

    void setReverbType(unsigned char value);
    void setReverbLevel(unsigned char value);
    void setReverbOutputAssign(unsigned char value);
    void setReverbParameters(unsigned int index, unsigned int value);

    boost::shared_ptr<PatchCommonReverb> deepClone() const;

    template<class Archive> void serialize(Archive &ar, const unsigned int version);
	friend class boost::serialization::access;
};//PatchCommonReverb

}//namespace SonicCellData

#endif

