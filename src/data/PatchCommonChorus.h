/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __PATCH_COMMON_CHORUS_H_
#define __PATCH_COMMON_CHORUS_H_

#include <boost/shared_ptr.hpp>
#include <boost/array.hpp>
#include <vector>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/access.hpp>
#include <jack/midiport.h>

namespace SonicCellData
{

class PatchCommonChorus
{
    unsigned char ChorusType;
    unsigned char ChorusLevel;
    unsigned char ChorusOutputAssign;
    unsigned char ChorusOutputSelect;
    boost::array<unsigned int, 20> ChorusParameters;

public:
    static const unsigned int fullDataSize = 0x54;

    PatchCommonChorus();
    virtual ~PatchCommonChorus();

    //Data will have address stripped from it
    bool handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize);

    std::vector<jack_midi_data_t> createUpdateRequest(unsigned char myIndex) const;
    std::vector<jack_midi_data_t> createWriteRequest(unsigned char myIndex) const;

    unsigned char getChorusType() const;
    unsigned char getChorusLevel() const;
    unsigned char getChorusOutputAssign() const;
    unsigned char getChorusOutputSelect() const;
    unsigned int getChorusParameter(unsigned int index) const;

    void setChorusType(unsigned char value);
    void setChorusLevel(unsigned char value);
    void setChorusOutputAssign(unsigned char value);
    void setChorusOutputSelect(unsigned char value);
    void setChorusParameters(unsigned int index, unsigned int value);

    boost::shared_ptr<PatchCommonChorus> deepClone() const;

    template<class Archive> void serialize(Archive &ar, const unsigned int version);
	friend class boost::serialization::access;
};//PatchCommonChorus

}//namespace SonicCellData

#endif
