/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "PatchCommonChorus.h"
#include "../main.h"
#include <iostream>
#include <string.h>

SonicCellData::PatchCommonChorus::PatchCommonChorus()
{
    ChorusType = 0;
    ChorusLevel = 0;
    ChorusOutputAssign = 0;
    ChorusOutputSelect = 0;
    memset(&ChorusParameters[0], 0, sizeof(ChorusParameters));
}//constructor

SonicCellData::PatchCommonChorus::~PatchCommonChorus()
{
    //Nothing
}//destructor

bool SonicCellData::PatchCommonChorus::handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize)
{
    if (dataSize != SonicCellData::PatchCommonChorus::fullDataSize) {
        std::cout << "Initial Chorus dataSize: " << dataSize << std::endl;
    }//if

    switch (offset) {
        case 0x00:
            if (0 == dataSize) break;
            ChorusType = *data;
            data++; dataSize--;
        case 0x01:
            if (0 == dataSize) break;
            ChorusLevel = *data;
            data++; dataSize--;
        case 0x02:
            if (0 == dataSize) break;
            ChorusOutputAssign = *data;
            data++; dataSize--;
        case 0x03:
            if (0 == dataSize) break;
            ChorusOutputSelect = *data;
            data++; dataSize--;
        case 0x04:
            if (4 > dataSize) break;
            ChorusParameters[0] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x08:
            if (4 > dataSize) break;
            ChorusParameters[1] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x0c:
            if (4 > dataSize) break;
            ChorusParameters[2] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x10:
            if (4 > dataSize) break;
            ChorusParameters[3] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x14:
            if (4 > dataSize) break;
            ChorusParameters[4] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x18:
            if (4 > dataSize) break;
            ChorusParameters[5] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x1c:
            if (4 > dataSize) break;
            ChorusParameters[6] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x20:
            if (4 > dataSize) break;
            ChorusParameters[7] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x24:
            if (4 > dataSize) break;
            ChorusParameters[8] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x28:
            if (4 > dataSize) break;
            ChorusParameters[9] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x2c:
            if (4 > dataSize) break;
            ChorusParameters[10] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x30:
            if (4 > dataSize) break;
            ChorusParameters[11] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x34:
            if (4 > dataSize) break;
            ChorusParameters[12] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x38:
            if (4 > dataSize) break;
            ChorusParameters[13] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x3c:
            if (4 > dataSize) break;
            ChorusParameters[14] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x40:
            if (4 > dataSize) break;
            ChorusParameters[15] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x44:
            if (4 > dataSize) break;
            ChorusParameters[16] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x48:
            if (4 > dataSize) break;
            ChorusParameters[17] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x4c:
            if (4 > dataSize) break;
            ChorusParameters[18] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x50:
            if (4 > dataSize) break;
            ChorusParameters[19] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
            break;
        default:
            std::cerr << "Invalid patch common chorus offset: " << offset << std::endl;
            return false;
    }//switch

    if (0 != dataSize) {
        std::cout << "chorus dataSize: " << dataSize << std::endl;
        return false;
    }//if

    assert(0 == dataSize);
    return true;
}//handleSysExMessage

std::vector<jack_midi_data_t> SonicCellData::PatchCommonChorus::createUpdateRequest(unsigned char myIndex) const
{
    std::vector<jack_midi_data_t> requestBuffer;

    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x11};
    std::copy(dataHeader, dataHeader+7, std::back_inserter(requestBuffer));
    
    unsigned char addressHighHigh = 0x30;
    unsigned char addressHighLow = myIndex;
    if (myIndex > 127) {
        addressHighHigh++;
        addressHighLow -= 128;
    }//if

    jack_midi_data_t addressHigh[2] = {addressHighHigh, addressHighLow};
    std::copy(addressHigh, addressHigh+2, std::back_inserter(requestBuffer));

    jack_midi_data_t commonOffset[2] = {0x04, 0x00};
    std::copy(commonOffset, commonOffset+2, std::back_inserter(requestBuffer));

    unsigned int size = SonicCellData::PatchCommonChorus::fullDataSize;
    requestBuffer.push_back((size & 0xff000000) >> 24);
    requestBuffer.push_back((size & 0x00ff0000) >> 16);
    requestBuffer.push_back((size & 0x0000ff00) >> 8);
    requestBuffer.push_back(size & 0x000000ff);

    requestBuffer.push_back(0xf7);

    JackSingleton::addChecksum(requestBuffer);

    return requestBuffer;
}//createUpdateRequest

std::vector<jack_midi_data_t> SonicCellData::PatchCommonChorus::createWriteRequest(unsigned char myIndex) const
{
    std::vector<jack_midi_data_t> requestBuffer;

    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x12};
    std::copy(dataHeader, dataHeader+7, std::back_inserter(requestBuffer));
    
    unsigned char addressHighHigh = 0x30;
    unsigned char addressHighLow = myIndex;
    if (myIndex > 127) {
        addressHighHigh++;
        addressHighLow -= 128;
    }//if

    jack_midi_data_t addressHigh[2] = {addressHighHigh, addressHighLow};
    std::copy(addressHigh, addressHigh+2, std::back_inserter(requestBuffer));

    jack_midi_data_t commonOffset[2] = {0x04, 0x00};
    std::copy(commonOffset, commonOffset+2, std::back_inserter(requestBuffer));

    //Add data
    requestBuffer.push_back(ChorusType);
    requestBuffer.push_back(ChorusLevel);
    requestBuffer.push_back(ChorusOutputAssign);
    requestBuffer.push_back(ChorusOutputSelect);

    for (unsigned int pos = 0; pos < ChorusParameters.size(); ++pos) {
        requestBuffer.push_back((ChorusParameters[pos] & 0xff000001) >> 24);
        requestBuffer.push_back((ChorusParameters[pos] & 0x00ff0000) >> 16);
        requestBuffer.push_back((ChorusParameters[pos] & 0x0000ff00) >> 8);
        requestBuffer.push_back(ChorusParameters[pos] & 0x000000ff);
    }//for

    //Footer
    requestBuffer.push_back(0xf7);

    assert(requestBuffer.size() == SonicCellData::PatchCommonChorus::fullDataSize + 7 + 4 + 1);

    JackSingleton::addChecksum(requestBuffer);

    return requestBuffer;
}//createUpdateRequest

unsigned char SonicCellData::PatchCommonChorus::getChorusType() const
{
    return ChorusType;
}//getChorusType

unsigned char SonicCellData::PatchCommonChorus::getChorusLevel() const
{
    return ChorusLevel;
}//getChorusLevel

unsigned char SonicCellData::PatchCommonChorus::getChorusOutputAssign() const
{
    return ChorusOutputAssign;
}//getChorusOutputAssign

unsigned char SonicCellData::PatchCommonChorus::getChorusOutputSelect() const
{
    return ChorusOutputSelect;
}//getChorusOutputSelect

unsigned int SonicCellData::PatchCommonChorus::getChorusParameter(unsigned int index) const
{
    if (index < ChorusParameters.size()) {
        return ChorusParameters[index];
    } else {
        return 0;
    }//if
}//getChorusParameter

void SonicCellData::PatchCommonChorus::setChorusType(unsigned char value)
{
    ChorusType = value;
}//getChorusType

void SonicCellData::PatchCommonChorus::setChorusLevel(unsigned char value)
{
    ChorusLevel = value;
}//getChorusLevel

void SonicCellData::PatchCommonChorus::setChorusOutputAssign(unsigned char value)
{
    ChorusOutputAssign = value;
}//getChorusOutputAssign

void SonicCellData::PatchCommonChorus::setChorusOutputSelect(unsigned char value)
{
    ChorusOutputSelect = value;
}//getChorusOutputSelect

void SonicCellData::PatchCommonChorus::setChorusParameters(unsigned int index, unsigned int value)
{
    if (index < ChorusParameters.size()) {
        ChorusParameters[index] = value;
    }//if
}//getChorusParameters

boost::shared_ptr<SonicCellData::PatchCommonChorus> SonicCellData::PatchCommonChorus::deepClone() const
{
    boost::shared_ptr<SonicCellData::PatchCommonChorus> clone(new SonicCellData::PatchCommonChorus);

    *clone = *this;

    return clone;
}//deepClone

template<class Archive>
void SonicCellData::PatchCommonChorus::serialize(Archive &ar, const unsigned int version)
{
    ar & BOOST_SERIALIZATION_NVP(ChorusType);
    ar & BOOST_SERIALIZATION_NVP(ChorusLevel);
    ar & BOOST_SERIALIZATION_NVP(ChorusOutputAssign);
    ar & BOOST_SERIALIZATION_NVP(ChorusOutputSelect);

    for (unsigned int index = 0; index < ChorusParameters.size(); ++index) {
        unsigned int ChorusParametersVal = ChorusParameters[index];
        ar & BOOST_SERIALIZATION_NVP(ChorusParametersVal);
        ChorusParameters[index] = ChorusParametersVal;
    }//for
}//serialize


template void SonicCellData::PatchCommonChorus::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive &ar, const unsigned int version);

template void SonicCellData::PatchCommonChorus::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive &ar, const unsigned int version);
