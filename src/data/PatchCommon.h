/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __PATCHCOMMON_H
#define __PATCHCOMMON_H

#include <boost/array.hpp>
#include <vector>
#include <string>
#include <jack/midiport.h>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/access.hpp>

namespace SonicCellData
{

class PatchCommon
{
    boost::array<unsigned char, 13> PatchName;
    unsigned char PatchCategory;
    unsigned char PatchLevel;
    unsigned char PatchPan;
    unsigned char PatchPriority;
    unsigned char PatchCoarseTune;
    unsigned char PatchFineTune;
    unsigned char OctaveShift;
    unsigned char StretchTuneDepth;
    unsigned char AnalogFeel;
    unsigned char MonoPoly;
    unsigned char LegatoSwitch;
    unsigned char LegatoRetrigger;
    unsigned char PortamentoSwitch;
    unsigned char PortamentoMode;
    unsigned char PortamentoType;
    unsigned char PortamentoStart;
    unsigned char PortamentoTime;
    unsigned char CutoffOffset;
    unsigned char ResonanceOffset;
    unsigned char AttackOffset;
    unsigned char ReleaseOffset;
    unsigned char VelocitySensOffset;
    unsigned char PatchOutputAssign;
    unsigned char TMTControlSwitch;
    unsigned char PitchBendRangeUp;
    unsigned char PitchBendRangeDown;
    boost::array<unsigned char, 4> MatrixControlSource;
    boost::array<boost::array<unsigned char, 4>, 4 > MatrixControlDestination;
    boost::array<boost::array<unsigned char, 4>, 4 > MatrixControlSens;
    unsigned char PartModulationSwitch;

public:
    static const unsigned int fullDataSize = 0x50;

    PatchCommon();
    virtual ~PatchCommon();

    bool handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize);

    std::vector<jack_midi_data_t> createUpdateRequest(unsigned char myIndex) const;
    std::vector<jack_midi_data_t> createWriteRequest(unsigned char myIndex) const;

    //Gets
    std::string getPatchName() const;
    unsigned char getPatchCategory() const;
    unsigned char getPatchLevel() const;
    unsigned char getPatchPan() const;
    unsigned char getPatchPriority() const;
    unsigned char getPatchCoarseTune() const;
    unsigned char getPatchFineTune() const;
    unsigned char getOctaveShift() const;
    unsigned char getStretchTuneDepth() const;
    unsigned char getAnalogFeel() const;
    unsigned char getMonoPoly() const;
    unsigned char getLegatoSwitch() const;
    unsigned char getLegatoRetrigger() const;
    unsigned char getPortamentoSwitch() const;
    unsigned char getPortamentoMode() const;
    unsigned char getPortamentoType() const;
    unsigned char getPortamentoStart() const;
    unsigned char getPortamentoTime() const;
    unsigned char getCutoffOffset() const;
    unsigned char getResonanceOffset() const;
    unsigned char getAttackOffset() const;
    unsigned char getReleaseOffset() const;
    unsigned char getVelocitySensOffset() const;
    unsigned char getPatchOutputAssign() const;
    unsigned char getTMTControlSwitch() const;
    unsigned char getPitchBendRangeUp() const;
    unsigned char getPitchBendRangeDown() const;
    unsigned char getMatrixControlSource(unsigned int index) const;
    unsigned char getMatrixControlDestination(unsigned int control, unsigned int index) const;
    unsigned char getMatrixControlSens(unsigned int control, unsigned int index) const;
    unsigned char getPartModulationSwitch() const;

    //Sets
    void setPatchName(const std::string &name);
    void setPatchCategory(unsigned char value);
    void setPatchLevel(unsigned char value);
    void setPatchPan(unsigned char value);
    void setPatchPriority(unsigned char value);
    void setPatchCoarseTune(unsigned char value);
    void setPatchFineTune(unsigned char value);
    void setOctaveShift(unsigned char value);
    void setStretchTuneDepth(unsigned char value);
    void setAnalogFeel(unsigned char value);
    void setMonoPoly(unsigned char value);
    void setLegatoSwitch(unsigned char value);
    void setLegatoRetrigger(unsigned char value);
    void setPortamentoSwitch(unsigned char value);
    void setPortamentoMode(unsigned char value);
    void setPortamentoType(unsigned char value);
    void setPortamentoStart(unsigned char value);
    void setPortamentoTime(unsigned char value);
    void setCutoffOffset(unsigned char value);
    void setResonanceOffset(unsigned char value);
    void setAttackOffset(unsigned char value);
    void setReleaseOffset(unsigned char value);
    void setVelocitySensOffset(unsigned char value);
    void setPatchOutputAssign(unsigned char value);
    void setTMTControlSwitch(unsigned char value);
    void setPitchBendRangeUp(unsigned char value);
    void setPitchBendRangeDown(unsigned char value);
    void setMatrixControlSource(unsigned int index, unsigned char value);
    void setMatrixControlDestination(unsigned int control, unsigned int index, unsigned char value);
    void setMatrixControlSens(unsigned int control, unsigned int index, unsigned char value);
    void setPartModulationSwitch(unsigned char value);

    boost::shared_ptr<PatchCommon> deepClone() const;

    template<class Archive> void serialize(Archive &ar, const unsigned int version);
	friend class boost::serialization::access;
};//PatchCommon

}//namespace SonicCellData

#endif


