/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "PatchCommonReverb.h"
#include "../main.h"
#include <iostream>
#include <string.h>

SonicCellData::PatchCommonReverb::PatchCommonReverb()
{
    ReverbType = 0;
    ReverbLevel = 0;
    ReverbOutputAssign = 0;
    memset(&ReverbParameters[0], 0, sizeof(ReverbParameters));
}//constructor

SonicCellData::PatchCommonReverb::~PatchCommonReverb()
{
    //Nothing
}//destructor

bool SonicCellData::PatchCommonReverb::handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize)
{
    if (dataSize != SonicCellData::PatchCommonReverb::fullDataSize) {
        std::cout << "Initial Reverb dataSize: " << dataSize << std::endl;
    }//if

    switch (offset) {
        case 0x00:
            if (0 == dataSize) break;
            ReverbType = *data;
            data++; dataSize--;
        case 0x01:
            if (0 == dataSize) break;
            ReverbLevel = *data;
            data++; dataSize--;
        case 0x02:
            if (0 == dataSize) break;
            ReverbOutputAssign = *data;
            data++; dataSize--;
        case 0x03:
            if (4 > dataSize) break;
            ReverbParameters[0] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x07:
            if (4 > dataSize) break;
            ReverbParameters[1] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x0b:
            if (4 > dataSize) break;
            ReverbParameters[2] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x0f:
            if (4 > dataSize) break;
            ReverbParameters[3] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x13:
            if (4 > dataSize) break;
            ReverbParameters[4] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x17:
            if (4 > dataSize) break;
            ReverbParameters[5] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x1b:
            if (4 > dataSize) break;
            ReverbParameters[6] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x1f:
            if (4 > dataSize) break;
            ReverbParameters[7] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x23:
            if (4 > dataSize) break;
            ReverbParameters[8] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x27:
            if (4 > dataSize) break;
            ReverbParameters[9] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x2b:
            if (4 > dataSize) break;
            ReverbParameters[10] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x2f:
            if (4 > dataSize) break;
            ReverbParameters[11] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x33:
            if (4 > dataSize) break;
            ReverbParameters[12] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x37:
            if (4 > dataSize) break;
            ReverbParameters[13] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x3b:
            if (4 > dataSize) break;
            ReverbParameters[14] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x3f:
            if (4 > dataSize) break;
            ReverbParameters[15] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x43:
            if (4 > dataSize) break;
            ReverbParameters[16] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x47:
            if (4 > dataSize) break;
            ReverbParameters[17] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x4b:
            if (4 > dataSize) break;
            ReverbParameters[18] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x4f:
            if (4 > dataSize) break;
            ReverbParameters[19] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
            break;
        default:
            std::cerr << "Invalid patch common reverb offset: " << offset << std::endl;
            return false;
    }//switch

    if (0 != dataSize) {
        std::cout << "reverb dataSize: " << dataSize << std::endl;
        return false;
    }//if

    assert(0 == dataSize);
    return true;
}//handleSysExMessage

std::vector<jack_midi_data_t> SonicCellData::PatchCommonReverb::createUpdateRequest(unsigned char myIndex) const
{
    std::vector<jack_midi_data_t> requestBuffer;

    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x11};
    std::copy(dataHeader, dataHeader+7, std::back_inserter(requestBuffer));
    
    unsigned char addressHighHigh = 0x30;
    unsigned char addressHighLow = myIndex;
    if (myIndex > 127) {
        addressHighHigh++;
        addressHighLow -= 128;
    }//if

    jack_midi_data_t addressHigh[2] = {addressHighHigh, addressHighLow};
    std::copy(addressHigh, addressHigh+2, std::back_inserter(requestBuffer));

    jack_midi_data_t commonOffset[2] = {0x06, 0x00};
    std::copy(commonOffset, commonOffset+2, std::back_inserter(requestBuffer));

    unsigned int size = SonicCellData::PatchCommonReverb::fullDataSize;
    requestBuffer.push_back((size & 0xff000000) >> 24);
    requestBuffer.push_back((size & 0x00ff0000) >> 16);
    requestBuffer.push_back((size & 0x0000ff00) >> 8);
    requestBuffer.push_back(size & 0x000000ff);

    requestBuffer.push_back(0xf7);

    JackSingleton::addChecksum(requestBuffer);

    return requestBuffer;
}//createUpdateRequest

std::vector<jack_midi_data_t> SonicCellData::PatchCommonReverb::createWriteRequest(unsigned char myIndex) const
{
    std::vector<jack_midi_data_t> requestBuffer;

    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x12};
    std::copy(dataHeader, dataHeader+7, std::back_inserter(requestBuffer));
    
    unsigned char addressHighHigh = 0x30;
    unsigned char addressHighLow = myIndex;
    if (myIndex > 127) {
        addressHighHigh++;
        addressHighLow -= 128;
    }//if

    jack_midi_data_t addressHigh[2] = {addressHighHigh, addressHighLow};
    std::copy(addressHigh, addressHigh+2, std::back_inserter(requestBuffer));

    jack_midi_data_t commonOffset[2] = {0x06, 0x00};
    std::copy(commonOffset, commonOffset+2, std::back_inserter(requestBuffer));

    //Add data
    requestBuffer.push_back(ReverbType);
    requestBuffer.push_back(ReverbLevel);
    requestBuffer.push_back(ReverbOutputAssign);

    for (unsigned int pos = 0; pos < ReverbParameters.size(); ++pos) {
        requestBuffer.push_back((ReverbParameters[pos] & 0xff000001) >> 24);
        requestBuffer.push_back((ReverbParameters[pos] & 0x00ff0000) >> 16);
        requestBuffer.push_back((ReverbParameters[pos] & 0x0000ff00) >> 8);
        requestBuffer.push_back(ReverbParameters[pos] & 0x000000ff);
    }//for

    //Footer
    requestBuffer.push_back(0xf7);

    assert(requestBuffer.size() == SonicCellData::PatchCommonReverb::fullDataSize + 7 + 4 + 1);

    JackSingleton::addChecksum(requestBuffer);

    return requestBuffer;
}//createWriteRequest

unsigned char SonicCellData::PatchCommonReverb::getReverbType() const
{
    return ReverbType;
}//getReverbType

unsigned char SonicCellData::PatchCommonReverb::getReverbLevel() const
{
    return ReverbLevel;
}//getReverbLevel

unsigned char SonicCellData::PatchCommonReverb::getReverbOutputAssign() const
{
    return ReverbOutputAssign;
}//getReverbOutputAssign

unsigned int SonicCellData::PatchCommonReverb::getReverbParameter(unsigned int index) const
{
    if (index < ReverbParameters.size()) {
        return ReverbParameters[index];
    } else {
        return 0;
    }//if
}//getReverbParameter

void SonicCellData::PatchCommonReverb::setReverbType(unsigned char value)
{
    ReverbType = value;
}//getReverbType

void SonicCellData::PatchCommonReverb::setReverbLevel(unsigned char value)
{
    ReverbLevel = value;
}//getReverbLevel

void SonicCellData::PatchCommonReverb::setReverbOutputAssign(unsigned char value)
{
    ReverbOutputAssign = value;
}//getReverbOutputAssign

void SonicCellData::PatchCommonReverb::setReverbParameters(unsigned int index, unsigned int value)
{
    if (index < ReverbParameters.size()) {
        ReverbParameters[index] = value;
    }//if
}//getReverbParameters

boost::shared_ptr<SonicCellData::PatchCommonReverb> SonicCellData::PatchCommonReverb::deepClone() const
{
    boost::shared_ptr<SonicCellData::PatchCommonReverb> clone(new SonicCellData::PatchCommonReverb);

    *clone = *this;

    return clone;
}//deepClone

template<class Archive>
void SonicCellData::PatchCommonReverb::serialize(Archive &ar, const unsigned int version)
{
    ar & BOOST_SERIALIZATION_NVP(ReverbType);
    ar & BOOST_SERIALIZATION_NVP(ReverbLevel);
    ar & BOOST_SERIALIZATION_NVP(ReverbOutputAssign);

    for (unsigned int index = 0; index < ReverbParameters.size(); ++index) {
        unsigned int ReverbParametersVal = ReverbParameters[index];
        ar & BOOST_SERIALIZATION_NVP(ReverbParametersVal);
        ReverbParameters[index] = ReverbParametersVal;
    }//for
}//serialize


template void SonicCellData::PatchCommonReverb::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive &ar, const unsigned int version);

template void SonicCellData::PatchCommonReverb::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive &ar, const unsigned int version);
