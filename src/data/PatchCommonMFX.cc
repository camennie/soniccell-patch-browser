/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "PatchCommonMFX.h"
#include "main.h"
#include <iostream>
#include <string.h>

SonicCellData::PatchCommonMFX::PatchCommonMFX()
{
    MFXType = 0;
    MFXDrySendLevel = 0;
    MFXChorusSendLevel = 0;
    MFXReverbSendLevel = 0;
    MFXOutputAssign = 0;

    memset(&MFXControlSource[0], 0, sizeof(MFXControlSource));
    memset(&MFXControlSens[0], 0, sizeof(MFXControlSens));
    memset(&MFXControlAssign[0], 0, sizeof(MFXControlAssign));
    memset(&MFXParameter[0], 0, sizeof(MFXParameter));
}//constructor

SonicCellData::PatchCommonMFX::~PatchCommonMFX()
{
    //Nothing
}//destructor

bool SonicCellData::PatchCommonMFX::handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize)
{
    if (dataSize != SonicCellData::PatchCommonMFX::fullDataSize) {
        std::cout << "Initial MFX dataSize: " << dataSize << std::endl;
    }//if

    switch (offset) {
        case 0x00:
            if (0 == dataSize) break;
            MFXType = *data;
            data++; dataSize--;
        case 0x01:
            if (0 == dataSize) break;
            MFXDrySendLevel = *data;
            data++; dataSize--;
        case 0x02:
            if (0 == dataSize) break;
            MFXChorusSendLevel = *data;
            data++; dataSize--;
        case 0x03:
            if (0 == dataSize) break;
            MFXReverbSendLevel = *data;
            data++; dataSize--;
        case 0x04:
            if (0 == dataSize) break;
            MFXOutputAssign = *data;
            data++; dataSize--;
        case 0x05:
            if (0 == dataSize) break;
            MFXControlSource[0] = *data;
            data++; dataSize--;
        case 0x06:
            if (0 == dataSize) break;
            MFXControlSens[0] = *data;
            data++; dataSize--;
        case 0x07:
            if (0 == dataSize) break;
            MFXControlSource[1] = *data;
            data++; dataSize--;
        case 0x08:
            if (0 == dataSize) break;
            MFXControlSens[1] = *data;
            data++; dataSize--;
        case 0x09:
            if (0 == dataSize) break;
            MFXControlSource[2] = *data;
            data++; dataSize--;
        case 0x0a:
            if (0 == dataSize) break;
            MFXControlSens[2] = *data;
            data++; dataSize--;
        case 0x0b:
            if (0 == dataSize) break;
            MFXControlSource[3] = *data;
            data++; dataSize--;
        case 0x0c:
            if (0 == dataSize) break;
            MFXControlSens[3] = *data;
            data++; dataSize--;
        case 0x0d:
            if (0 == dataSize) break;
            MFXControlAssign[0] = *data;
            data++; dataSize--;
        case 0x0e:
            if (0 == dataSize) break;
            MFXControlAssign[1] = *data;
            data++; dataSize--;
        case 0x0f:
            if (0 == dataSize) break;
            MFXControlAssign[2] = *data;
            data++; dataSize--;
        case 0x10:
            if (0 == dataSize) break;
            MFXControlAssign[3] = *data;
            data++; dataSize--;
        case 0x11:
            if (4 > dataSize) break;
            MFXParameter[0] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x15:
            if (4 > dataSize) break;
            MFXParameter[1] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x19:
            if (4 > dataSize) break;
            MFXParameter[2] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x1d:
            if (4 > dataSize) break;
            MFXParameter[3] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x21:
            if (4 > dataSize) break;
            MFXParameter[4] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x25:
            if (4 > dataSize) break;
            MFXParameter[5] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x29:
            if (4 > dataSize) break;
            MFXParameter[6] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x2d:
            if (4 > dataSize) break;
            MFXParameter[7] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x31:
            if (4 > dataSize) break;
            MFXParameter[8] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x35:
            if (4 > dataSize) break;
            MFXParameter[9] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x39:
            if (4 > dataSize) break;
            MFXParameter[10] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x3d:
            if (4 > dataSize) break;
            MFXParameter[11] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x41:
            if (4 > dataSize) break;
            MFXParameter[12] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x45:
            if (4 > dataSize) break;
            MFXParameter[13] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x49:
            if (4 > dataSize) break;
            MFXParameter[14] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x4d:
            if (4 > dataSize) break;
            MFXParameter[15] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x51:
            if (4 > dataSize) break;
            MFXParameter[16] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x55:
            if (4 > dataSize) break;
            MFXParameter[17] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x59:
            if (4 > dataSize) break;
            MFXParameter[18] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x5d:
            if (4 > dataSize) break;
            MFXParameter[19] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x61:
            if (4 > dataSize) break;
            MFXParameter[20] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x65:
            if (4 > dataSize) break;
            MFXParameter[21] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x69:
            if (4 > dataSize) break;
            MFXParameter[22] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x6d:
            if (4 > dataSize) break;
            MFXParameter[23] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x71:
            if (4 > dataSize) break;
            MFXParameter[24] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x75:
            if (4 > dataSize) break;
            MFXParameter[25] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x79:
            if (4 > dataSize) break;
            MFXParameter[26] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x7d:
            if (4 > dataSize) break;
            MFXParameter[27] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x81:
            if (4 > dataSize) break;
            MFXParameter[28] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x85:
            if (4 > dataSize) break;
            MFXParameter[29] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x89:
            if (4 > dataSize) break;
            MFXParameter[30] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
        case 0x8d:
            if (4 > dataSize) break;
            MFXParameter[31] = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
            data += 4; dataSize -= 4;
            break;
        default:
            std::cerr << "Invalid patch common MFX offset: " << offset << std::endl;
            return false;
    }//switch

    if (0 != dataSize) {
        std::cout << "dataSize: " << dataSize << std::endl;
        return false;
    }//if

    assert(0 == dataSize);
    return true;
}//handleSysExMessage

std::vector<jack_midi_data_t> SonicCellData::PatchCommonMFX::createUpdateRequest(unsigned char myIndex) const
{
    std::vector<jack_midi_data_t> requestBuffer;

    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x11};
    std::copy(dataHeader, dataHeader+7, std::back_inserter(requestBuffer));
    
    unsigned char addressHighHigh = 0x30;
    unsigned char addressHighLow = myIndex;
    if (myIndex > 127) {
        addressHighHigh++;
        addressHighLow -= 128;
    }//if

    jack_midi_data_t addressHigh[2] = {addressHighHigh, addressHighLow};
    std::copy(addressHigh, addressHigh+2, std::back_inserter(requestBuffer));

    jack_midi_data_t commonOffset[2] = {0x02, 0x00};
    std::copy(commonOffset, commonOffset+2, std::back_inserter(requestBuffer));

    unsigned int size = 0x00000111; //SonicCellData::PatchCommonMFX::fullDataSize;
    requestBuffer.push_back((size & 0xff000000) >> 24);
    requestBuffer.push_back((size & 0x00ff0000) >> 16);
    requestBuffer.push_back((size & 0x0000ff00) >> 8);
    requestBuffer.push_back(size & 0x000000ff);

    requestBuffer.push_back(0xf7);

    JackSingleton::addChecksum(requestBuffer);

    return requestBuffer;
}//createUpdateRequest

std::vector<jack_midi_data_t> SonicCellData::PatchCommonMFX::createWriteRequest(unsigned char myIndex) const
{
    std::vector<jack_midi_data_t> requestBuffer;

    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x12};
    std::copy(dataHeader, dataHeader+7, std::back_inserter(requestBuffer));
    
    unsigned char addressHighHigh = 0x30;
    unsigned char addressHighLow = myIndex;
    if (myIndex > 127) {
        addressHighHigh++;
        addressHighLow -= 128;
    }//if

    jack_midi_data_t addressHigh[2] = {addressHighHigh, addressHighLow};
    std::copy(addressHigh, addressHigh+2, std::back_inserter(requestBuffer));

    jack_midi_data_t commonOffset[2] = {0x02, 0x00};
    std::copy(commonOffset, commonOffset+2, std::back_inserter(requestBuffer));

    //Add data
    requestBuffer.push_back(MFXType);
    requestBuffer.push_back(MFXDrySendLevel);
    requestBuffer.push_back(MFXChorusSendLevel);
    requestBuffer.push_back(MFXReverbSendLevel);
    requestBuffer.push_back(MFXOutputAssign);

    assert(MFXControlSource.size() == MFXControlSens.size());
    for (unsigned int pos = 0; pos < MFXControlSource.size(); ++pos) {
        requestBuffer.push_back(MFXControlSource[pos]);
        requestBuffer.push_back(MFXControlSens[pos]);
    }//for

    for (unsigned int pos = 0; pos < MFXControlAssign.size(); ++pos) {
        requestBuffer.push_back(MFXControlAssign[pos]);
    }//for

    for (unsigned int pos = 0; pos < MFXParameter.size(); ++pos) {
        requestBuffer.push_back((MFXParameter[pos] & 0xff000001) >> 24);
        requestBuffer.push_back((MFXParameter[pos] & 0x00ff0000) >> 16);
        requestBuffer.push_back((MFXParameter[pos] & 0x0000ff00) >> 8);
        requestBuffer.push_back(MFXParameter[pos] & 0x000000ff);
    }//for

    //Footer
    requestBuffer.push_back(0xf7);

    assert(requestBuffer.size() == SonicCellData::PatchCommonMFX::fullDataSize + 7 + 4 + 1);

    JackSingleton::addChecksum(requestBuffer);

    return requestBuffer;
}//createWriteRequest

unsigned char SonicCellData::PatchCommonMFX::getMFXType() const
{
    return MFXType;
}//getMFXType

unsigned char SonicCellData::PatchCommonMFX::getMFXDrySendLevel() const
{
    return MFXDrySendLevel;
}//getMFXDrySendLevel

unsigned char SonicCellData::PatchCommonMFX::getMFXChorusSendLevel() const
{
    return MFXChorusSendLevel;
}//getMFXChorusSendLevel

unsigned char SonicCellData::PatchCommonMFX::getMFXReverbSendLevel() const
{
    return MFXReverbSendLevel;
}//getMFXReverbSendLevel

unsigned char SonicCellData::PatchCommonMFX::getMFXOutputAssign() const
{
    return MFXOutputAssign;
}//getMFXOutputAssign

unsigned char SonicCellData::PatchCommonMFX::getMFXControlSource(unsigned int index) const
{
    if (index < MFXControlSource.size()) {
        return MFXControlSource[index];
    } else {
        return 0;
    }//if
}//getMFXControlSource

unsigned char SonicCellData::PatchCommonMFX::getMFXControlSens(unsigned int index) const
{
    if (index < MFXControlSens.size()) {
        return MFXControlSens[index];
    } else {
        return 0;
    }//if
}//getMFXControlSens

unsigned char SonicCellData::PatchCommonMFX::getMFXControlAssign(unsigned int index) const
{
    if (index < MFXControlAssign.size()) {
        return MFXControlAssign[index];
    } else {
        return 0;
    }//if
}//getMFXControlAssign

unsigned int SonicCellData::PatchCommonMFX::getMFXParameter(unsigned int index) const
{
    if (index < MFXParameter.size()) {
        return MFXParameter[index];
    } else {
        return 0;
    }//if
}//getMFXParameter

void SonicCellData::PatchCommonMFX::setMFXType(unsigned char value)
{
    MFXType = value;
}//setMFXType

void SonicCellData::PatchCommonMFX::setMFXDrySendLevel(unsigned char value)
{
    MFXDrySendLevel = value;
}//setMFXDrySendLevel

void SonicCellData::PatchCommonMFX::setMFXChorusSendLevel(unsigned char value)
{
    MFXChorusSendLevel = value;
}//setMFXChorusSendLevel

void SonicCellData::PatchCommonMFX::setMFXReverbSendLevel(unsigned char value)
{
    MFXReverbSendLevel = value;
}//setMFXReverbSendLevel

void SonicCellData::PatchCommonMFX::setMFXOutputAssign(unsigned char value)
{
    MFXOutputAssign = value;
}//setMFXOutputAssign

void SonicCellData::PatchCommonMFX::setMFXControlSource(unsigned int index, unsigned char value)
{
    if (index < MFXControlSource.size()) {
        MFXControlSource[index] = value;
    }//if
}//setMFXControlSource

void SonicCellData::PatchCommonMFX::setMFXControlSens(unsigned int index, unsigned char value)
{
    if (index < MFXControlSens.size()) {
        MFXControlSens[index] = value;
    }//if
}//setMFXControlSens

void SonicCellData::PatchCommonMFX::setMFXControlAssign(unsigned int index, unsigned char value)
{
    if (index < MFXControlAssign.size()) {
        MFXControlAssign[index] = value;
    }//if
}//setMFXControlAssign

void SonicCellData::PatchCommonMFX::setMFXParameter(unsigned int index, unsigned int value)
{
    if (index < MFXParameter.size()) {
        MFXParameter[index] = value;
    }//if
}//setMFXParameter

boost::shared_ptr<SonicCellData::PatchCommonMFX> SonicCellData::PatchCommonMFX::deepClone() const
{
    boost::shared_ptr<SonicCellData::PatchCommonMFX> clone(new SonicCellData::PatchCommonMFX);

    *clone = *this;

    return clone;
}//deepClone

template<class Archive>
void SonicCellData::PatchCommonMFX::serialize(Archive &ar, const unsigned int version)
{
    ar & BOOST_SERIALIZATION_NVP(MFXType);
    ar & BOOST_SERIALIZATION_NVP(MFXDrySendLevel);
    ar & BOOST_SERIALIZATION_NVP(MFXChorusSendLevel);
    ar & BOOST_SERIALIZATION_NVP(MFXReverbSendLevel);
    ar & BOOST_SERIALIZATION_NVP(MFXOutputAssign);

    for (unsigned int index = 0; index < MFXControlSource.size(); ++index) {
        unsigned char MFXControlSourceVal = MFXControlSource[index];
        ar & BOOST_SERIALIZATION_NVP(MFXControlSourceVal);
        MFXControlSource[index] = MFXControlSourceVal;
    }//for

    for (unsigned int index = 0; index < MFXControlSens.size(); ++index) {
        unsigned char MFXControlSensVal = MFXControlSens[index];
        ar & BOOST_SERIALIZATION_NVP(MFXControlSensVal);
        MFXControlSens[index] = MFXControlSensVal;
    }//for

    for (unsigned int index = 0; index < MFXControlAssign.size(); ++index) {
        unsigned char MFXControlAssignVal = MFXControlAssign[index];
        ar & BOOST_SERIALIZATION_NVP(MFXControlAssignVal);
        MFXControlAssign[index] = MFXControlAssignVal;
    }//for

    for (unsigned int index = 0; index < MFXParameter.size(); ++index) {
        unsigned int MFXParameterVal = MFXParameter[index];
        ar & BOOST_SERIALIZATION_NVP(MFXParameterVal);
        MFXParameter[index] = MFXParameterVal;
    }//for
}//serialize


template void SonicCellData::PatchCommonMFX::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive &ar, const unsigned int version);

template void SonicCellData::PatchCommonMFX::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive &ar, const unsigned int version);
