/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __PATCH_COMMON_MFX_H
#define __PATCH_COMMON_MFX_H

#include <boost/array.hpp>
#include <vector>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/access.hpp>
#include <jack/midiport.h>

namespace SonicCellData
{

class PatchCommonMFX
{
    unsigned char MFXType;
    unsigned char MFXDrySendLevel;
    unsigned char MFXChorusSendLevel;
    unsigned char MFXReverbSendLevel;
    unsigned char MFXOutputAssign;

    boost::array<unsigned char, 4> MFXControlSource;
    boost::array<unsigned char, 4> MFXControlSens;
    boost::array<unsigned char, 4> MFXControlAssign;
    boost::array<unsigned int, 32> MFXParameter;

public:
    static const unsigned int fullDataSize = 0x091; //0x1111;

    PatchCommonMFX();
    virtual ~PatchCommonMFX();

    //Data will have address stripped from it
    bool handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize);

    std::vector<jack_midi_data_t> createUpdateRequest(unsigned char myIndex) const;
    std::vector<jack_midi_data_t> createWriteRequest(unsigned char myIndex) const;

    unsigned char getMFXType() const;
    unsigned char getMFXDrySendLevel() const;
    unsigned char getMFXChorusSendLevel() const;
    unsigned char getMFXReverbSendLevel() const;
    unsigned char getMFXOutputAssign() const;

    unsigned char getMFXControlSource(unsigned int index) const;
    unsigned char getMFXControlSens(unsigned int index) const;
    unsigned char getMFXControlAssign(unsigned int index) const;
    unsigned int getMFXParameter(unsigned int index) const;

    void setMFXType(unsigned char value);
    void setMFXDrySendLevel(unsigned char value);
    void setMFXChorusSendLevel(unsigned char value);
    void setMFXReverbSendLevel(unsigned char value);
    void setMFXOutputAssign(unsigned char value);

    void setMFXControlSource(unsigned int index, unsigned char value);
    void setMFXControlSens(unsigned int index, unsigned char value);
    void setMFXControlAssign(unsigned int index, unsigned char value);
    void setMFXParameter(unsigned int index, unsigned int value);

    boost::shared_ptr<PatchCommonMFX> deepClone() const;

    template<class Archive> void serialize(Archive &ar, const unsigned int version);
	friend class boost::serialization::access;
};//PatchCommonMFX

}//namespace SonicCellData

#endif

