/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __SONICCELL_H
#define __SONICCELL_H

#include <boost/shared_ptr.hpp>
#include <boost/array.hpp>
#include <vector>
#include <map>
#include <deque>
#include <set>
#include <jack/midiport.h>
#include <boost/serialization/version.hpp>

class SonicCellLibrarianWidgets;

namespace SonicCellData
{

enum PatchInfoType
{
    ROM,
    GM,
    SRX,
    User
};//PatchInfoType

struct PatchInfo
{
    std::string patchName;
    std::string bankName;

    unsigned char patchBankMSB;
    unsigned char patchBankLSB;
    unsigned char patchNum;
    unsigned int actualPatchNum;
    unsigned char category;

    std::string memo1;
    std::string memo2;
    std::string memo3;
    std::string memo4;

    bool hasSample;

    PatchInfo()
    {
        hasSample = false;
    }//constructor
};//PatchInfo

struct PatchInfoRequest
{
    PatchInfoRequest(PatchInfoType infoType_, std::string bankName_, unsigned char patchBankMSB_, 
                        unsigned char patchBankLSB_, unsigned char numPrograms_, unsigned int programOffset_) 
    {
        infoType = infoType_;
        bankName = bankName_;
        patchBankMSB = patchBankMSB_;
        patchBankLSB = patchBankLSB_;
        numPrograms = numPrograms_;
        programOffset = programOffset_;
    }//constructor

    PatchInfoType infoType;
    std::string bankName;
    unsigned char patchBankMSB;
    unsigned char patchBankLSB;
    unsigned char numPrograms;
    unsigned int programOffset;
};//PatchInfoRequest

class SonicCell
{
protected:
    std::deque<unsigned int> pendingSRXChecks;
    std::set<unsigned int> installedSRXs;

    mutable std::deque<std::pair<PatchInfoType, PatchInfo> > pendingPatchInfos;

    std::multimap<PatchInfoType, boost::shared_ptr<PatchInfo> > PatchInfos;

public:
    SonicCell();
    virtual ~SonicCell();

    //Data will have the SYSEX header, checksum, and footer stripped
    bool handleSysExMessage(jack_midi_data_t *data, unsigned int length);

    //Updates everything contained in the SonicCell
    std::vector<std::vector<jack_midi_data_t> > createUpdateRequest();

    std::vector<boost::shared_ptr<PatchInfo> > getAllPatchInfos() const;

    void checkForSRXs();

    void loadFromFile(const std::string &filename);
    void saveToFile(const std::string &filename);

    friend class ::SonicCellLibrarianWidgets;
};//SonicCell

}//namespace SonicCellData

BOOST_CLASS_VERSION(SonicCellData::SonicCell, 1);

#endif

