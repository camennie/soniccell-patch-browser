/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __PATCH_TONE_MIX_TABLE_H_
#define __PATCH_TONE_MIX_TABLE_H_

#include <boost/shared_ptr.hpp>
#include <boost/array.hpp>
#include <vector>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/access.hpp>
#include <jack/midiport.h>

namespace SonicCellData
{

class PatchToneMixTable
{
    unsigned char StructureType12;
    unsigned char Booster12;
    unsigned char StructureType34;
    unsigned char Booster34;
    unsigned char TMTVelocityControl;
    boost::array<unsigned char, 4> TMTToneSwitch;
    boost::array<unsigned char, 4> TMTKeyboardRangeLower;
    boost::array<unsigned char, 4> TMTKeyboardRangeUpper;
    boost::array<unsigned char, 4> TMTKeyboardFadeWidthLower;
    boost::array<unsigned char, 4> TMTKeyboardFadeWidthUpper;
    boost::array<unsigned char, 4> TMTVelocityRangeLower;
    boost::array<unsigned char, 4> TMTVelocityRangeUpper;
    boost::array<unsigned char, 4> TMTVelocityFadeWidthLower;
    boost::array<unsigned char, 4> TMTVelocityFadeWidthUpper;

public:
    static const unsigned int fullDataSize = 0x29;

    PatchToneMixTable();
    virtual ~PatchToneMixTable();

    //Data will have address stripped from it
    bool handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize);

    std::vector<jack_midi_data_t> createUpdateRequest(unsigned char myIndex) const;
    std::vector<jack_midi_data_t> createWriteRequest(unsigned char myIndex) const;

    unsigned char getStructureType12() const;
    unsigned char getBooster12() const;
    unsigned char getStructureType34() const;
    unsigned char getBooster34() const;
    unsigned char getTMTVelocityControl() const;
    unsigned char getTMTToneSwitch(unsigned int index) const;
    unsigned char getTMTKeyboardRangeLower(unsigned int index) const;
    unsigned char getTMTKeyboardRangeUpper(unsigned int index) const;
    unsigned char getTMTKeyboardFadeWidthLower(unsigned int index) const;
    unsigned char getTMTKeyboardFadeWidthUpper(unsigned int index) const;
    unsigned char getTMTVelocityRangeLower(unsigned int index) const;
    unsigned char getTMTVelocityRangeUpper(unsigned int index) const;
    unsigned char getTMTVelocityFadeWidthLower(unsigned int index) const;
    unsigned char getTMTVelocityFadeWidthUpper(unsigned int index) const;

    void setStructureType12(unsigned char value);
    void setBooster12(unsigned char value);
    void setStructureType34(unsigned char value);
    void setBooster34(unsigned char value);
    void setTMTVelocityControl(unsigned char value);
    void setTMTToneSwitch(unsigned int index, unsigned char value);
    void setTMTKeyboardRangeLower(unsigned int index, unsigned char value);
    void setTMTKeyboardRangeUpper(unsigned int index, unsigned char value);
    void setTMTKeyboardFadeWidthLower(unsigned int index, unsigned char value);
    void setTMTKeyboardFadeWidthUpper(unsigned int index, unsigned char value);
    void setTMTVelocityRangeLower(unsigned int index, unsigned char value);
    void setTMTVelocityRangeUpper(unsigned int index, unsigned char value);
    void setTMTVelocityFadeWidthLower(unsigned int index, unsigned char value);
    void setTMTVelocityFadeWidthUpper(unsigned int index, unsigned char value);

    boost::shared_ptr<PatchToneMixTable> deepClone() const;

    template<class Archive> void serialize(Archive &ar, const unsigned int version);
	friend class boost::serialization::access;
};//PatchToneMixTable

}//namespace SonicCellData

#endif
