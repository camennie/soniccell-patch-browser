/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __PATCH_H_
#define __PATCH_H_

#include <boost/shared_ptr.hpp>
#include <boost/array.hpp>
#include <vector>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/access.hpp>
#include <jack/midiport.h>
#include <boost/serialization/version.hpp>

namespace SonicCellData
{

class PatchCommon;
class PatchCommonMFX;
class PatchCommonChorus;
class PatchCommonReverb;
class PatchToneMixTable;
class PatchTone;

class Patch
{
    boost::shared_ptr<PatchCommon> patchCommon;
    boost::shared_ptr<PatchCommonMFX> patchCommonMFX;
    boost::shared_ptr<PatchCommonChorus> patchCommonChorus;
    boost::shared_ptr<PatchCommonReverb> patchCommonReverb;
    boost::shared_ptr<PatchToneMixTable> patchToneMixTable;
    boost::array<boost::shared_ptr<PatchTone>, 4> patchTones;

    boost::array<std::string, 4> memos;

public:
    Patch();
    virtual ~Patch();

    //Data will have address stripped from it
    bool handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int length);

    std::vector<std::vector<jack_midi_data_t> > createUpdateRequest(unsigned char myIndex) const;
    std::vector<std::vector<jack_midi_data_t> > createWriteRequest(unsigned char myIndex) const;

    boost::shared_ptr<PatchCommon> getPatchCommon() const;
    boost::shared_ptr<PatchCommonMFX> getPatchCommonMFX() const;
    boost::shared_ptr<PatchCommonChorus> getPatchCommonChorus() const;
    boost::shared_ptr<PatchCommonReverb> getPatchCommonReverb() const;
    boost::shared_ptr<PatchToneMixTable> getPatchToneMixTable() const;
    boost::shared_ptr<PatchTone> getPatchTone(unsigned int index) const;

    void setMemo(unsigned int index, const std::string &memo);
    std::string getMemo(unsigned int index) const;

    boost::shared_ptr<Patch> deepClone() const;

    template<class Archive> void serialize(Archive &ar, const unsigned int version);
	friend class boost::serialization::access;
};//Patch

}//namespace SonicCellData

BOOST_CLASS_VERSION(SonicCellData::Patch, 1);

#endif

