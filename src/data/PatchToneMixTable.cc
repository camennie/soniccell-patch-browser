/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "PatchToneMixTable.h"
#include "main.h"
#include <iostream>
#include <string.h>

SonicCellData::PatchToneMixTable::PatchToneMixTable()
{
    StructureType12 = 0;
    Booster12 = 0;
    StructureType34 = 0;
    Booster34 = 0;
    TMTVelocityControl = 0;

    memset(&TMTToneSwitch[0], 0, sizeof(TMTToneSwitch));
    memset(&TMTKeyboardRangeLower[0], 0, sizeof(TMTKeyboardRangeLower));
    memset(&TMTKeyboardRangeUpper[0], 0, sizeof(TMTKeyboardRangeUpper));
    memset(&TMTKeyboardFadeWidthLower[0], 0, sizeof(TMTKeyboardFadeWidthLower));
    memset(&TMTKeyboardFadeWidthUpper[0], 0, sizeof(TMTKeyboardFadeWidthUpper));
    memset(&TMTVelocityRangeLower[0], 0, sizeof(TMTVelocityRangeLower));
    memset(&TMTVelocityRangeUpper[0], 0, sizeof(TMTVelocityRangeUpper));
    memset(&TMTVelocityFadeWidthLower[0], 0, sizeof(TMTVelocityFadeWidthLower));
    memset(&TMTVelocityFadeWidthUpper[0], 0, sizeof(TMTVelocityFadeWidthUpper));
}//constructor

SonicCellData::PatchToneMixTable::~PatchToneMixTable()
{
    //Nothing
}//destructor

bool SonicCellData::PatchToneMixTable::handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize)
{
    if (dataSize != SonicCellData::PatchToneMixTable::fullDataSize) {
        std::cout << "Initial tone mix table dataSize: " << dataSize << std::endl;
    }//if

    switch (offset) {
        case 0x00:
            if (0 == dataSize) break;
            StructureType12 = *data;
            data++; dataSize--;
        case 0x01:
            if (0 == dataSize) break;
            Booster12 = *data;
            data++; dataSize--;
        case 0x02:
            if (0 == dataSize) break;
            StructureType34 = *data;
            data++; dataSize--;
        case 0x03:
            if (0 == dataSize) break;
            Booster34 = *data;
            data++; dataSize--;
        case 0x04:
            if (0 == dataSize) break;
            TMTVelocityControl = *data;
            data++; dataSize--;
        case 0x05:
            if (0 == dataSize) break;
            TMTToneSwitch[0] = *data;
            data++; dataSize--;
        case 0x06:
            if (0 == dataSize) break;
            TMTKeyboardRangeLower[0] = *data;
            data++; dataSize--;
        case 0x07:
            if (0 == dataSize) break;
            TMTKeyboardRangeUpper[0] = *data;
            data++; dataSize--;
        case 0x08:
            if (0 == dataSize) break;
            TMTKeyboardFadeWidthLower[0] = *data;
            data++; dataSize--;
        case 0x09:
            if (0 == dataSize) break;
            TMTKeyboardFadeWidthUpper[0] = *data;
            data++; dataSize--;
        case 0x0a:
            if (0 == dataSize) break;
            TMTVelocityRangeLower[0] = *data;
            data++; dataSize--;
        case 0x0b:
            if (0 == dataSize) break;
            TMTVelocityRangeUpper[0] = *data;
            data++; dataSize--;
        case 0x0c:
            if (0 == dataSize) break;
            TMTVelocityFadeWidthLower[0] = *data;
            data++; dataSize--;
        case 0x0d:
            if (0 == dataSize) break;
            TMTVelocityFadeWidthUpper[0] = *data;
            data++; dataSize--;
        case 0x0e:
            if (0 == dataSize) break;
            TMTToneSwitch[1] = *data;
            data++; dataSize--;
        case 0x0f:
            if (0 == dataSize) break;
            TMTKeyboardRangeLower[1] = *data;
            data++; dataSize--;
        case 0x10:
            if (0 == dataSize) break;
            TMTKeyboardRangeUpper[1] = *data;
            data++; dataSize--;
        case 0x11:
            if (0 == dataSize) break;
            TMTKeyboardFadeWidthLower[1] = *data;
            data++; dataSize--;
        case 0x12:
            if (0 == dataSize) break;
            TMTKeyboardFadeWidthUpper[1] = *data;
            data++; dataSize--;
        case 0x13:
            if (0 == dataSize) break;
            TMTVelocityRangeLower[1] = *data;
            data++; dataSize--;
        case 0x14:
            if (0 == dataSize) break;
            TMTVelocityRangeUpper[1] = *data;
            data++; dataSize--;
        case 0x15:
            if (0 == dataSize) break;
            TMTVelocityFadeWidthLower[1] = *data;
            data++; dataSize--;
        case 0x16:
            if (0 == dataSize) break;
            TMTVelocityFadeWidthUpper[1] = *data;
            data++; dataSize--;
        case 0x17:
            if (0 == dataSize) break;
            TMTToneSwitch[2] = *data;
            data++; dataSize--;
        case 0x18:
            if (0 == dataSize) break;
            TMTKeyboardRangeLower[2] = *data;
            data++; dataSize--;
        case 0x19:
            if (0 == dataSize) break;
            TMTKeyboardRangeUpper[2] = *data;
            data++; dataSize--;
        case 0x1a:
            if (0 == dataSize) break;
            TMTKeyboardFadeWidthLower[2] = *data;
            data++; dataSize--;
        case 0x1b:
            if (0 == dataSize) break;
            TMTKeyboardFadeWidthUpper[2] = *data;
            data++; dataSize--;
        case 0x1c:
            if (0 == dataSize) break;
            TMTVelocityRangeLower[2] = *data;
            data++; dataSize--;
        case 0x1d:
            if (0 == dataSize) break;
            TMTVelocityRangeUpper[2] = *data;
            data++; dataSize--;
        case 0x1e:
            if (0 == dataSize) break;
            TMTVelocityFadeWidthLower[2] = *data;
            data++; dataSize--;
        case 0x1f:
            if (0 == dataSize) break;
            TMTVelocityFadeWidthUpper[2] = *data;
            data++; dataSize--;
        case 0x20:
            if (0 == dataSize) break;
            TMTToneSwitch[3] = *data;
            data++; dataSize--;
        case 0x21:
            if (0 == dataSize) break;
            TMTKeyboardRangeLower[3] = *data;
            data++; dataSize--;
        case 0x22:
            if (0 == dataSize) break;
            TMTKeyboardRangeUpper[3] = *data;
            data++; dataSize--;
        case 0x23:
            if (0 == dataSize) break;
            TMTKeyboardFadeWidthLower[3] = *data;
            data++; dataSize--;
        case 0x24:
            if (0 == dataSize) break;
            TMTKeyboardFadeWidthUpper[3] = *data;
            data++; dataSize--;
        case 0x25:
            if (0 == dataSize) break;
            TMTVelocityRangeLower[3] = *data;
            data++; dataSize--;
        case 0x26:
            if (0 == dataSize) break;
            TMTVelocityRangeUpper[3] = *data;
            data++; dataSize--;
        case 0x27:
            if (0 == dataSize) break;
            TMTVelocityFadeWidthLower[3] = *data;
            data++; dataSize--;
        case 0x28:
            if (0 == dataSize) break;
            TMTVelocityFadeWidthUpper[3] = *data;
            data++; dataSize--;
            break;
        default:
            std::cerr << "Invalid patch tone mix table offset: " << offset << std::endl;
            return false;
    }//switch

    if (0 != dataSize) {
        std::cout << "tone mix table dataSize: " << dataSize << std::endl;
        return false;
    }//if

    assert(0 == dataSize);
    return true;
}//handleDataUpdate

std::vector<jack_midi_data_t> SonicCellData::PatchToneMixTable::createUpdateRequest(unsigned char myIndex) const
{
    std::vector<jack_midi_data_t> requestBuffer;

    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x11};
    std::copy(dataHeader, dataHeader+7, std::back_inserter(requestBuffer));
    
    unsigned char addressHighHigh = 0x30;
    unsigned char addressHighLow = myIndex;
    if (myIndex > 127) {
        addressHighHigh++;
        addressHighLow -= 128;
    }//if

    jack_midi_data_t addressHigh[2] = {addressHighHigh, addressHighLow};
    std::copy(addressHigh, addressHigh+2, std::back_inserter(requestBuffer));

    jack_midi_data_t commonOffset[2] = {0x10, 0x00};
    std::copy(commonOffset, commonOffset+2, std::back_inserter(requestBuffer));

    unsigned int size = SonicCellData::PatchToneMixTable::fullDataSize;
    requestBuffer.push_back((size & 0xff000000) >> 24);
    requestBuffer.push_back((size & 0x00ff0000) >> 16);
    requestBuffer.push_back((size & 0x0000ff00) >> 8);
    requestBuffer.push_back(size & 0x000000ff);

    requestBuffer.push_back(0xf7);

    JackSingleton::addChecksum(requestBuffer);

    return requestBuffer;
}//createUpdateRequest

std::vector<jack_midi_data_t> SonicCellData::PatchToneMixTable::createWriteRequest(unsigned char myIndex) const
{
    std::vector<jack_midi_data_t> requestBuffer;

    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x12};
    std::copy(dataHeader, dataHeader+7, std::back_inserter(requestBuffer));
    
    unsigned char addressHighHigh = 0x30;
    unsigned char addressHighLow = myIndex;
    if (myIndex > 127) {
        addressHighHigh++;
        addressHighLow -= 128;
    }//if

    jack_midi_data_t addressHigh[2] = {addressHighHigh, addressHighLow};
    std::copy(addressHigh, addressHigh+2, std::back_inserter(requestBuffer));

    jack_midi_data_t commonOffset[2] = {0x10, 0x00};
    std::copy(commonOffset, commonOffset+2, std::back_inserter(requestBuffer));

    //Add data
    requestBuffer.push_back(StructureType12);
    requestBuffer.push_back(Booster12);
    requestBuffer.push_back(StructureType34);
    requestBuffer.push_back(Booster34);
    requestBuffer.push_back(TMTVelocityControl);

    for (unsigned int index = 0; index < 4; ++index) {
        requestBuffer.push_back(TMTToneSwitch[index]);
        requestBuffer.push_back(TMTKeyboardRangeLower[index]);
        requestBuffer.push_back(TMTKeyboardRangeUpper[index]);
        requestBuffer.push_back(TMTKeyboardFadeWidthLower[index]);
        requestBuffer.push_back(TMTKeyboardFadeWidthUpper[index]);
        requestBuffer.push_back(TMTVelocityRangeLower[index]);
        requestBuffer.push_back(TMTVelocityRangeUpper[index]);
        requestBuffer.push_back(TMTVelocityFadeWidthLower[index]);
        requestBuffer.push_back(TMTVelocityFadeWidthUpper[index]);
    }//for

    //Footer
    requestBuffer.push_back(0xf7);

    assert(requestBuffer.size() == SonicCellData::PatchToneMixTable::fullDataSize + 7 + 4 + 1);

    JackSingleton::addChecksum(requestBuffer);

    return requestBuffer;
}//createWriteRequest

unsigned char SonicCellData::PatchToneMixTable::getStructureType12() const
{
    return StructureType12;
}//getStructureType12

unsigned char SonicCellData::PatchToneMixTable::getBooster12() const
{
    return Booster12;
}//getBooster12

unsigned char SonicCellData::PatchToneMixTable::getStructureType34() const
{
    return StructureType34;
}//getStructureType34

unsigned char SonicCellData::PatchToneMixTable::getBooster34() const
{
    return Booster34;
}//getBooster34

unsigned char SonicCellData::PatchToneMixTable::getTMTVelocityControl() const
{
    return TMTVelocityControl;
}//getTMTVelocityControl

unsigned char SonicCellData::PatchToneMixTable::getTMTToneSwitch(unsigned int index) const
{
    if (index < TMTToneSwitch.size()) {
        return TMTToneSwitch[index];
    } else {
        return 0;
    }//if
}//getTMTToneSwitch

unsigned char SonicCellData::PatchToneMixTable::getTMTKeyboardRangeLower(unsigned int index) const
{
    if (index < TMTKeyboardRangeLower.size()) {
        return TMTKeyboardRangeLower[index];
    } else {
        return 0;
    }//if
}//getTMTKeyboardRangeLower

unsigned char SonicCellData::PatchToneMixTable::getTMTKeyboardRangeUpper(unsigned int index) const
{
    if (index < TMTKeyboardRangeUpper.size()) {
        return TMTKeyboardRangeUpper[index];
    } else {
        return 0;
    }//if
}//getTMTKeyboardRangeUpper

unsigned char SonicCellData::PatchToneMixTable::getTMTKeyboardFadeWidthLower(unsigned int index) const
{
    if (index < TMTKeyboardFadeWidthLower.size()) {
        return TMTKeyboardFadeWidthLower[index];
    } else {
        return 0;
    }//if
}//getTMTKeyboardFadeWidthLower

unsigned char SonicCellData::PatchToneMixTable::getTMTKeyboardFadeWidthUpper(unsigned int index) const
{
    if (index < TMTKeyboardFadeWidthUpper.size()) {
        return TMTKeyboardFadeWidthUpper[index];
    } else {
        return 0;
    }//if
}//getTMTKeyboardFadeWidthUpper

unsigned char SonicCellData::PatchToneMixTable::getTMTVelocityRangeLower(unsigned int index) const
{
    if (index < TMTVelocityRangeLower.size()) {
        return TMTVelocityRangeLower[index];
    } else {
        return 0;
    }//if
}//getTMTVelocityRangeLower

unsigned char SonicCellData::PatchToneMixTable::getTMTVelocityRangeUpper(unsigned int index) const
{
    if (index < TMTVelocityRangeUpper.size()) {
        return TMTVelocityRangeUpper[index];
    } else {
        return 0;
    }//if
}//getTMTVelocityRangeUpper

unsigned char SonicCellData::PatchToneMixTable::getTMTVelocityFadeWidthLower(unsigned int index) const
{
    if (index < TMTVelocityFadeWidthLower.size()) {
        return TMTVelocityFadeWidthLower[index];
    } else {
        return 0;
    }//if
}//getTMTVelocityFadeWidthLower

unsigned char SonicCellData::PatchToneMixTable::getTMTVelocityFadeWidthUpper(unsigned int index) const
{
    if (index < TMTVelocityFadeWidthUpper.size()) {
        return TMTVelocityFadeWidthUpper[index];
    } else {
        return 0;
    }//if
}//getTMTVelocityFadeWidthUpper

void SonicCellData::PatchToneMixTable::setStructureType12(unsigned char value)
{
    StructureType12 = value;
}//setStructureType12

void SonicCellData::PatchToneMixTable::setBooster12(unsigned char value)
{
    Booster12 = value;
}//setBooster12

void SonicCellData::PatchToneMixTable::setStructureType34(unsigned char value)
{
    StructureType34 = value;
}//setStructureType34

void SonicCellData::PatchToneMixTable::setBooster34(unsigned char value)
{
    Booster34 = value;
}//setBooster34

void SonicCellData::PatchToneMixTable::setTMTVelocityControl(unsigned char value)
{
    TMTVelocityControl = value;
}//setTMTVelocityControl

void SonicCellData::PatchToneMixTable::setTMTToneSwitch(unsigned int index, unsigned char value)
{
    if (index < TMTToneSwitch.size()) {
        TMTToneSwitch[index] = value;
    }//if
}//setTMTToneSwitch

void SonicCellData::PatchToneMixTable::setTMTKeyboardRangeLower(unsigned int index, unsigned char value)
{
    if (index < TMTKeyboardRangeLower.size()) {
        TMTKeyboardRangeLower[index] = value;
    }//if
}//setTMTKeyboardRangeLower

void SonicCellData::PatchToneMixTable::setTMTKeyboardRangeUpper(unsigned int index, unsigned char value)
{
    if (index < TMTKeyboardRangeUpper.size()) {
        TMTKeyboardRangeUpper[index] = value;
    }//if
}//setTMTKeyboardRangeUpper

void SonicCellData::PatchToneMixTable::setTMTKeyboardFadeWidthLower(unsigned int index, unsigned char value)
{
    if (index < TMTKeyboardFadeWidthLower.size()) {
        TMTKeyboardFadeWidthLower[index] = value;
    }//if
}//setTMTKeyboardFadeWidthLower

void SonicCellData::PatchToneMixTable::setTMTKeyboardFadeWidthUpper(unsigned int index, unsigned char value)
{
    if (index < TMTKeyboardFadeWidthUpper.size()) {
        TMTKeyboardFadeWidthUpper[index] = value;
    }//if
}//setTMTKeyboardFadeWidthUpper

void SonicCellData::PatchToneMixTable::setTMTVelocityRangeLower(unsigned int index, unsigned char value)
{
    if (index < TMTVelocityRangeLower.size()) {
        TMTVelocityRangeLower[index] = value;
    }//if
}//setTMTVelocityRangeLower

void SonicCellData::PatchToneMixTable::setTMTVelocityRangeUpper(unsigned int index, unsigned char value)
{
    if (index < TMTVelocityRangeUpper.size()) {
        TMTVelocityRangeUpper[index] = value;
    }//if
}//setTMTVelocityRangeUpper

void SonicCellData::PatchToneMixTable::setTMTVelocityFadeWidthLower(unsigned int index, unsigned char value)
{
    if (index < TMTVelocityFadeWidthLower.size()) {
        TMTVelocityFadeWidthLower[index] = value;
    }//if
}//setTMTVelocityFadeWidthLower

void SonicCellData::PatchToneMixTable::setTMTVelocityFadeWidthUpper(unsigned int index, unsigned char value)
{
    if (index < TMTVelocityFadeWidthUpper.size()) {
        TMTVelocityFadeWidthUpper[index] = value;
    }//if
}//setTMTVelocityFadeWidthUpper

boost::shared_ptr<SonicCellData::PatchToneMixTable> SonicCellData::PatchToneMixTable::deepClone() const
{
    boost::shared_ptr<SonicCellData::PatchToneMixTable> clone(new SonicCellData::PatchToneMixTable);

    *clone = *this;

    return clone;
}//deepClone

template<class Archive>
void SonicCellData::PatchToneMixTable::serialize(Archive &ar, const unsigned int version)
{
    ar & BOOST_SERIALIZATION_NVP(StructureType12);
    ar & BOOST_SERIALIZATION_NVP(Booster12);
    ar & BOOST_SERIALIZATION_NVP(StructureType34);
    ar & BOOST_SERIALIZATION_NVP(Booster34);
    ar & BOOST_SERIALIZATION_NVP(TMTVelocityControl);

    for (unsigned int index = 0; index < 4; ++index) {
        unsigned char TMTToneSwitchVal = TMTToneSwitch[index];
        unsigned char TMTKeyboardRangeLowerVal = TMTKeyboardRangeLower[index];
        unsigned char TMTKeyboardRangeUpperVal = TMTKeyboardRangeUpper[index];
        unsigned char TMTKeyboardFadeWidthLowerVal = TMTKeyboardFadeWidthLower[index];
        unsigned char TMTKeyboardFadeWidthUpperVal = TMTKeyboardFadeWidthUpper[index];
        unsigned char TMTVelocityRangeLowerVal = TMTVelocityRangeLower[index];
        unsigned char TMTVelocityRangeUpperVal = TMTVelocityRangeUpper[index];
        unsigned char TMTVelocityFadeWidthLowerVal = TMTVelocityFadeWidthLower[index];
        unsigned char TMTVelocityFadeWidthUpperVal = TMTVelocityFadeWidthUpper[index];

        ar & BOOST_SERIALIZATION_NVP(TMTToneSwitchVal);
        ar & BOOST_SERIALIZATION_NVP(TMTKeyboardRangeLowerVal);
        ar & BOOST_SERIALIZATION_NVP(TMTKeyboardRangeUpperVal);
        ar & BOOST_SERIALIZATION_NVP(TMTKeyboardFadeWidthLowerVal);
        ar & BOOST_SERIALIZATION_NVP(TMTKeyboardFadeWidthUpperVal);
        ar & BOOST_SERIALIZATION_NVP(TMTVelocityRangeLowerVal);
        ar & BOOST_SERIALIZATION_NVP(TMTVelocityRangeUpperVal);
        ar & BOOST_SERIALIZATION_NVP(TMTVelocityFadeWidthLowerVal);
        ar & BOOST_SERIALIZATION_NVP(TMTVelocityFadeWidthUpperVal);

        TMTToneSwitch[index] = TMTToneSwitchVal;
        TMTKeyboardRangeLower[index] = TMTKeyboardRangeLowerVal;
        TMTKeyboardRangeUpper[index] = TMTKeyboardRangeUpperVal;
        TMTKeyboardFadeWidthLower[index] = TMTKeyboardFadeWidthLowerVal;
        TMTKeyboardFadeWidthUpper[index] = TMTKeyboardFadeWidthUpperVal;
        TMTVelocityRangeLower[index] = TMTVelocityRangeLowerVal;
        TMTVelocityRangeUpper[index] = TMTVelocityRangeUpperVal;
        TMTVelocityFadeWidthLower[index] = TMTVelocityFadeWidthLowerVal;
        TMTVelocityFadeWidthUpper[index] = TMTVelocityFadeWidthUpperVal;
    }//for
}//serialize


template void SonicCellData::PatchToneMixTable::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive &ar, const unsigned int version);

template void SonicCellData::PatchToneMixTable::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive &ar, const unsigned int version);
