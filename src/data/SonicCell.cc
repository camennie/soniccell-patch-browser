/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "SonicCell.h"
#include "Patch.h"
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/access.hpp>
#include <fstream>
#include <iostream>
#include <vector>
#include <list>
#include "../main.h"

namespace {

std::string getCategoryStr(unsigned int category)
{
    boost::array<const char *, 39> categories = {
                                            "--- / No Assign",
                                            "PNO / AC.Piano",
                                            "EP / EL.Piano",
                                            "KEY / Keyboards",
                                            "BEL / Bell",
                                            "MLT / Mallet",
                                            "ORG / Organ",
                                            "ACD / Accordion",
                                            "HRM / Harmonica",
                                            "AGT / AC.Guitar",
                                            "EGT / EL.Guitar",
                                            "DGT / DIST.Guitar",
                                            "BS / Bass",
                                            "SBS / Synth Bass",
                                            "STR / Strings",
                                            "ORC / Orchestra",
                                            "HIT / Hit&Stab",
                                            "WND / Wind",
                                            "FLT / Flute",
                                            "BRS / AC.Brass",
                                            "SBR / Synth Brass",
                                            "SAX / Sax",
                                            "HLD / Hard Lead",
                                            "SLD / Soft Lead",
                                            "TEK / Techno Synth",
                                            "PLS / Pulsating",
                                            "FX / Synth FX",
                                            "SYN / Other Synth",
                                            "BPD / Bright Pad",
                                            "SPD / Soft Pad",
                                            "VOX / Vox",
                                            "PLK / Plucked",
                                            "ETH / Ethnic",
                                            "FRT / Fretted",
                                            "PRC / Percussion",
                                            "SFX / Sound FX",
                                            "BTS / Beat&Groove",
                                            "DRM / Drums",
                                            "CMB / Combination"
                                            };

    if (category < categories.size()) {
        return categories[category];
    } else {
        return "UNKOWN";
    }//if
}//getCategoryStr

}//anonymous namespace

SonicCellData::SonicCell::SonicCell()
{
    /*
    for (unsigned int index = 0; index < UserPatches.size(); ++index) {
        UserPatches[index].reset(new SonicCellData::Patch);
    }//for

    for (unsigned int index = 0; index < UserPatchesDirty.size(); ++index) {
        UserPatchesDirty[index] = false;
    }//for
*/    
}//constructor

SonicCellData::SonicCell::~SonicCell()
{
    //Nothing
}//destructor

/*
bool SonicCellData::SonicCell::getPatchDirtyFlag(unsigned int index) const
{
    if (index < UserPatchesDirty.size()) {
        return UserPatchesDirty[index];
    } else {
        return false;
    }//if
}//getPatchDirtyFlag

void SonicCellData::SonicCell::setPatchDirtyFlag(unsigned int index, bool dirty)
{
    if (index < UserPatchesDirty.size()) {
        UserPatchesDirty[index] = dirty;
    }//if
}//setPatchDirtyFlag

boost::shared_ptr<SonicCellData::Patch> SonicCellData::SonicCell::getUserPatch(unsigned int index) const
{
    if (index < UserPatches.size()) {
        return UserPatches[index];
    } else {
        return boost::shared_ptr<SonicCellData::Patch>();
    }//if
}//getUserPatch

void SonicCellData::SonicCell::setUserPatch(unsigned int index, boost::shared_ptr<SonicCellData::Patch> patch)
{
    if (index < UserPatches.size()) {
        UserPatches[index] = patch;
    }//if
}//setUserPatch
*/

//Data will have the SYSEX header, checksum, and footer stripped
bool SonicCellData::SonicCell::handleSysExMessage(jack_midi_data_t *data, unsigned int length)
{
    for (unsigned int pos = 0; pos < length; ++pos) {
        if (data[pos] >= 0x80) {
            for (unsigned int index = 0; index < length; ++index) {
                std::cout << std::hex << (unsigned int)data[index] << " ";                
            }//for
            std::cout << std::dec << " -- " << pos << std::endl;
        }//if

        assert(data[pos] < 0x80);
    }//for

    unsigned int address = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];

    if (address == 0x1f000000) {
//        unsigned int patchNum = (data[0] - 0x30) * 0x80 + data[1];
//        unsigned int offset = address & 0xffff;


        if (true == pendingSRXChecks.empty()) {
            std::vector<char> patchNameBase;
            std::copy(data+4, data+4+12, std::back_inserter(patchNameBase));
            patchNameBase.push_back('\0');

            std::string patchName(&patchNameBase[0]);
            unsigned char category = data[4+12];

            std::pair<PatchInfoType, PatchInfo> topQueue = pendingPatchInfos.front();
            pendingPatchInfos.pop_front();

            topQueue.second.patchName = patchName;
            topQueue.second.category = category;

            boost::shared_ptr<PatchInfo> newPatchInfo(new PatchInfo);
            *newPatchInfo = topQueue.second;
            PatchInfos.insert(std::make_pair(topQueue.first, newPatchInfo));

//            std::cout << "Bank: '" << topQueue.second.bankName << "' Num: " << topQueue.second.actualPatchNum 
//                        << " Category: " << getCategoryStr(topQueue.second.category) << " Name: '" << topQueue.second.patchName << "'"
 //                       << std::endl;
        } else {
//            std::cout << (unsigned int)(data[4]) << " - " << (unsigned int)(data[5]) << (unsigned int)(data[6]) << std::endl;
//            unsigned int curSRX = pendingSRXChecks.front();
//            pendingSRXChecks.pop_front();
//            std::cout << "SRX: " << curSRX << " - '" << patchName << "'" << std::endl;
        }//if

        return true;
    } else {
        std::cout << "Unknown SonicCell::SysEx Message: " << address << std::endl;
    }//if

    return false;
}//handleSysExMessage

//Updates everything contained in the SonicCell
std::vector<std::vector<jack_midi_data_t> > SonicCellData::SonicCell::createUpdateRequest()
{
    std::vector<PatchInfoRequest> patchInfoRequests;

    patchInfoRequests.push_back(PatchInfoRequest(User, "USER", 87, 0, 128, 0));
    patchInfoRequests.push_back(PatchInfoRequest(User, "USER", 87, 1, 128, 128));
    patchInfoRequests.push_back(PatchInfoRequest(ROM, "PR-A", 87, 64, 128, 0));
    patchInfoRequests.push_back(PatchInfoRequest(ROM, "PR-B", 87, 65, 128, 0));
    patchInfoRequests.push_back(PatchInfoRequest(ROM, "PR-C", 87, 66, 128, 0));
    patchInfoRequests.push_back(PatchInfoRequest(ROM, "PR-D", 87, 67, 128, 0));
    patchInfoRequests.push_back(PatchInfoRequest(ROM, "PR-E", 87, 68, 128, 0));
    patchInfoRequests.push_back(PatchInfoRequest(ROM, "PR-F", 87, 69, 128, 0));
    patchInfoRequests.push_back(PatchInfoRequest(ROM, "PR-G", 87, 70, 128, 0));
    patchInfoRequests.push_back(PatchInfoRequest(GM, "GM", 0, 0, 128, 0));

    patchInfoRequests.push_back(PatchInfoRequest(SRX, "SRX-08", 93, 15, 128,   0));
    patchInfoRequests.push_back(PatchInfoRequest(SRX, "SRX-08", 93, 16, 128, 128));
    patchInfoRequests.push_back(PatchInfoRequest(SRX, "SRX-08", 93, 17, 128, 256));
    patchInfoRequests.push_back(PatchInfoRequest(SRX, "SRX-08", 93, 18,  64, 386));

    std::vector<std::vector<jack_midi_data_t> > retData;

    assert(true == pendingPatchInfos.empty());
    for (unsigned int infoRequestIndex = 0; infoRequestIndex < patchInfoRequests.size(); ++infoRequestIndex) {
        for (unsigned int patch = 0; patch < patchInfoRequests[infoRequestIndex].numPrograms; ++patch) {
            PatchInfo newPatchInfo;
            newPatchInfo.bankName = patchInfoRequests[infoRequestIndex].bankName;
            newPatchInfo.patchBankMSB = patchInfoRequests[infoRequestIndex].patchBankMSB;
            newPatchInfo.patchBankLSB = patchInfoRequests[infoRequestIndex].patchBankLSB;
            newPatchInfo.patchNum = patch;
            newPatchInfo.actualPatchNum = patch + patchInfoRequests[infoRequestIndex].programOffset;

            std::pair<PatchInfoType, PatchInfo> queuePair(patchInfoRequests[infoRequestIndex].infoType, newPatchInfo) ;
            pendingPatchInfos.push_back(queuePair);

            std::vector<jack_midi_data_t> setMSB;
            std::vector<jack_midi_data_t> setLSB;
            std::vector<jack_midi_data_t> programChange;

            setMSB.push_back(0xb0);
            setMSB.push_back(0x00);
            setMSB.push_back(newPatchInfo.patchBankMSB);

            setLSB.push_back(0xb0);
            setLSB.push_back(0x20);
            setLSB.push_back(newPatchInfo.patchBankLSB);

            programChange.push_back(0xc0);
            programChange.push_back(newPatchInfo.patchNum);

            boost::array<jack_midi_data_t, 16> nameRequestBase = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x11, 0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x13, 0xf7}; //name and category
            std::vector<jack_midi_data_t> nameRequest;
            std::copy(&nameRequestBase[0], &nameRequestBase[0]+nameRequestBase.size(), std::back_inserter(nameRequest));
            JackSingleton::instance().addChecksum(nameRequest);

            retData.push_back(setMSB);
            retData.push_back(setLSB);
            retData.push_back(programChange);
            retData.push_back(nameRequest);
        }//for
    }//for

    return retData;
}//createUpdateRequest

void SonicCellData::SonicCell::loadFromFile(const std::string &filename)
{
	std::ifstream inputStream(filename.c_str());
	assert(inputStream.good());
	if (false == inputStream.good()) {
		return;
	}//if

	boost::archive::xml_iarchive inputArchive(inputStream);

    unsigned int numPatches = PatchInfos.size();
    inputArchive & BOOST_SERIALIZATION_NVP(numPatches);

    PatchInfos.clear();
    for (unsigned int index = 0; index < numPatches; ++index) {
        SonicCellData::PatchInfo patchInfo;
        PatchInfoType patchInfoType;

        inputArchive & BOOST_SERIALIZATION_NVP(patchInfoType);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchName);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.bankName);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchBankMSB);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchBankLSB);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchNum);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.actualPatchNum);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.category);

        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo1);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo2);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo3);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo4);

        boost::shared_ptr<PatchInfo> newPatchInfo(new PatchInfo);
        *newPatchInfo = patchInfo;
        PatchInfos.insert(std::make_pair(patchInfoType, newPatchInfo));

        //For rosegarden device definition files
//        int patchNum = patchInfo.patchNum + 1;
//        std::cout << patchInfo.bankName << " , " << (unsigned int)patchInfo.patchBankMSB << " , "
//                  << (unsigned int)patchInfo.patchBankLSB << " , " << patchNum << " , " 
//                  << patchInfo.patchName << std::endl;

    }//for

    inputStream.close();
}//loadFromFile

void SonicCellData::SonicCell::saveToFile(const std::string &filename)
{
	std::ofstream outputStream(filename.c_str());
	assert(outputStream.good());
	if (false == outputStream.good()) {
		return;
	}//if

	boost::archive::xml_oarchive outputArchive(outputStream);

    unsigned int numPatches = PatchInfos.size();
    outputArchive & BOOST_SERIALIZATION_NVP(numPatches);
    for (std::multimap<PatchInfoType, boost::shared_ptr<PatchInfo> >::const_iterator patchIter = PatchInfos.begin(); patchIter != PatchInfos.end(); ++patchIter) {
        SonicCellData::PatchInfo patchInfo = *patchIter->second;
        PatchInfoType patchInfoType = patchIter->first;

        outputArchive & BOOST_SERIALIZATION_NVP(patchInfoType);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchName);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.bankName);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchBankMSB);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchBankLSB);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchNum);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.actualPatchNum);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.category);

        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo1);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo2);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo3);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo4);
    }//for

    outputStream.close();
}//saveToFile

void SonicCellData::SonicCell::checkForSRXs()
{
    std::map<unsigned int, unsigned char> SRX_LSB;

    SRX_LSB[1] = 0;
    SRX_LSB[2] = 1;
    SRX_LSB[3] = 2;
    SRX_LSB[4] = 3;
    SRX_LSB[5] = 4;
    SRX_LSB[6] = 7;
    SRX_LSB[7] = 11;
    SRX_LSB[8] = 15;
    SRX_LSB[9] = 19;
    SRX_LSB[10] = 23;
    SRX_LSB[11] = 24;
    SRX_LSB[12] = 26;
    SRX_LSB[97] = 97;
    SRX_LSB[98] = 98;

    std::vector<std::vector<jack_midi_data_t> > retData;

    for (std::map<unsigned int, unsigned char>::const_iterator srxIter = SRX_LSB.begin(); srxIter != SRX_LSB.end(); ++srxIter) {
        pendingSRXChecks.push_back(srxIter->first);

        std::vector<jack_midi_data_t> setMSB;
        std::vector<jack_midi_data_t> setLSB;
        std::vector<jack_midi_data_t> programChange;

        setMSB.push_back(0xb0);
        setMSB.push_back(0x00);
        setMSB.push_back(93);

        setLSB.push_back(0xb0);
        setLSB.push_back(0x20);
        setLSB.push_back(srxIter->second);

        programChange.push_back(0xc0);
        programChange.push_back(0x00);

//        boost::array<jack_midi_data_t, 16> nameRequestBase = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x11, 0x1f, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x01, 0xf7}; //name and category
        boost::array<jack_midi_data_t, 16> nameRequestBase = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x11, 0x01, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x03, 0xf7};
        std::vector<jack_midi_data_t> nameRequest;
        std::copy(&nameRequestBase[0], &nameRequestBase[0]+nameRequestBase.size(), std::back_inserter(nameRequest));
        JackSingleton::instance().addChecksum(nameRequest);

        retData.push_back(setMSB);
        retData.push_back(setLSB);
        retData.push_back(programChange);
        retData.push_back(nameRequest);
    }//for

    for (unsigned int pos = 0; pos < retData.size(); ++pos) {
        std::vector<jack_midi_data_t> request = retData[pos];
        JackSingleton &jack = JackSingleton::instance();
        jack.queueMessage(&request[0], request.size());
    }//for

    JackSingleton &jack = JackSingleton::instance();
    while (jack.pendingMessages.empty() == false) {
        sleep(1);
    }//while
}//checkForSRXs

std::vector<boost::shared_ptr<SonicCellData::PatchInfo> > SonicCellData::SonicCell::getAllPatchInfos() const
{
    std::vector<boost::shared_ptr<PatchInfo> > retValues;

    for (std::multimap<PatchInfoType, boost::shared_ptr<PatchInfo> >::const_iterator patchIter = PatchInfos.begin(); patchIter != PatchInfos.end(); ++patchIter) {
        retValues.push_back(patchIter->second);
    }//for

    return retValues;
}//getAllPatchInfos

