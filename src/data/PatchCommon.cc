/*
    SonicCellPatchBrowser -- A Linux based patch browser tool for the Roland SonicCell
    Copyright (C) 2010 Chris A. Mennie

    This file is part of SonicCellPatchBrowser.

    SonicCellPatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SonicCellPatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SonicCellPatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "PatchCommon.h"
#include <algorithm>
#include <iostream>
#include "../main.h"
#include "Patch.h"
#include <jack/midiport.h>
#include <string.h>

SonicCellData::PatchCommon::PatchCommon()
{
    memset(&PatchName[0], ' ', 12);
    PatchName[12] = '\0';

    PatchCategory = 0;
    PatchLevel = 0;
    PatchPan = 0;
    PatchPriority = 0;
    PatchCoarseTune = 0;
    PatchFineTune = 0;
    OctaveShift = 0;
    StretchTuneDepth = 0;
    AnalogFeel = 0;
    MonoPoly = 0;
    LegatoSwitch = 0;
    LegatoRetrigger = 0;
    PortamentoSwitch = 0;
    PortamentoMode = 0;
    PortamentoType = 0;
    PortamentoStart = 0;
    PortamentoTime = 0;
    CutoffOffset = 0;
    ResonanceOffset = 0;
    AttackOffset = 0;
    ReleaseOffset = 0;
    VelocitySensOffset = 0;
    PatchOutputAssign = 0;
    TMTControlSwitch = 0;
    PitchBendRangeUp = 0;
    PitchBendRangeDown = 0;

    memset(&MatrixControlSource[0], 0, sizeof(MatrixControlSource));
    memset(&MatrixControlDestination[0][0], 0, sizeof(MatrixControlDestination));
    memset(&MatrixControlSens[0][0], 0, sizeof(MatrixControlSens));
    
    PartModulationSwitch = 0;
}//constructor

SonicCellData::PatchCommon::~PatchCommon()
{
    //Nothing
}//destructor

bool SonicCellData::PatchCommon::handleSysExMessage(unsigned int offset, jack_midi_data_t *data, unsigned int dataSize)
{
    if (dataSize != SonicCellData::PatchCommon::fullDataSize) {
        std::cout << "Initial patch common dataSize: " << dataSize << std::endl;
    }//if

    switch (offset) {
        case 0x00:
            if (0 == dataSize) break;
            PatchName[0] = *data;
            data++; dataSize--;
        case 0x01:
            if (0 == dataSize) break;
            PatchName[1] = *data;
            data++; dataSize--;           
        case 0x02:
            if (0 == dataSize) break;
            PatchName[2] = *data;
            data++; dataSize--;
        case 0x03:
            if (0 == dataSize) break;
            PatchName[3] = *data;
            data++; dataSize--;
        case 0x04:
            if (0 == dataSize) break;
            PatchName[4] = *data;
            data++; dataSize--;
        case 0x05:
            if (0 == dataSize) break;
            PatchName[5] = *data;
            data++; dataSize--;
       case 0x06:
            if (0 == dataSize) break;
            PatchName[6] = *data;
            data++; dataSize--;
       case 0x07:
            if (0 == dataSize) break;
            PatchName[7] = *data;
            data++; dataSize--;
        case 0x08:
            if (0 == dataSize) break;
            PatchName[8] = *data;
            data++; dataSize--;
        case 0x09:
            if (0 == dataSize) break;
            PatchName[9] = *data;
            data++; dataSize--;
        case 0x0a:
            if (0 == dataSize) break;
            PatchName[10] = *data;
            data++; dataSize--;
        case 0x0b:
            if (0 == dataSize) break;
            PatchName[11] = *data;
            data++; dataSize--;
        case 0x0c:
            if (0 == dataSize) break;
            PatchCategory = *data;
            data++; dataSize--;

            //0x0D: reserved
            data++; dataSize--;

        case 0x0e:
            if (0 == dataSize) break;
            PatchLevel = *data;
            data++; dataSize--;
        case 0x0f:
            if (0 == dataSize) break;
            PatchPan = *data;
            data++; dataSize--;
        case 0x10:
            if (0 == dataSize) break;
            PatchPriority = *data;
            data++; dataSize--;
        case 0x11:
            if (0 == dataSize) break;
            PatchCoarseTune = *data;
            data++; dataSize--;
        case 0x12:
            if (0 == dataSize) break;
            PatchFineTune = *data;
            data++; dataSize--;
        case 0x13:
            if (0 == dataSize) break;
            OctaveShift = *data;
            data++; dataSize--;
        case 0x14:
            if (0 == dataSize) break;
            StretchTuneDepth = *data;
            data++; dataSize--;
        case 0x15:
            if (0 == dataSize) break;
            AnalogFeel = *data;
            data++; dataSize--;
        case 0x16:
            if (0 == dataSize) break;
            MonoPoly = *data;
            data++; dataSize--;
        case 0x17:
            if (0 == dataSize) break;
            LegatoSwitch = *data;
            data++; dataSize--;
        case 0x18:
            if (0 == dataSize) break;
            LegatoRetrigger = *data;
            data++; dataSize--;
        case 0x19:
            if (0 == dataSize) break;
            PortamentoSwitch = *data;
            data++; dataSize--;
        case 0x1a:
            if (0 == dataSize) break;
            PortamentoMode = *data;
            data++; dataSize--;
        case 0x1b:
            if (0 == dataSize) break;
            PortamentoType = *data;
            data++; dataSize--;
        case 0x1c:
            if (0 == dataSize) break;
            PortamentoStart = *data;
            data++; dataSize--;
        case 0x1d:
            if (0 == dataSize) break;
            PortamentoTime = *data;
            data++; dataSize--;

            //0x1e: reserved
            data++; dataSize--;
            //0x1f: reserved
            data++; dataSize--;
            //0x20: reserved
            data++; dataSize--;
            //0x21: reserved
            data++; dataSize--;

        case 0x22:
            if (0 == dataSize) break;
            CutoffOffset = *data;
            data++; dataSize--;
        case 0x23:
            if (0 == dataSize) break;
            ResonanceOffset = *data;
            data++; dataSize--;
        case 0x24:
            if (0 == dataSize) break;
            AttackOffset = *data;
            data++; dataSize--;
        case 0x25:
            if (0 == dataSize) break;
            ReleaseOffset = *data;
            data++; dataSize--;
        case 0x26:
            if (0 == dataSize) break;
            VelocitySensOffset = *data;
            data++; dataSize--;
        case 0x27:
            if (0 == dataSize) break;
            PatchOutputAssign = *data;
            data++; dataSize--;
        case 0x28:
            if (0 == dataSize) break;
            TMTControlSwitch = *data;
            data++; dataSize--;
        case 0x29:
            if (0 == dataSize) break;
            PitchBendRangeUp = *data;
            data++; dataSize--;
        case 0x2a:
            if (0 == dataSize) break;
            PitchBendRangeDown = *data;
            data++; dataSize--;
        case 0x2b:
            if (0 == dataSize) break;
            MatrixControlSource[0] = *data;
            data++; dataSize--;
        case 0x2c:
            if (0 == dataSize) break;
            MatrixControlDestination[0][0] = *data;
            data++; dataSize--;
        case 0x2d:
            if (0 == dataSize) break;
            MatrixControlSens[0][0] = *data;
            data++; dataSize--;
        case 0x2e:
            if (0 == dataSize) break;
            MatrixControlDestination[0][1] = *data;
            data++; dataSize--;
        case 0x2f:
            if (0 == dataSize) break;
            MatrixControlSens[0][1] = *data;
            data++; dataSize--;
        case 0x30:
            if (0 == dataSize) break;
            MatrixControlDestination[0][2] = *data;
            data++; dataSize--;
        case 0x31:
            if (0 == dataSize) break;
            MatrixControlSens[0][2] = *data;
            data++; dataSize--;
        case 0x32:
            if (0 == dataSize) break;
            MatrixControlDestination[0][3] = *data;
            data++; dataSize--;
        case 0x33:
            if (0 == dataSize) break;
            MatrixControlSens[0][3] = *data;
            data++; dataSize--;
        case 0x34:
            if (0 == dataSize) break;
            MatrixControlSource[1] = *data;
            data++; dataSize--;
        case 0x35:
            if (0 == dataSize) break;
            MatrixControlDestination[1][0] = *data;
            data++; dataSize--;
        case 0x36:
            if (0 == dataSize) break;
            MatrixControlSens[1][0] = *data;
            data++; dataSize--;
        case 0x37:
            if (0 == dataSize) break;
            MatrixControlDestination[1][1] = *data;
            data++; dataSize--;
        case 0x38:
            if (0 == dataSize) break;
            MatrixControlSens[1][1] = *data;
            data++; dataSize--;
        case 0x39:
            if (0 == dataSize) break;
            MatrixControlDestination[1][2] = *data;
            data++; dataSize--;
        case 0x3a:
            if (0 == dataSize) break;
            MatrixControlSens[1][2] = *data;
            data++; dataSize--;
        case 0x3b:
            if (0 == dataSize) break;
            MatrixControlDestination[1][3] = *data;
            data++; dataSize--;
        case 0x3c:
            if (0 == dataSize) break;
            MatrixControlSens[1][3] = *data;
            data++; dataSize--;
        case 0x3d:
            if (0 == dataSize) break;
            MatrixControlSource[2] = *data;
            data++; dataSize--;
        case 0x3e:
            if (0 == dataSize) break;
            MatrixControlDestination[2][0] = *data;
            data++; dataSize--;
        case 0x3f:
            if (0 == dataSize) break;
            MatrixControlSens[2][0] = *data;
            data++; dataSize--;
        case 0x40:
            if (0 == dataSize) break;
            MatrixControlDestination[2][1] = *data;
            data++; dataSize--;
        case 0x41:
            if (0 == dataSize) break;
            MatrixControlSens[2][1] = *data;
            data++; dataSize--;
        case 0x42:
            if (0 == dataSize) break;
            MatrixControlDestination[2][2] = *data;
            data++; dataSize--;
        case 0x43:
            if (0 == dataSize) break;
            MatrixControlSens[2][2] = *data;
            data++; dataSize--;
        case 0x44:
            if (0 == dataSize) break;
            MatrixControlDestination[2][3] = *data;
            data++; dataSize--;
        case 0x45:
            if (0 == dataSize) break;
            MatrixControlSens[2][3] = *data;
            data++; dataSize--;
        case 0x46:
            if (0 == dataSize) break;
            MatrixControlSource[3] = *data;
            data++; dataSize--;
        case 0x47:
            if (0 == dataSize) break;
            MatrixControlDestination[3][0] = *data;
            data++; dataSize--;
        case 0x48:
            if (0 == dataSize) break;
            MatrixControlSens[3][0] = *data;
            data++; dataSize--;
        case 0x49:
            if (0 == dataSize) break;
            MatrixControlDestination[3][1] = *data;
            data++; dataSize--;
        case 0x4a:
            if (0 == dataSize) break;
            MatrixControlSens[3][1] = *data;
            data++; dataSize--;
        case 0x4b:
            if (0 == dataSize) break;
            MatrixControlDestination[3][2] = *data;
            data++; dataSize--;
        case 0x4c:
            if (0 == dataSize) break;
            MatrixControlSens[3][2] = *data;
            data++; dataSize--;
        case 0x4d:
            if (0 == dataSize) break;
            MatrixControlDestination[3][3] = *data;
            data++; dataSize--;
        case 0x4e:
            if (0 == dataSize) break;
            MatrixControlSens[3][3] = *data;
            data++; dataSize--;
        case 0x4f:
            if (0 == dataSize) break;
            PartModulationSwitch = *data;
            data++; dataSize--;
            break;
        default:
            std::cerr << "Invalid patch common offset: " << offset << std::endl;
            return false;
    }//switch

    if (0 != dataSize) {
        std::cout << "dataSize: " << dataSize << std::endl;
        return 0;
    }//if

    assert(0 == dataSize);
    return true;
}//handleSysExMessage

std::vector<jack_midi_data_t> SonicCellData::PatchCommon::createWriteRequest(unsigned char myIndex) const
{
    std::vector<jack_midi_data_t> requestBuffer;

    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x12};
    std::copy(dataHeader, dataHeader+7, std::back_inserter(requestBuffer));
    
    unsigned char addressHighHigh = 0x30;
    unsigned char addressHighLow = myIndex;
    if (myIndex > 127) {
        addressHighHigh++;
        addressHighLow -= 128;
    }//if

    jack_midi_data_t addressHigh[2] = {addressHighHigh, addressHighLow};
    std::copy(addressHigh, addressHigh+2, std::back_inserter(requestBuffer));

    jack_midi_data_t commonOffset[2] = {0x00, 0x00};
    std::copy(commonOffset, commonOffset+2, std::back_inserter(requestBuffer));

    //Add data
    requestBuffer.push_back(PatchName[0]);
    requestBuffer.push_back(PatchName[1]);
    requestBuffer.push_back(PatchName[2]);
    requestBuffer.push_back(PatchName[3]);
    requestBuffer.push_back(PatchName[4]);
    requestBuffer.push_back(PatchName[5]);
    requestBuffer.push_back(PatchName[6]);
    requestBuffer.push_back(PatchName[7]);
    requestBuffer.push_back(PatchName[8]);
    requestBuffer.push_back(PatchName[9]);
    requestBuffer.push_back(PatchName[10]);
    requestBuffer.push_back(PatchName[11]);
    requestBuffer.push_back(PatchCategory);

    //0x0D: reserved
    requestBuffer.push_back(0);

    requestBuffer.push_back(PatchLevel);
    requestBuffer.push_back(PatchPan);
    requestBuffer.push_back(PatchPriority);
    requestBuffer.push_back(PatchCoarseTune);
    requestBuffer.push_back(PatchFineTune);
    requestBuffer.push_back(OctaveShift);
    requestBuffer.push_back(StretchTuneDepth);
    requestBuffer.push_back(AnalogFeel);
    requestBuffer.push_back(MonoPoly);
    requestBuffer.push_back(LegatoSwitch);
    requestBuffer.push_back(LegatoRetrigger);
    requestBuffer.push_back(PortamentoSwitch);
    requestBuffer.push_back(PortamentoMode);
    requestBuffer.push_back(PortamentoType);
    requestBuffer.push_back(PortamentoStart);
    requestBuffer.push_back(PortamentoTime);

    //0x1e: reserved
    requestBuffer.push_back(0);
    //0x1f: reserved
    requestBuffer.push_back(0);
    //0x20: reserved
    requestBuffer.push_back(0);
    //0x21: reserved
    requestBuffer.push_back(0);

    requestBuffer.push_back(CutoffOffset);
    requestBuffer.push_back(ResonanceOffset);
    requestBuffer.push_back(AttackOffset);
    requestBuffer.push_back(ReleaseOffset);
    requestBuffer.push_back(VelocitySensOffset);
    requestBuffer.push_back(PatchOutputAssign);
    requestBuffer.push_back(TMTControlSwitch);
    requestBuffer.push_back(PitchBendRangeUp);
    requestBuffer.push_back(PitchBendRangeDown);
    requestBuffer.push_back(MatrixControlSource[0]);
    requestBuffer.push_back(MatrixControlDestination[0][0]);
    requestBuffer.push_back(MatrixControlSens[0][0]);
    requestBuffer.push_back(MatrixControlDestination[0][1]);
    requestBuffer.push_back(MatrixControlSens[0][1]);
    requestBuffer.push_back(MatrixControlDestination[0][2]);
    requestBuffer.push_back(MatrixControlSens[0][2]);
    requestBuffer.push_back(MatrixControlDestination[0][3]);
    requestBuffer.push_back(MatrixControlSens[0][3]);
    requestBuffer.push_back(MatrixControlSource[1]);
    requestBuffer.push_back(MatrixControlDestination[1][0]);
    requestBuffer.push_back(MatrixControlSens[1][0]);
    requestBuffer.push_back(MatrixControlDestination[1][1]);
    requestBuffer.push_back(MatrixControlSens[1][1]);
    requestBuffer.push_back(MatrixControlDestination[1][2]);
    requestBuffer.push_back(MatrixControlSens[1][2]);
    requestBuffer.push_back(MatrixControlDestination[1][3]);
    requestBuffer.push_back(MatrixControlSens[1][3]);
    requestBuffer.push_back(MatrixControlSource[2]);
    requestBuffer.push_back(MatrixControlDestination[2][0]);
    requestBuffer.push_back(MatrixControlSens[2][0]);
    requestBuffer.push_back(MatrixControlDestination[2][1]);
    requestBuffer.push_back(MatrixControlSens[2][1]);
    requestBuffer.push_back(MatrixControlDestination[2][2]);
    requestBuffer.push_back(MatrixControlSens[2][2]);
    requestBuffer.push_back(MatrixControlDestination[2][3]);
    requestBuffer.push_back(MatrixControlSens[2][3]);
    requestBuffer.push_back(MatrixControlSource[3]);
    requestBuffer.push_back(MatrixControlDestination[3][0]);
    requestBuffer.push_back(MatrixControlSens[3][0]);
    requestBuffer.push_back(MatrixControlDestination[3][1]);
    requestBuffer.push_back(MatrixControlSens[3][1]);
    requestBuffer.push_back(MatrixControlDestination[3][2]);
    requestBuffer.push_back(MatrixControlSens[3][2]);
    requestBuffer.push_back(MatrixControlDestination[3][3]);
    requestBuffer.push_back(MatrixControlSens[3][3]);
    requestBuffer.push_back(PartModulationSwitch);

    //Footer
    requestBuffer.push_back(0xf7);

    assert(requestBuffer.size() == SonicCellData::PatchCommon::fullDataSize + 7 + 4 + 1);

    JackSingleton::addChecksum(requestBuffer);

    return requestBuffer;
}//createWriteRequest

std::vector<jack_midi_data_t> SonicCellData::PatchCommon::createUpdateRequest(unsigned char myIndex) const
{
    std::vector<jack_midi_data_t> requestBuffer;

    jack_midi_data_t dataHeader[7] = {0xf0, 0x41, 0x10, 0x00, 0x00, 0x25, 0x11};
    std::copy(dataHeader, dataHeader+7, std::back_inserter(requestBuffer));
    
    unsigned char addressHighHigh = 0x30;
    unsigned char addressHighLow = myIndex;
    if (myIndex > 127) {
        addressHighHigh++;
        addressHighLow -= 128;
    }//if

    jack_midi_data_t addressHigh[2] = {addressHighHigh, addressHighLow};
    std::copy(addressHigh, addressHigh+2, std::back_inserter(requestBuffer));

    jack_midi_data_t commonOffset[2] = {0x00, 0x00};
    std::copy(commonOffset, commonOffset+2, std::back_inserter(requestBuffer));

    unsigned int size = SonicCellData::PatchCommon::fullDataSize;
    requestBuffer.push_back((size & 0xff000000) >> 24);
    requestBuffer.push_back((size & 0x00ff0000) >> 16);
    requestBuffer.push_back((size & 0x0000ff00) >> 8);
    requestBuffer.push_back(size & 0x000000ff);

    requestBuffer.push_back(0xf7);

    JackSingleton::addChecksum(requestBuffer);

    return requestBuffer;
}//createUpdateRequest

std::string SonicCellData::PatchCommon::getPatchName() const
{
    return std::string((const char*)&PatchName[0]);
}//getPatchName

unsigned char SonicCellData::PatchCommon::getPatchCategory() const
{
    return PatchCategory;
}//getPatchCategory

unsigned char SonicCellData::PatchCommon::getPatchLevel() const
{
    return PatchLevel;
}//getPatchLevel

unsigned char SonicCellData::PatchCommon::getPatchPan() const
{
    return PatchPan;
}//getPatchPan

unsigned char SonicCellData::PatchCommon::getPatchPriority() const
{
    return PatchPriority;
}//getPatchPriority

unsigned char SonicCellData::PatchCommon::getPatchCoarseTune() const
{
    return PatchCoarseTune;
}//getPatchCoarseTune

unsigned char SonicCellData::PatchCommon::getPatchFineTune() const
{
    return PatchFineTune;
}//getPatchFineTune

unsigned char SonicCellData::PatchCommon::getOctaveShift() const
{
    return OctaveShift;
}//getOctaveShift

unsigned char SonicCellData::PatchCommon::getStretchTuneDepth() const
{
    return StretchTuneDepth;
}//getStretchTuneDepth

unsigned char SonicCellData::PatchCommon::getAnalogFeel() const
{
    return AnalogFeel;
}//getAnalogFeel

unsigned char SonicCellData::PatchCommon::getMonoPoly() const
{
    return MonoPoly;
}//getMonoPoly

unsigned char SonicCellData::PatchCommon::getLegatoSwitch() const
{
    return LegatoSwitch;
}//getLegatoSwitch

unsigned char SonicCellData::PatchCommon::getLegatoRetrigger() const
{
    return LegatoRetrigger;
}//getLegatoRetrigger

unsigned char SonicCellData::PatchCommon::getPortamentoSwitch() const
{
    return PortamentoSwitch;
}//getPortamentoSwitch

unsigned char SonicCellData::PatchCommon::getPortamentoMode() const
{
    return PortamentoMode;
}//getPortamentoMode

unsigned char SonicCellData::PatchCommon::getPortamentoType() const
{
    return PortamentoType;
}//getPortamentoType

unsigned char SonicCellData::PatchCommon::getPortamentoStart() const
{
    return PortamentoStart;
}//getPortamentoStart

unsigned char SonicCellData::PatchCommon::getPortamentoTime() const
{
    return PortamentoTime;
}//getPortamentoTime

unsigned char SonicCellData::PatchCommon::getCutoffOffset() const
{
    return CutoffOffset;
}//getCutoffOffset

unsigned char SonicCellData::PatchCommon::getResonanceOffset() const
{
    return ResonanceOffset;
}//getResonanceOffset

unsigned char SonicCellData::PatchCommon::getAttackOffset() const
{
    return AttackOffset;
}//getAttackOffset

unsigned char SonicCellData::PatchCommon::getReleaseOffset() const
{
    return ReleaseOffset;
}//getReleaseOffset

unsigned char SonicCellData::PatchCommon::getVelocitySensOffset() const
{
    return VelocitySensOffset;
}//getVelocitySensOffset

unsigned char SonicCellData::PatchCommon::getPatchOutputAssign() const
{
    return PatchOutputAssign;
}//getPatchOutputAssign

unsigned char SonicCellData::PatchCommon::getTMTControlSwitch() const
{
    return TMTControlSwitch;
}//getTMTControlSwitch

unsigned char SonicCellData::PatchCommon::getPitchBendRangeUp() const
{
    return PitchBendRangeUp;
}//getPitchBendRangeUp

unsigned char SonicCellData::PatchCommon::getPitchBendRangeDown() const
{
    return PitchBendRangeDown;
}//getPitchBendRangeDown

unsigned char SonicCellData::PatchCommon::getMatrixControlSource(unsigned int index) const
{
    if (index < MatrixControlSource.size()) {
        return MatrixControlSource[index];
    } else {
        return 0;
    }//if
}//getMatrixControlSource

unsigned char SonicCellData::PatchCommon::getMatrixControlDestination(unsigned int control, unsigned int index) const
{
    if ((control < MatrixControlDestination.size()) && (index < MatrixControlDestination[0].size())) {
        return MatrixControlDestination[control][index];
    } else {
        return 0;
    }//if
}//getMatrixControlDestination

unsigned char SonicCellData::PatchCommon::getMatrixControlSens(unsigned int control, unsigned int index) const
{
    if ((control < MatrixControlSens.size()) && (index < MatrixControlSens[0].size())) {
        return MatrixControlSens[control][index];
    } else {
        return 0;
    }//if
}//getMatrixControlSens

unsigned char SonicCellData::PatchCommon::getPartModulationSwitch() const
{
    return PartModulationSwitch;
}//getPartModulationSwitch

void SonicCellData::PatchCommon::setPatchName(const std::string &name)
{
    unsigned int length = 12;

    if (name.size() < 12) {
        length = name.size();
    }//if

    memset(&PatchName[0], ' ', 12);
    memcpy(&PatchName[0], name.c_str(), length);
}//setPatchName

void SonicCellData::PatchCommon::setPatchCategory(unsigned char value)
{
    PatchCategory = value;
}//setPatchCategory

void SonicCellData::PatchCommon::setPatchLevel(unsigned char value)
{
    PatchLevel = value;
}//setPatchLevel

void SonicCellData::PatchCommon::setPatchPan(unsigned char value)
{
    PatchPan = value;
}//setPatchPan

void SonicCellData::PatchCommon::setPatchPriority(unsigned char value)
{
    PatchPriority = value;
}//setPatchPriority

void SonicCellData::PatchCommon::setPatchCoarseTune(unsigned char value)
{
    PatchCoarseTune = value;
}//setPatchCoarseTune

void SonicCellData::PatchCommon::setPatchFineTune(unsigned char value)
{
    PatchFineTune = value;
}//setPatchFineTune

void SonicCellData::PatchCommon::setOctaveShift(unsigned char value)
{
    OctaveShift = value;
}//setOctaveShift

void SonicCellData::PatchCommon::setStretchTuneDepth(unsigned char value)
{
    StretchTuneDepth = value;
}//setStretchTuneDepth

void SonicCellData::PatchCommon::setAnalogFeel(unsigned char value)
{
    AnalogFeel = value;
}//setAnalogFeel

void SonicCellData::PatchCommon::setMonoPoly(unsigned char value)
{
    MonoPoly = value;
}//setMonoPoly

void SonicCellData::PatchCommon::setLegatoSwitch(unsigned char value)
{
    LegatoSwitch = value;
}//setLegatoSwitch

void SonicCellData::PatchCommon::setLegatoRetrigger(unsigned char value)
{
    LegatoRetrigger = value;
}//setLegatoRetrigger

void SonicCellData::PatchCommon::setPortamentoSwitch(unsigned char value)
{
    PortamentoSwitch = value;
}//setPortamentoSwitch

void SonicCellData::PatchCommon::setPortamentoMode(unsigned char value)
{
    PortamentoMode = value;
}//setPortamentoMode

void SonicCellData::PatchCommon::setPortamentoType(unsigned char value)
{
    PortamentoType = value;
}//setPortamentoType

void SonicCellData::PatchCommon::setPortamentoStart(unsigned char value)
{
    PortamentoStart = value;
}//setPortamentoStart

void SonicCellData::PatchCommon::setPortamentoTime(unsigned char value)
{
    PortamentoTime = value;
}//setPortamentoTime

void SonicCellData::PatchCommon::setCutoffOffset(unsigned char value)
{
    CutoffOffset = value;
}//setCutoffOffset

void SonicCellData::PatchCommon::setResonanceOffset(unsigned char value)
{
    ResonanceOffset = value;
}//setResonanceOffset

void SonicCellData::PatchCommon::setAttackOffset(unsigned char value)
{
    AttackOffset = value;
}//setAttackOffset

void SonicCellData::PatchCommon::setReleaseOffset(unsigned char value)
{
    ReleaseOffset = value;
}//setReleaseOffset

void SonicCellData::PatchCommon::setVelocitySensOffset(unsigned char value)
{
    VelocitySensOffset = value;
}//setVelocitySensOffset

void SonicCellData::PatchCommon::setPatchOutputAssign(unsigned char value)
{
    PatchOutputAssign = value;
}//setPatchOutputAssign

void SonicCellData::PatchCommon::setTMTControlSwitch(unsigned char value)
{
    TMTControlSwitch = value;
}//setTMTControlSwitch

void SonicCellData::PatchCommon::setPitchBendRangeUp(unsigned char value)
{
    PitchBendRangeUp = value;
}//setPitchBendRangeUp

void SonicCellData::PatchCommon::setPitchBendRangeDown(unsigned char value)
{
    PitchBendRangeDown = value;
}//setPitchBendRangeDown

void SonicCellData::PatchCommon::setMatrixControlSource(unsigned int index, unsigned char value)
{
    if (index < MatrixControlSource.size()) {
        MatrixControlSource[index] = value;
    }//if
}//setMatrixControlSource

void SonicCellData::PatchCommon::setMatrixControlDestination(unsigned int control, unsigned int index, unsigned char value)
{
    if ((control < MatrixControlDestination.size()) && (index < MatrixControlDestination[0].size())) {
        MatrixControlDestination[control][index] = value;
    }//if
}//setMatrixControlDestination

void SonicCellData::PatchCommon::setMatrixControlSens(unsigned int control, unsigned int index, unsigned char value)
{
    if ((control < MatrixControlSens.size()) && (index < MatrixControlSens[0].size())) {
        MatrixControlSens[control][index] = value;
    }//if
}//setMatrixControlSens

void SonicCellData::PatchCommon::setPartModulationSwitch(unsigned char value)
{
    PartModulationSwitch = value;
}//setPartModulationSwitch

boost::shared_ptr<SonicCellData::PatchCommon> SonicCellData::PatchCommon::deepClone() const
{
    boost::shared_ptr<SonicCellData::PatchCommon> clone(new SonicCellData::PatchCommon);

    *clone = *this;

    return clone;
}//deepClone

template<class Archive>
void SonicCellData::PatchCommon::serialize(Archive &ar, const unsigned int version)
{
    std::string patchName = getPatchName();
    ar & BOOST_SERIALIZATION_NVP(patchName);
    setPatchName(patchName);

    ar & BOOST_SERIALIZATION_NVP(PatchCategory);
    ar & BOOST_SERIALIZATION_NVP(PatchLevel);
    ar & BOOST_SERIALIZATION_NVP(PatchPan);
    ar & BOOST_SERIALIZATION_NVP(PatchPriority);
    ar & BOOST_SERIALIZATION_NVP(PatchCoarseTune);
    ar & BOOST_SERIALIZATION_NVP(PatchFineTune);
    ar & BOOST_SERIALIZATION_NVP(OctaveShift);
    ar & BOOST_SERIALIZATION_NVP(StretchTuneDepth);
    ar & BOOST_SERIALIZATION_NVP(AnalogFeel);
    ar & BOOST_SERIALIZATION_NVP(MonoPoly);
    ar & BOOST_SERIALIZATION_NVP(LegatoSwitch);
    ar & BOOST_SERIALIZATION_NVP(LegatoRetrigger);
    ar & BOOST_SERIALIZATION_NVP(PortamentoSwitch);
    ar & BOOST_SERIALIZATION_NVP(PortamentoMode);
    ar & BOOST_SERIALIZATION_NVP(PortamentoType);
    ar & BOOST_SERIALIZATION_NVP(PortamentoStart);
    ar & BOOST_SERIALIZATION_NVP(PortamentoTime);
    ar & BOOST_SERIALIZATION_NVP(CutoffOffset);
    ar & BOOST_SERIALIZATION_NVP(ResonanceOffset);
    ar & BOOST_SERIALIZATION_NVP(AttackOffset);
    ar & BOOST_SERIALIZATION_NVP(ReleaseOffset);
    ar & BOOST_SERIALIZATION_NVP(VelocitySensOffset);
    ar & BOOST_SERIALIZATION_NVP(PatchOutputAssign);
    ar & BOOST_SERIALIZATION_NVP(TMTControlSwitch);
    ar & BOOST_SERIALIZATION_NVP(PitchBendRangeUp);
    ar & BOOST_SERIALIZATION_NVP(PitchBendRangeDown);

    for (unsigned int index = 0; index < 4; ++index) {
        unsigned char MatrixControlSourceVal = MatrixControlSource[index];
        ar & BOOST_SERIALIZATION_NVP(MatrixControlSourceVal);
        MatrixControlSource[index] = MatrixControlSourceVal;
    }//for

    for (unsigned int control = 0; control < 4; ++control) {
        for (unsigned int index = 0; index < 4; ++index) {
            unsigned char MatrixControlDestinationVal = MatrixControlDestination[control][index];
            unsigned char MatrixControlSensVal = MatrixControlSens[control][index];

            ar & BOOST_SERIALIZATION_NVP(MatrixControlDestinationVal);
            ar & BOOST_SERIALIZATION_NVP(MatrixControlSensVal);

            MatrixControlDestination[control][index] = MatrixControlDestinationVal ;
            MatrixControlSens[control][index] = MatrixControlSensVal ;
        }//for
    }//for

    ar & BOOST_SERIALIZATION_NVP(PartModulationSwitch);
}//serialize


template void SonicCellData::PatchCommon::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive &ar, const unsigned int version);

template void SonicCellData::PatchCommon::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive &ar, const unsigned int version);

